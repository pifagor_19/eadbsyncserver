'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import taskmanager, scheduler, threading

class SyncServer(object):
    # init task manager and scheduler
    def __init__(self):
        self.executor = taskmanager.TaskManager()
        self.reload_server()

    # create scheduler
    def reload_server(self, reload = False):
        self.stop_sched_evt = threading.Event()
        self.scheduler = scheduler.Scheduler(self.stop_sched_evt)
        self.scheduler.add_executor(self.executor)
        self.scheduler.start()

    # get ref to task manager
    def task_manager(self):
        return self.executor
