# -*- coding: utf-8 -*-
'''
This source file is part of eily.managers

For the latest info, see http://integra-s.com/

Created: 16.10.2013
Updated: 08.04.2014
Author: Golodov Aleksey mailto:golodov@integra-s.com

Copyright 2013-2014 (c) Integra-S JSC
'''
import math


select_distinct = lambda src,acc : filter( lambda x: x not in acc,src )

def SelectDistinct(src,acc):
    ''' Returns elements from "src" which are not in "acc"'''
    return select_distinct( src, acc )


#select_distinct_by = lambda src,acc, attr : filter( lambda x: hasattr( x, attr ) and not acc.has_key( getattr( x, attr ))  ,src )

'''
    Returns elements from "src" which are in "acc"
    src, acc must be a list type
'''
select_same = lambda src,acc : filter( lambda x: x in acc,src )

'''
    Removes elements from x which are not in y
    src, acc must be a dict type
'''
removes_distinct = lambda x, y : map( x.remove, select_distinct( x,y ))

'''
    Append elements to y which are present in y but not in x
    x and y must be a list type
'''
append_which_not_present = lambda x,y : map( x.append, select_distinct( y,x ))

'''
    Return index of value "x" from "lst" if present. -1 othercase.
    "lst" must be a list or dict type
'''
indexOf = lambda x, lst, n = 0 : -1 if n == len(lst) else n if lst[n]==x else indexOf( x, lst, n+1 )

'''
    Copy value of a field "f" from "y" to "x".
    "x" and "y" must be a dict type
'''
field_upd = lambda x,y,f : x.__setitem__( f, y.__getitem__( f ) ) if f in x.keys() and f in y.keys() else x

'''
    Returns elements from "d" which are in "s"
'''
get_same_fields = lambda d,s : filter( lambda x: x in s, d )

'''
    Returns elements from "s" which are present in "d" but not equal by value
    "s" and "d"
'''
same_not_eq = lambda d,s : filter( lambda i: i in s and s.__getitem__(i) != d.__getitem__(i), d )

'''
    Returns list of keys which are present in 'Y' and not present in 'X'
    'X' and 'Y' must be a dict type
'''
distinct_keys = lambda X,Y : filter( lambda i: i not in X, Y  )


append_dist_vals = lambda X,Y : map( lambda i: X.__setitem__( i, Y.__getitem__(i)) , distinct_keys(X,Y))
def AppendDistVals( x,y ):
    '''
    Appends key-value from 'Y' to 'X' if key from 'Y' is not in 'X'
    '''
    return append_dist_vals( x,y)


'''
    Updates values of elements in "x" which are not equal by values with elements from "s"
'''
upd_not_eq = lambda x,y : map(  lambda f: x.__setitem__(f, y.__getitem__(f)) , same_not_eq(x,y) )

uneq = lambda x,y:map(lambda f:x.__setitem__(f,y.__getitem__(f)),(lambda d,s:filter(lambda i:i in s and s.__getitem__(i) != d.__getitem__(i),d))(x,y))


upd_all = lambda x,y : map(lambda f:x.__setitem__(f,y.__getitem__(f)),(lambda d,s:filter( lambda k: k in s, d ))(x,y) )
'''
    Updates all key equal values in dict "x" from dict "y"
'''
def UpdAll( x,y):
    return upd_all(x,y)

''' Returs attributes from "d" which are in attributes of "s"'''
same_atr = lambda d,s : filter( lambda x: x in atrs_(s), atrs_(d))

''' Returns attributes of "o" excluded  a system attrs '''
atrs_ = lambda o:filter( lambda x: not str(x).startswith('__'), dir(o))

''' Returns list of attributes which are present in object "src" which names are equal with keys in "dst"'''
#non_eq
select_attr = lambda src,dst : filter( lambda atr: src.has_key(atr), atrs_(dst))

''' updates "x" attributes values from dict "y" '''
#assign
set_atr_from_dict = lambda x,y : map( lambda a: setattr( x, a, y[a]), select_attr(y,x))

convUrl = lambda x: 'http'+x[ x.index('://' ):]
appendParam = lambda x,p,v,quote='\"' : x[:x.index('}')]+ u',"%s":%s%s%s' %(p,quote,v,quote) +x[x.index('}'):]

'''
    Check is dictionary <d> has all keys in an input list <lst>
'''
is_match_inputs = lambda d,l : type(d) is dict and reduce( lambda x,y: x and y, [ d.has_key(k) for k in l ] )


to_rad = lambda x : x*math.pi / 180.
to_deg = lambda x : x*180. / math.pi
