#!/bin/sh

export LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib:/opt/lib:$LD_LIBRARY_PATH

loop() {
	cd /eadbsyncsrv
	while [ -f "/tmp/eadbsyncsrv.lock" ]
	do
		export PYTHONPATH=./SharedLibs
	    python eadbsyncsrv.py --loglvl=warning --dbhost=127.0.0.1 --dbuser=monitoring --dbpass=monitoring --ldapurl=ldap://127.0.0.1 --monitor
	    sleep 5s
	done
}

start() {
	echo "start eadbsyncsrv"
	sysctl -w net.ipv4.tcp_keepalive_time=10 net.ipv4.tcp_keepalive_intvl=5 net.ipv4.tcp_keepalive_probes=5 
	echo "starting" | cat > /tmp/eadbsyncsrv.lock
	/etc/init.d/eadbsyncsrv.sh loop &
	echo
}

stop() {
	echo "stop eadbsyncsrv"
	kill -9 `head -1 /tmp/eadbsyncsrv.lock`
	rm -f /tmp/eadbsyncsrv.lock
        echo
}

case "$1" in
  loop)
	loop
	;;
  start)
        start
        ;;
  stop)
        stop
        ;;
  status)
        cat /tmp/eadbsyncsrv.lock
        ;;
  restart)
        stop
        start
        ;;
  *)
        echo $"Usage: $0 {start|stop|restart|status}"
        exit 1
esac
exit 0