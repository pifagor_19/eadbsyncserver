import json

_file = "tasks_params.json"

fd = open(_file).read()
jsonObj = json.loads(fd)
with open(_file, "w") as _out:
    json.dump(jsonObj, _out, indent=4, sort_keys=True)
