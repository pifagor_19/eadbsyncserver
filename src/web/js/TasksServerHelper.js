/*Author: Yulia Savinkova
Author: Alex Pronin
Date: 15.01.14*/

//Префикс сервера для удаленной работы
var serverAddress = "https://192.168.10.233:8973"
var serverPrefix = "/handle_request?format=myfunc(%s)";
var serverHost = /*serverAddress +*/ serverPrefix;

var requestsCallback = [];

//Запрос списка задач
function getAllTasks() {
    sendRequest(serverHost, "get_all_tasks", "", getAllTasksCallback);
}

//Запрос параметров текущей задачи
function getTaskInfoByUID(task_uid) {
    sendRequest(serverHost, "get_task", "&task_uid=" + task_uid, getTaskCallback);
}

//Запрос статуса выполнения заданий
function getTaskStatus(){
    console.log("Get status!");
    sendRequest(serverHost, "tasks_status", "", getTaskStatusCallback, null, 200);
}

//Запуск выполнения заданий
function executeTasksRequest(){
    sendRequest(serverHost, "execute_tasks", "", executeTasksCallback, null, 10);
}

//Запрос названий колонок
function getColumns(){
    sendRequest(serverHost, "get_columns_list", "", getColumnsListCallback)
}

//Запрос планов
function getPlans(args, isReady){
    sendRequest(serverHost, "get_plans", args, getPlansCallback, isReady);
}

//Удаление задания по UID
function deleteTask(task_uid){
    sendRequest(serverHost, "delete_task", "&task_uid=" + task_uid, getDeleteTaskCallback);
}

function addTaskInfo(jsonTask){
    sendRequest(serverHost, "create_new_task", "&task=" + jsonTask, getAddTaskCallback);
};

function editTaskInfo(jsonTask){
    sendRequest(serverHost, "edit_task", "&task=" + jsonTask, getEditTaskCallback, 8000);
}

//Отправка запроса и вызов callback
function sendRequest(url, func, kwargs, callback, object, time) {
    if (time == undefined)
        time = 8000;
    //console.log(url + "&func=" + func + kwargs);
    requestsCallback[func] = callback;
    //console.log(requestsCallback);
    $.ajax({
        type:"GET",
        url: url + "&func=" + func + kwargs,
        jsonp:false,
        crossDomain: true,
        dataType:"jsonp",
        timeout:time,
        jsonpCallback: "myfunc",
        error: function() {
            callback(null);
        },
        success: function(data) {
            var call = requestsCallback[data.func];
           // console.log(call);
           // console.log(data);
            call(data.data, object);
        }
    });
}