/*Author: Yulia Savinkova
 Date: 15.01.14*/

//Скрытие кнопок управления
function makeButtonsInvisible() {
    $('#previousStep').css('display', 'none');
    $('#save').css('display', 'none');
    $('#cancel').css('display', 'none');
    $('#edit').css('display', 'none');
};

function makeInputDisabled() {
    var inputs = $('#mainform input');
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].disabled = true;
    }
    var selects = $('#mainform select');
    for (var i = 0; i < selects.length; i++) {
        selects[i].disabled = true;
    }
}

function makeInputEnabled() {
    var inputs = $('#mainform input');
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].disabled = false;
    }
    var selects = $('#mainform select');
    for (var i = 0; i < selects.length; i++) {
        selects[i].disabled = false;
    }
}

function showTask() {
    $('#taskInfo').css('display', 'block');
    $('#updateInfo').css('display', 'block');
    $('#execInfoAll').css('display', 'none');

    $('#edit').css('display', 'inline-block');
}

function hideAll() {
    $('#taskInfo').css('display', 'none');
    $('#updateInfo').css('display', 'none');
    $('#execInfoAll').css('display', 'none');

    $('#edit').css('display', 'none');
}

//Заполнение полей настройками задачи
function fillTask(task, needPlans) {
    $('#name')[0].value = "" + task.task_name;
    $('#dbHost')[0].value = task.database_host;
    $('#dbPort')[0].value = task.database_port;
    $('#inputDBName')[0].value = task.database_name;
    $('#inputLogin')[0].value = task.database_login;
    $('#inputPassword')[0].value = task.database_password;

    //Запрос и вывод списка планов для изменения/просмотра
    if (needPlans) {
        getPlansInfo(true);
    } else {
        fillRoutines(task.routines);
    }
};

function getTaskParams() {
    isSelected.task_name = $('#name')[0].value;
    isSelected.database_host = $('#dbHost')[0].value;
    isSelected.database_port = parseInt($('#dbPort')[0].value);
    isSelected.database_name = $('#inputDBName')[0].value;
    isSelected.database_login = $('#inputLogin')[0].value;
    isSelected.database_password = $('#inputPassword')[0].value;
    //console.log(isSelected);
};

//Заполнение значений объекта задание значениями считанными из формы
function getTaskUpdateParams() {
    var bdChecked = false;
    var columnsChecked = false;
    var plansChecked = false;

    var syncdirChecked = false;
    var tdOrMediaChecked = false;

    isSelected.routines = {};
    if ($('#updateBD')[0].checked) {
        bdChecked = true;
        isSelected.routines.database = {};
        isSelected.routines.database.plans = {};
        isSelected.routines.database.plans.ids = [];
        isSelected.routines.database.plans.columns = [];

        var columns = $('#fieldsetColumns')[0].elements;
        for (var i = 1; i < columns.length; i++) {
            if (columns[i].checked) {
                isSelected.routines.database.plans.columns.push(columns[i].parentNode.id);
                columnsChecked = true;
            }
        }

        var plans = $('#fieldsetPlans')[0].elements;
        for (var i = 1; i < plans.length; i++) {
            if (plans[i].checked) {
                var plan = {};
                plan.uid = plans[i].parentNode.id;
                plan.name = plans[i].id;
                /*var plan = plans[i].parentNode.id;*/
                isSelected.routines.database.plans.ids.push(plan);
                plansChecked = true;
            }
        }

        isSelected.routines.database.update_parameters = {};
        isSelected.routines.database.update_parameters.update_method = $('#typeBDUpdate').find(":selected")[0].value;
        isSelected.routines.database.update_parameters.update_params = [];
        isSelected.routines.database.update_parameters.update_params[0] = parseInt($('#cronHourBD').find(":selected")[0].value);
        isSelected.routines.database.update_parameters.update_params[1] = parseInt($('#cronMinuteBD').find(":selected")[0].value);
        isSelected.routines.database.update_parameters.update_params[2] = parseInt($('#cronSecondBD').find(":selected")[0].value);

        if ($('#updateFTP')[0].checked) {
            isSelected.routines.models = {};
            isSelected.routines.models.update_parameters = {};
            isSelected.routines.models.update_parameters.update_method = $('#typeBDUpdate').find(":selected")[0].value;
            isSelected.routines.models.update_parameters.update_params = [];
            isSelected.routines.models.update_parameters.update_params[0] = parseInt($('#cronHourBD').find(":selected")[0].value);
            isSelected.routines.models.update_parameters.update_params[1] = parseInt($('#cronMinuteBD').find(":selected")[0].value);
            isSelected.routines.models.update_parameters.update_params[2] = parseInt($('#cronSecondBD').find(":selected")[0].value);
        }
    }

    if ($('#updateSyncDir')[0].checked) {
        syncdirChecked = true;
        isSelected.routines.syncdir = {};
        isSelected.routines.syncdir.syncdir_folders = [];


        if ($('#updateMedia')[0].checked) {
            tdOrMediaChecked = true;
            isSelected.routines.syncdir.syncdir_folders.push('/eaintegration');
        }
        if ($('#updateTypeDef')[0].checked) {
            tdOrMediaChecked = true;
            isSelected.routines.syncdir.syncdir_folders.push('/typedef');
        }
        if ($('#updateTracking')[0].checked) {
            tdOrMediaChecked = true;
            isSelected.routines.syncdir.syncdir_folders.push('/tracking');
        }
        isSelected.routines.syncdir.update_parameters = {};
        isSelected.routines.syncdir.update_parameters.update_method = $('#typeSDUpdate').find(":selected")[0].value;
        isSelected.routines.syncdir.update_parameters.update_params = [];
        isSelected.routines.syncdir.update_parameters.update_params[0] = parseInt($('#cronHourSD').find(":selected")[0].value);
        isSelected.routines.syncdir.update_parameters.update_params[1] = parseInt($('#cronMinuteSD').find(":selected")[0].value);
        isSelected.routines.syncdir.update_parameters.update_params[2] = parseInt($('#cronSecondSD').find(":selected")[0].value);
    }

    if ((bdChecked && columnsChecked && plansChecked) || (syncdirChecked && tdOrMediaChecked)) {
        console.log((bdChecked && columnsChecked && plansChecked) + " " + (syncdirChecked && tdOrMediaChecked) + " " + ((bdChecked && columnsChecked && plansChecked) || (syncdirChecked && tdOrMediaChecked)));
        console.log(JSON.stringify(isSelected));
        if (!editingMode) {
            addTaskInfo(JSON.stringify(isSelected));
        } else {
            editTaskInfo(JSON.stringify(isSelected));
        }
        return true;
    } else {
        console.log((bdChecked && columnsChecked && plansChecked) + " " + (syncdirChecked && tdOrMediaChecked) + " " + ((bdChecked && columnsChecked && plansChecked) || (syncdirChecked && tdOrMediaChecked)));
        return false;
    }
};

//Заполнение полей настройки параметров обновления
function parseRoutines() {
    $("#select-all-columns")[0].checked = false;
    $("#select-all-plans")[0].checked = false;
    var routines = isSelected.routines;
    //console.log(routines);
    if (routines.database != undefined && routines.database != null && routines.database !== 'None') {
        //console.log('Database is selected');
        //$('#updateBD').click();
        /*if (!$('#updateBD')[0].checked) {
            $('#updateBD').click();
        }*/
        $('#updateBD')[0].checked = true;
        $('#InputTables').css("display", 'block');

        showDefaultColumns();

        for (var i = 0; i < routines.database.plans.columns.length; i++) {
            var columnLabel = $('label#' + routines.database.plans.columns[i])[0];
            if (columnLabel != undefined) {
                columnLabel.children[0].checked = true;
                //console.log('Check: ' + routines.database.plans.columns[i]);
            }
        }
        var columnsCount = $('#columns').find(":checked").length;
        if (columnsCount == defaultColumns.length) {
            console.log("Все колонки выделены!");
            $("#select-all-columns")[0].checked = true;
        }

        var div = document.getElementsByClassName("multiselect")[0];
        div.scrollTop = 0;

        for (var i = 0; i < routines.database.plans.ids.length; i++) {
            var planLabel = $('label#' + routines.database.plans.ids[i].uid)[0];
            if (planLabel != undefined) {
                planLabel.children[0].checked = true;
                //console.log('Check: ' + routines.database.plans.ids[i].uid.trim());
                //console.log('Check: ' + routines.database.plans.ids[i].name);
            }
        }
        var plansCount = $('#plans').find(":checked").length;
        if (plansCount == $('#plans')[0].children.length) {
            console.log("Все планы выделены!");
            $("#select-all-plans")[0].checked = true;
        }

        var div = document.getElementsByClassName("multiselect")[1];
        div.scrollTop = 0;

        $("#" + routines.database.update_parameters.update_method + 'BD')[0].selected = true;
        $("#" + routines.database.update_parameters.update_params[0] + 'hour' + 'BD')[0].selected = true;
        $("#" + routines.database.update_parameters.update_params[1] + 'minute' + 'BD')[0].selected = true;
        $("#" + routines.database.update_parameters.update_params[2] + 'second' + 'BD')[0].selected = true;


        if (routines.models != undefined && routines.models != null && routines.models !== 'None') {
            //console.log('Models is selected');
            //$("#updateFTP").click();
            if (!$('#updateFTP')[0].checked) {
                $("#updateFTP")[0].checked = true;
            }
        }
    }
    if (routines.syncdir != undefined && routines.syncdir != null && routines.syncdir !== 'None') {
        //console.log('Syncdir is selected');
        //$("#updateSyncDir")[0].click();
        /*if (!$('#updateSyncDir')[0].checked) {
            $("#updateSyncDir")[0].click();
        }*/
        $("#updateSyncDir")[0].checked = true;
        $('#cronTypeDef').css("display", 'block');
        for (var i = 0; i < routines.syncdir.syncdir_folders.length; i++) {
            var type = routines.syncdir.syncdir_folders[i];
            switch (type){
                case '/eaintegration': {
                    if (!$('#updateMedia')[0].checked) {
                        $('#updateMedia')[0].checked = true;
                    };
                    break;
                }
                case '/tracking':{
                    if (!$('#updateTracking')[0].checked) {
                        $('#updateTracking')[0].checked = true;
                    };
                    break;
                }
                case '/typedef':{
                    if (!$('#updateTypeDef')[0].checked) {
                        $('#updateTypeDef')[0].checked = true;
                    };
                    break;
                }
            }
        }

        $("#" + routines.syncdir.update_parameters.update_method + 'SD')[0].selected = true;
        $("#" + routines.syncdir.update_parameters.update_params[0] + 'hour' + 'SD')[0].selected = true;
        $("#" + routines.syncdir.update_parameters.update_params[1] + 'minute' + 'SD')[0].selected = true;
        $("#" + routines.syncdir.update_parameters.update_params[2] + 'second' + 'SD')[0].selected = true;
    }

    if (!editingMode) {
        makeInputDisabled();
    } else {
        makeInputEnabled();
    }

    var div = document.getElementsByClassName("modal-body")[0];
    div.scrollTop = 0;
};

function createPlanDiv(id, name) {
    var div = document.createElement('div');
    div.innerHTML = '<label id=\"' + id + '\"><input type=\"checkbox\" name=\"columns\" id=\"' + name + '\" value=\"' + name + '\"\/>' + name + '<\/label>';
    return div;
};

//Заполнение параметров обновления сведениями задачи
function fillRoutines(routines) {
    if (routines != undefined) {
        //console.log(routines);
        if (routines.database != undefined && routines.database != null && routines.database !== 'None') {
            //console.log('Database is selected');
            /*if (!$('#updateBD')[0].checked) {
                $('#updateBD').click();
            }*/
            $('#updateBD')[0].checked = true;
            $('#InputTables').css("display", 'block');

            $("#columns")[0].innerHTML = "";
            for (var i = 0; i < routines.database.plans.columns.length; i++) {
                var child = createPlanDiv(routines.database.plans.columns[i], defaultColumns[routines.database.plans.columns[i]]);
                if (child != undefined) {
                    child.children[0].children[0].checked = true;
                    $('#columns')[0].appendChild(child);
                    //console.log('Check: ' + routines.database.plans.columns[i]);
                }
            }
            var columnsCount = $('#columns').find(":checked").length;
            if (columnsCount == routines.database.plans.columns.length) {
                console.log("Все колонки выделены!");
                $("#select-all-columns")[0].checked = true;
            }

            $("#plans")[0].innerHTML = "";
            for (var i = 0; i < routines.database.plans.ids.length; i++) {
                var child = createPlanDiv(routines.database.plans.ids[i].uid, routines.database.plans.ids[i].name);
                if (child != undefined) {
                    child.children[0].children[0].checked = true;
                    $('#plans')[0].appendChild(child);
                    //console.log('Check: ' + routines.database.plans.ids[i].uid.trim());
                    //console.log('Check: ' + routines.database.plans.ids[i].name);
                }
            }
            var plansCount = $('#plans').find(":checked").length;
            if (plansCount == $('#plans')[0].children.length) {
                console.log("Все планы выделены!");
                $("#select-all-plans")[0].checked = true;
            }

            var div = document.getElementsByClassName("multiselect")[0];
            div.scrollTop = 0;

            $("#" + routines.database.update_parameters.update_method + 'BD')[0].selected = true;
            $("#" + routines.database.update_parameters.update_params[0] + 'hour' + 'BD')[0].selected = true;
            $("#" + routines.database.update_parameters.update_params[1] + 'minute' + 'BD')[0].selected = true;
            $("#" + routines.database.update_parameters.update_params[2] + 'second' + 'BD')[0].selected = true;


            if (routines.models != undefined && routines.models != null && routines.models !== 'None') {
                //console.log('Models is selected');
                if (!$('#updateFTP')[0].checked) {
                    $("#updateFTP")[0].checked = true;
                }
            }
        }
        if (routines.syncdir != undefined && routines.syncdir != null && routines.syncdir !== 'None') {
            //console.log('Syncdir is selected');
            /*if (!$('#updateSyncDir')[0].checked) {
                $("#updateSyncDir")[0].click();
            }*/
            $("#updateSyncDir")[0].checked = true;
            $('#cronTypeDef').css("display", 'block');

            for (var i = 0; i < routines.syncdir.syncdir_folders.length; i++) {
                var type = routines.syncdir.syncdir_folders[i];
                switch (type){
                    case '/eaintegration': {
                        if (!$('#updateMedia')[0].checked) {
                            $('#updateMedia')[0].checked = true;
                        };
                        break;
                    }
                    case '/tracking':{
                        if (!$('#updateTracking')[0].checked) {
                            $('#updateTracking')[0].checked = true;
                        };
                        break;
                    }
                    case '/typedef':{
                        if (!$('#updateTypeDef')[0].checked) {
                            $('#updateTypeDef')[0].checked = true;
                        };
                        break;
                    }
                }
                /*if (type == '/eaintegration') {
                    if (!$('#updateMedia')[0].checked) {
                        $('#updateMedia')[0].checked = true;
                    }
                } else {
                    if (!$('#updateTypeDef')[0].checked) {
                        $('#updateTypeDef')[0].checked = true;
                    }
                }*/
            }

            $("#" + routines.syncdir.update_parameters.update_method + 'SD')[0].selected = true;
            $("#" + routines.syncdir.update_parameters.update_params[0] + 'hour' + 'SD')[0].selected = true;
            $("#" + routines.syncdir.update_parameters.update_params[1] + 'minute' + 'SD')[0].selected = true;
            $("#" + routines.syncdir.update_parameters.update_params[2] + 'second' + 'SD')[0].selected = true;
        }

        if (!editingMode) {
            makeInputDisabled();
        } else {
            makeInputEnabled();
        }

        var div = document.getElementsByClassName("modal-body")[0];
        div.scrollTop = 0;
    }
};

//Очистка полей для ввода настроек задачи
function clearTask() {
    $('#name')[0].value = "";
    $('#dbHost')[0].value = "";
    $('#dbPort')[0].value = "";
    $('#inputDBName')[0].value = "";
    $('#inputLogin')[0].value = "";
    $('#inputPassword')[0].value = "";

    $('#updateBD')[0].checked = false;
    $('#updateFTP')[0].checked = false;

    $('#updateSyncDir')[0].checked = false;
    $('#updateTypeDef')[0].checked = false;
    $('#updateMedia')[0].checked = false;
    $('#updateTracking')[0].checked = false;

    makeAllColumnsChecked(false);

    var plans = document.getElementById("plans");
    for (var i = 0; i < plans.children.length; i++) {
        plans.children[i].children[0].children[0].checked = false;
    }

    $('#cronTypeDef').css("display", 'none');
    $('#InputTables').css("display", 'none');

    $("#" + 'cron' + 'BD')[0].selected = true;
    $("#" + 0 + 'hour' + 'BD')[0].selected = true;
    $("#" + 0 + 'minute' + 'BD')[0].selected = true;
    $("#" + 0 + 'second' + 'BD')[0].selected = true;

    $("#" + 'cron' + 'SD')[0].selected = true;
    $("#" + 0 + 'hour' + 'SD')[0].selected = true;
    $("#" + 0 + 'minute' + 'SD')[0].selected = true;
    $("#" + 0 + 'second' + 'SD')[0].selected = true;

    $("#select-all-columns")[0].checked = false;
    $("#select-all-plans")[0].checked = false;
    removeErrorStyles();
};

function removeErrorStyles(){
    $('#mainform input').each(function() {
        $(this)
            .removeClass('valid')
            .removeClass('error')
            .css('border-color', '')
            .parent()
            .removeClass('has-error')
            .removeClass('has-success')
            .find('.form-error') // remove inline error message
            .remove();
    });
}

//
function makeAllColumnsChecked(isChecked) {
    var columns = document.getElementById("columns");
    for (var i = 0; i < columns.children.length; i++) {
        columns.children[i].children[0].children[0].checked = isChecked;
    }
}

//Заполнение значений часов, минут, секунд
function updateCronIntervalUi(type) {
    var hour = document.getElementById("cronHour" + type);
    for (var i = 0; i <= 23; ++i) {
        var option = document.createElement('option');
        option.text = option.value = i;
        option.id = i + 'hour' + type;
        hour.options[hour.options.length] = option;
    }
    hour.value = 0;

    var minute = document.getElementById("cronMinute" + type);
    for (var i = 0; i <= 59; ++i) {
        var option = document.createElement('option');
        option.text = option.value = i;
        option.id = i + 'minute' + type;
        minute.options[minute.options.length] = option;
    }
    minute.value = 0;

    var second = document.getElementById("cronSecond" + type);
    for (var i = 0; i <= 59; ++i) {
        var option = document.createElement('option');
        option.text = option.value = i;
        option.id = i + 'second' + type;
        second.options[second.options.length] = option;
    }
    second.value = 0;
};

function showDefaultColumns() {
    $('#columns')[0].innerHTML = "";

    for(key in defaultColumns) {
        $('#columns')[0].appendChild(createPlanDiv(key, defaultColumns[key]));
    }
}