/*Author: Yulia Savinkova
 Author: Alex Pronin
 Date: 15.01.14*/

//Выбранное задание из списка
var isSelected;
var firstStep = true;
//Цикличный запрос статуса выполнения заданий
var intervalID;
var intervalRotate;
//Флаг выполнения задания
var isExecuted;
var editingMode = false;
var addingMode = false;

var executing = false;

//TODO: для теста
var defaultTask = {
    task_name: 'task2',
    database_host: '192.168.10.32',
    database_port: '5432',
    database_name: 'integraplanetearth',
    database_login: 'postgres',
    database_password: 'acuario'
};
//Список колонок
var defaultColumns;

$(function () {
    //Заполнение параметров cron и interval
    updateCronIntervalUi('SD');
    updateCronIntervalUi('BD');

    makeButtonsInvisible();

    hideAll();

    //Запрос списка колонок и начального списка задач
    getColumns();

    $('#updateSyncDir').click(showSyncDirSettings);
    $('#updateBD').click(showBDSettings);
});

function deleteTaskInfo() {
    //console.log("Delete " + isSelected.task_uid);
    if(isSelected.task_uid != undefined && isSelected != null && !addingMode){
        deleteTask(isSelected.task_uid);
    } else {
        alert("Выберите, пожалуйста, задачу из списка!");
    }

};

function getDeleteTaskCallback(data) {
    //console.log("Task " + data + " deleted");
    getAllTasks();
};

function getColumnsListCallback(data) {
    //console.log(data);
    if (data != undefined && data != null && data !== 'None') {
        if (data.err_msg == undefined) {
            defaultColumns = data;
        } else {
            console.log(data.err_msg);
        }
    }
    getAllTasks();
};

function getAllTasksCallback(data) {
    console.log(data);
    if (data !== undefined && data !== null && data !== 'None') {
        if (data.err_msg == undefined) {
            $('#listTasksUi')[0].innerHTML = "";
            clearTask();
            if (data.length != 0) {
                isSelected = data[0];
                for (var i = 0; i < data.length; i++) {
                    var li = document.createElement('li');
                    li.innerHTML = "<a onclick=\"get_task('" + data[i].task_uid + "')\">" + data[i].task_name + "</a>";
                    li.id = data[i].task_uid;
                    $('#listTasksUi')[0].appendChild(li);
                }
                $("#" + isSelected.task_uid)[0].children[0].click();
            } else {
                hideAll();
            }
        } else {
            hideAll();
            console.log(data.err_msg);
            alert(data.err_msg);
        }
    }
};

function get_task(id) {
    //console.log("Get info task " + id);
    clearInterval(intervalID);
    executing = false;
    if (editingMode) {
        alert("Для просмотра другой задачи закончите или отмените редактирование!");
    } else {
        if(addingMode){
            alert("Для просмотра другой задачи закончите или отмените создание!");
        } else {
            $('.circle, .circle1').toggleClass('stop');
            $("#ProgressBarCircle")[0].style.display = 'none';
            if (isSelected != null && isSelected != undefined) {
                $('#' + isSelected.task_uid).removeClass('active');
            }
            getTaskInfoByUID(id);
        }
    }

    /*if (!editingMode && !addingMode) {
        if (isSelected != null && isSelected != undefined) {
            $('#' + isSelected.task_uid).removeClass('active');
        }
        getTaskInfoByUID(id);
    } else {
        if (editingMode) {
            alert("Для просмотра другой задачи закончите или отмените редактирование!");
        } else {
            if(addingMode){
                alert("Для просмотра другой задачи закончите или отмените создание!");
            } else {

            }
        }
    }*/

}

function getTaskCallback(data) {
    //console.log(data);
    if (data !== undefined && data !== null && data !== 'None') {
        $('#' + data.task_uid).addClass('active');
        makeInputEnabled();
        clearTask();
        isSelected = data;
        showTask();
        fillTask(isSelected, false);
    }
};

//Выполнение задания, ведение лога
function executeTasks() {

    if(!executing){
        executing = true;
        if (isSelected != null && isSelected != undefined) {
            $('#' + isSelected.task_uid).removeClass('active');
        }

        $('#title')[0].innerHTML = "Выполнение задания";
        $('#execInfo')[0].innerHTML = "";

        $('#taskInfo').css('display', 'none');
        $('#updateInfo').css('display', 'none');
        $('#execInfoAll').css('display', 'block');

        makeButtonsInvisible();
        executeTasksRequest();
    } else {
        console.log("Выполнение задач уже было запущено!");
    }
};

function executeTasksCallback(data) {
    data = true;
    if (data != undefined && data != null && data !== 'None') {
        console.log(data);
        if (data.err_msg == undefined && data.err_msg == null) {
            isExecuted = true;
        } else {
            clearInterval(intervalID);
            executing = false;
            console.log("Error");
            isExecuted = false;
            console.log(data.err_msg);
            alert(data.err_msg);
        }
    } else {
        isExecuted = true;
    }
    if (isExecuted) {
        console.log("Start");
        intervalID = setInterval(getTaskStatus, 600);
        //$("#ProgressBarCircle")[0].style.display = 'inline-block';
        //$('.circle, .circle1').removeClass('stop');
    }
};

function getTaskStatusCallback(data) {
    console.log(data);
    if (data != undefined && data != null && data !== 'None') {
        if (data.completed) {
            $('.circle, .circle1').toggleClass('stop');
            $("#ProgressBarCircle")[0].style.display = 'none';
            clearInterval(intervalID);
            executing = false;
            $('#execInfo')[0].innerHTML = data.msg;
        }
        $('#execInfo')[0].innerHTML = data.msg;
        var elem = document.getElementsByClassName('modal-body')[0];
        elem.scrollTop = elem.scrollHeight;
    }
};

function addTask() {
    $('.circle, .circle1').toggleClass('stop');
    $("#ProgressBarCircle")[0].style.display = 'none';
    if (isSelected != null && isSelected != undefined) {
        $('#' + isSelected.task_uid).removeClass('active');
    }
    showDefaultColumns();
    clearTask();
    makeInputEnabled();
    makeAllColumnsChecked(true);
    firstStep = true;
    /*//TODO: временно, для теста
    editingMode = true;
    fillTask(defaultTask, false);
    editingMode = false;*/

    $('#title')[0].innerHTML = "Настройка задания. Шаг 1 из 2";

    $('#taskInfo').css('display', 'block');
    $('#updateInfo').css('display', 'none');
    $('#execInfoAll').css('display', 'none');

    $('#previousStep').css('display', 'none');
    $('#cancel').css('display', 'inline-block');
    $('#save').css('display', 'inline-block');
    $('#edit').css('display', 'none');
    addingMode = true;
};

function previousStepFunc() {
    //console.log("Previous step!");
    $('#title')[0].innerHTML = "Настройка задания. Шаг 1 из 2";
    $('#taskInfo').css('display', 'block');
    $('#updateInfo').css('display', 'none');
    $('#execInfoAll').css('display', 'none');
    $('#edit').css('display', 'none');

    firstStep = true;

    fillTask(isSelected, false);
};

function saveFunc() {
    //console.log("Next step!");
    if (firstStep) {
        if (!editingMode) {
            isSelected = {};
            getTaskParams();
            getPlansInfo(false);
        } else {
            getTaskParams();
            getPlansInfo(true);
            makeInputEnabled();
        }

        $('#title')[0].innerHTML = "Настройка задания. Шаг 2 из 2";
        $('#taskInfo').css('display', 'none');
        $('#updateInfo').css('display', 'block');
        $('#execInfoAll').css('display', 'none');

        $('#previousStep').css('display', 'inline-block');
        firstStep = false;
    } else {
        var ok = getTaskUpdateParams();
        if (ok) {
            $('#title')[0].innerHTML = "Параметры задания";
            cancelFunc();
        } else {
            //TODO: вывод сообщения
            alert("");
        }
    }
};

function getAddTaskCallback(data) {
    console.log(data);
    getAllTasks();
}

//Запрос списка планов
function getPlansInfo(isReady) {
    var kwargs = "&database_host=" + isSelected.database_host;
    kwargs += "&database_port=" + isSelected.database_port;
    kwargs += "&database_name=" + isSelected.database_name;
    kwargs += "&database_login=" + isSelected.database_login;
    kwargs += "&database_password=" + isSelected.database_password;
    if (isReady) {
        kwargs += "&action=" + "update";
    } else {
        kwargs += "&action=" + "insert";
    }
    getPlans(kwargs, isReady);
};

function getPlansCallback(plans, isReady) {
    //console.log(plans);
    if (plans != undefined && plans != null && plans !== 'None') {
        if (plans.err_msg == undefined) {
            $('#plans')[0].innerHTML = "";
            function appendPlan(plan) {
                $('#plans')[0].appendChild(createPlanDiv(plan.uid, plan.name));
            };

            plans.forEach(appendPlan);

            if (isReady) {
                parseRoutines();
            }
        } else {
            console.log(plans.err_msg);
            alert(plans.err_msg);
            previousStepFunc();
        }
    }
};

function editFunc() {
    editingMode = true;
    makeInputEnabled();
    firstStep = true;
    $('#title')[0].innerHTML = "Настройка задания. Шаг 1 из 2";

    $('#taskInfo').css('display', 'block');
    $('#updateInfo').css('display', 'none');
    $('#execInfoAll').css('display', 'none');

    $('#edit').css('display', 'none');
    $('#previousStep').css('display', 'none');
    $('#cancel').css('display', 'inline-block');
    $('#save').css('display', 'inline-block');
}

function getEditTaskCallback(e) {
    console.log("Err:" + e.err_msg);
    getAllTasks();
}

function cancelFunc() {
    //console.log("Cancel!");
    if (editingMode) {
        editingMode = false;
        console.log("EditingMode = false!");
    } else {
        if (addingMode) {
            addingMode = false;
            console.log("AddingMode = false!");
        }
    }
    makeButtonsInvisible();
    showTask();
};

function cancel() {
    addingMode = false;
    editingMode = false;
    isSelected = {};
    getAllTasks();
    makeButtonsInvisible();
    showTask();
}

function showBDSettings(e) {
    if ($(this).is(':checked')) {
        //console.log(e.target.name + ' checked');
        $('#InputTables').css("display", 'block');
    }
    else {
        //console.log(e.target.name + ' unchecked');
        $('#InputTables').css("display", 'none');
    }
};

function showSyncDirSettings(e) {
    if ($(this).is(':checked')) {
        //console.log(e.target.name + ' checked');
        $('#cronTypeDef').css("display", 'block');
    }
    else {
        //console.log(e.target.name + ' unchecked');
        $('#cronTypeDef').css("display", 'none');
    }
};

//check all
function toggle(source) {
    if (source.id == 'select-all-columns') {
        checkboxes = document.getElementById('columns').children;
    } else {
        if (source.id == 'select-all-plans') {
            checkboxes = document.getElementById('plans').children;
        }
    }
    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].children[0].children[0].checked = source.checked;
    }
};
