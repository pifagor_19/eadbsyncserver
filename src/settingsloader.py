'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import  json, settings, logging
from sys import exit

jsonObj = dict()

sample_config_format = {
    "credentials": {
        "server_password": "5f4dcc3b5aa765d61d8327deb882cf99",
        "server_login": "admin"
    },
    "tasks": [],
    "local_settings": {
        "database_password": "",
        "database_name": "",
        "database_host": "",
        "database_port": "",
        "database_login": ""
    },
    "server_version": str(settings.SERVER_VERSION),
    "settings": {
        "ftp_homedir": ""
    }
}

def reload_settings():
    global jsonObj
    try:
        fd = open(settings.CONFIG_FILE_NAME).read()
        jsonObj = json.loads(fd)
        if jsonObj['server_version'] != settings.SERVER_VERSION:
            exit('Invalid version of server!')
    except:
        logging.error("Config file not found - creating empty default config")
        with open(settings.CONFIG_FILE_NAME, 'w') as outfile:
            json.dump(settings.sample_config_format, outfile, indent=4, sort_keys=True)
        reload_settings()

def getinfobytag(tag):
    return jsonObj[tag] if tag in jsonObj.keys() else None

def save_local_settings(section_name = 'local_settings', **kwargs):
    for i, v in kwargs.iteritems():
        if i in jsonObj[section_name].keys():
            jsonObj[section_name][i] = v
    with open(settings.CONFIG_FILE_NAME, 'w') as outfile:
        json.dump(jsonObj, outfile, indent=4, sort_keys=True)

def save_tasks(section_name = 'tasks', *args):
    del jsonObj[section_name]
    jsonObj[section_name] = args[0]
    with open(settings.CONFIG_FILE_NAME, 'w') as outfile:
        json.dump(jsonObj, outfile)
