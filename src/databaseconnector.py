'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import psycopg2, logging, settings, copy, traceback
from utils import exc_handler

conn_params = ['database_host', 'database_port', 'database_name', 'database_login', 'database_password']

def get_param(param):
    return s_settings[param] if param in s_settings.keys() else None

# --------------------------------------------------------------------------------------------------------------
# Exceptions
# --------------------------------------------------------------------------------------------------------------

# base class for all execptions
class DBException(Exception):
    pass

# connect error
class DBConnExc(DBException):
    pass

# fetch data error
class DBFetchExc(DBException):
    pass

# --------------------------------------------------------------------------------------------------------------
# Database connector class
# --------------------------------------------------------------------------------------------------------------

class DatabaseConnector(object):
    def __init__(self, **kwargs):
        #global conn_params
        self.Pool = DBConnectionPool()
        self.create_connection(**kwargs)
        #conn_dict = {}
        #for parameter in conn_params:
        #    conn_dict[parameter] = kwargs[parameter]
        ##print conn_dict
        #self.Pool.add_connection('main', **conn_dict)

    # make connection to database
    def create_connection(self, task_identificator = None, **kwargs):
        global conn_params
        # "create new connection %s" % task_identificator
        conn_dict = {}
        for parameter in conn_params:
            conn_dict[parameter] = kwargs[parameter]
        self.Pool.add_connection('main', **conn_dict) if not task_identificator else self.Pool.add_connection(task_identificator, **conn_dict)

    # --------------------------------------------------------------------------------------------------------------

    def remove_connection(self, task_uid = None):
        #print "Remove db connection %s" % task_uid
        self.Pool.remove_connection('main') if not task_uid else self.Pool.remove_connection(task_uid)

    # --------------------------------------------------------------------------------------------------------------

    def close_all(self):
        self.Pool.close_all_connections()

    # --------------------------------------------------------------------------------------------------------------

    def get_connection(self, task_name):
        return self.Pool.get_connection(task_name)

    # --------------------------------------------------------------------------------------------------------------

    def fetch_query(self, task_name, query_string):
        connector = self.get_connection(task_name)
        if not connector:
            return None
        cursor = connector.cursor()
        try:
            cursor.execute(query_string)
        except psycopg2.DatabaseError as e:
            logging.error('An error occured while executing query in task %s with error: "%s"' % (task_name, e))
            # 'An error occured while executing query in task %s with error: "%s"' % (task_name, e)
            self.Pool.drop_connection(task_name)
            return None
        values = cursor.fetchall()
        self.Pool.drop_connection(task_name)
        return values

    # --------------------------------------------------------------------------------------------------------------

    def execute_query(self, task_name, query_string, data = None):
        connector = self.get_connection(task_name)
        if not connector:
            return None
        cursor = connector.cursor()
        try:
            if data is None:
                cursor.execute(query_string)
            else:
                cursor.execute(query_string, data)
                self.Pool.drop_connection(task_name)
        except psycopg2.DatabaseError as e:
            connector.rollback()
            self.Pool.drop_connection(task_name)
            logging.error('An error occured while executing query in task %s with error: "%s"' % (task_name, e))
            #print 'An error occured while executing query in task %s with error: "%s"' % (task_name, e)

# --------------------------------------------------------------------------------------------------------------
# Database connector class - alternative version
# --------------------------------------------------------------------------------------------------------------
class DBModule(object):
    # init connection
    def __init__(self, **kwargs):
        conn_dict = dict()
        for parameter in conn_params:
            conn_dict[parameter] = kwargs[parameter] if parameter in kwargs else None
        self._params = dict(conn_dict)
        self._conn = None

    # open connection
    def open(self):
        self.drop()
        try:
            self._conn = psycopg2.connect("host=%s port=%s dbname=%s user=%s \
                                          password=%s connect_timeout=%d" %
                (self._params['database_host'], self._params['database_port'],
                 self._params['database_name'], self._params['database_login'],
                self._params['database_password'], settings.TIMEOUT_INTERVAL))
            self._conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        except:
            logging.error(traceback.format_exc())
            raise DBConnExc

    # drop connection
    def drop(self):
        if self._conn:
            logging.info("DB close")
            self._conn.close()
        else:
            logging.info("DB init")
        self._conn = None

    # fetch data from db
    def fetch(self, query, data = None):
        if not self._conn: self.open()
        cr = self._conn.cursor()
        try:
            cr.execute(query) if not data else cr.execute(query, data)
            _ret = cr.fetchall()
            return _ret
        except:
            logging.error(traceback.format_exc())
            self.drop()
            raise DBFetchExc

    # execute script
    def execute(self, query, data = None):
        if not self._conn: self.open()
        cr = self._conn.cursor()
        try:
            cr.execute(query) if not data else cr.execute(query, data)
            self._conn.commit()
        except:
            logging.error(traceback.format_exc())
            self.drop()
            raise DBFetchExc

# --------------------------------------------------------------------------------------------------------------
# Connection pool
# --------------------------------------------------------------------------------------------------------------

class DBConnectionPool(object):
    def __init__(self):
        self.connections = {}

    # --------------------------------------------------------------------------------------------------------------

    def add_connection(self, con_uid, **kwargs):
        self.remove_connection(con_uid)
        task_desc = {'desc': dict(kwargs), 'conn': None}
        self.connections[con_uid] = task_desc

    # --------------------------------------------------------------------------------------------------------------

    def get_connection(self, con_uid):
        if self.connections[con_uid]['conn']:
            return self.connections[con_uid]['conn']
        else:
            connection = None
        opts = self.connections[con_uid]['desc']
        try:
            connection = psycopg2.connect("host=%s port=%s dbname=%s user=%s password=%s connect_timeout=%d" %
                (opts['database_host'], opts['database_port'], opts['database_name'], opts['database_login'],
                opts['database_password'], settings.TIMEOUT_INTERVAL))
        except psycopg2.DatabaseError as e:
            #print "Connection error: Unable to connect with host ", opts['database_host'], "Details: %s", e
            logging.error('Unable to connect to the database %s. Details: %s' % (opts['database_host'], e))
        if connection:
            connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            self.connections[con_uid]['conn'] = connection
            return self.connections[con_uid]['conn']
        else:
            return None

    # --------------------------------------------------------------------------------------------------------------

    def remove_connection(self, con_uid):
        self.drop_connection(con_uid)
        if con_uid in self.connections.keys():
            del self.connections[con_uid]

    # --------------------------------------------------------------------------------------------------------------

    def drop_connection(self, con_uid):
        if con_uid in self.connections.keys():
            conn = self.connections[con_uid]['conn']
            if conn: conn.close()
            self.connections[con_uid]['conn'] = None
