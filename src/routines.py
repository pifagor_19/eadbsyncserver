﻿'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import querycomposer as qc, utils, settings, logging, os.path, traceback, settingsloader

# ------------------------------------------------------------------------------------------------------------
#   Update class
# ------------------------------------------------------------------------------------------------------------

class Update(object):
    # create update params obj
    def __init__(self, **kwargs):
        self._method = kwargs["update_method"]
        self._params = kwargs["update_params"]

    # get update method
    def method(self):
        return self._method

    # get update params
    def params(self):
        return self._params

# ------------------------------------------------------------------------------------------------------------
#   AbstractRoutine class
# ------------------------------------------------------------------------------------------------------------

class AbstractRoutine(object):
    # init routine
    def __init__(self, session, **kwargs):
        self._session = session
        for k,v in kwargs.items():
            setattr(self, k, v)

    # abstract routine
    def _type(self):
        return "abstract"

    # get update params
    def _update(self):
        _p = getattr(self, "update_parameters")
        return Update(**_p)

# ------------------------------------------------------------------------------------------------------------
#   ModelsRoutine class
# ------------------------------------------------------------------------------------------------------------

class ModelsRoutine(AbstractRoutine):
    # get type of routine
    def _type(self):
        return "models"

    # retrieve models from FTP
    def execute(self, table_ids, callback = None):
        if callable(callback):
            callback("<li>Обновление моделей планов</li>")
        ftp = self._session.ftp()
        ftp_home_dir = settingsloader.getinfobytag('settings')['ftp_homedir']
        dest_path = ftp_home_dir + ftp.media_root + '/'
        plan_uids = [item['uid'] for item in table_ids]
        logging.info('Executing model routine')
        stmt = qc.get_select_condition_statement('building_uid, model_uid', 'integra_buildings_view', 'building_uid in ('
                                                 + ','.join("'" + item + "'" for item in plan_uids)
                                                 + ') AND model_uid IS NOT NULL ORDER BY building_uid')
        remote_buildings_list = self._session.query(stmt)
        remote_dict = dict(remote_buildings_list)
        result_dict = dict()
        for _b, _p in remote_dict.iteritems():
            _file = str(dest_path + _b + '/' + _p)
            if not os.path.isfile(_file):
                result_dict[_b] = _p

        logging.info('Downloading models to FTP')

        if callable(callback):
            callback("<li>Количество скачиваемых моделей: %d</li>" % len(result_dict))

        logging.info("Total amount of downloading plans: %s" % len(remote_dict if settings.FORCE_UPDATE_FTP else result_dict))

        retr_plans = lambda plans: self._session.download(**plans)
        retr_plans(remote_dict) if settings.FORCE_UPDATE_FTP else retr_plans(result_dict)

        return True

    # erase obsolete model files
    def clean(self, callback = None):
        if callable(callback):
            callback("<li>Удаление неиспользуемых моделей</li>")
        logging.info('Cleaning up FTP')
        utils.set_homedir(self._session.ftp().get_mediaroot())
        stmt = qc.get_select_condition_statement('building_uid, model_uid', 'integra_buildings', 'model_uid IS NOT NULL')
        self._session.query(stmt, _main = True)
        db_models = dict(self._session.query(stmt, _main = True))
        utils.erase_obsolete_models(**db_models)
        return True

# ------------------------------------------------------------------------------------------------------------
#   SyncdirRoutine class
# ------------------------------------------------------------------------------------------------------------

class SyncdirRoutine(AbstractRoutine):
    # get type of routine
    def _type(self):
        return "syncdir"

    # update typedef and media
    def execute(self, callback = None):
        logging.info('Updating typedefs')
        if callable(callback):
            callback("<li>Обновление определений типов и медиаресурсов</li><ul type=\"circle\">")
        res = self._session.upd_res(callback, *getattr(self, 'syncdir_folders'))
        logging.info("update status = %s" % res)
        if callable(callback):
            text = "</ul><li>Ресурсы успешно обновлены</li>" if res else "</ul><b>При обновлении ресурсов произошла ошибка</b>"
            callback(text)
        if res == -1:
            raise Exception
            #return False
        return True

# ------------------------------------------------------------------------------------------------------------
#   DatabaseRoutine class
# ------------------------------------------------------------------------------------------------------------

class DatabaseRoutine(AbstractRoutine):
    # get type of routine
    def _type(self):
        return "database"

    # sync databases
    def execute(self, callback = None):
        try:
            if callable(callback):
                callback("<li>Обновление базы данных</li>")
            logging.info('Updating database')
            plans = getattr(self, 'plans')
            table_name = 'integra_buildings'
            rem_tbl_name = 'integra_buildings_view'
            pr_key = 'building_uid'
            columns = plans['columns']
            sync_uids = [item['uid'] for item in plans['ids']]
            select_stmt = qc.get_select_statement(pr_key, 'integra_buildings')
            local_ids_tuples = self._session.query(select_stmt, _main = True)
            local_ids = [item[0] for item in local_ids_tuples]

            action_dict = dict()
            action_dict['insert'] = [item for item in sync_uids if item not in local_ids]
            action_dict['update'] = [item for item in sync_uids if item in local_ids]
            statement = qc.get_table_columns(table_name)
            local_column_lst = self._session.query(statement, _main = True)

            if callable(callback):
                callback("<li>Количество добавляемых объектов: %d, обновляемых объектов: %d</li>" % (len(action_dict['insert']), len(action_dict['update'])))

            logging.info("Objects to insert: %d, update %d" % (len(action_dict['insert']), len(action_dict['update'])))

            # insert items to db
            def insert_item():
                if not len(action_dict['insert']):
                    return False
                logging.info('Inserting new items')
                if callable(callback):
                    callback("<li>Добавление новых планов</li>")
                statement = qc.get_table_columns(rem_tbl_name)
                rem_column_lst = self._session.query(statement)
                result_column_list = list(set(local_column_lst) & set(rem_column_lst))
                cond = pr_key + ' IN( ' + ','.join("'" + str(item) + "'" for item in action_dict['insert']) + ' )'
                statement = qc.get_select_condition_statement(','.join(i[0] for i in result_column_list), rem_tbl_name, cond)
                insert_data = self._session.query(statement)
                for ins_item in insert_data:
                    statement = qc.get_insert_query('integra_buildings', result_column_list)
                    self._session.query(statement, _data = ins_item, _fetch = False, _main = True)

            # update existing plans
            def update_item():
                if not len(action_dict['update']):
                    return False
                logging.info('Updating tables')
                if callable(callback):
                    callback("<li>Обновление существующих планов</li>")
                statement = qc.get_table_columns('integra_buildings_view')
                _table_columns = self._session.query(statement)
                _res = [item[0] for item in _table_columns]
                _res.remove('building_uid')
                colons = list( set(columns) & set(_res) )
                cond = pr_key + ' IN( ' + ','.join("'" + str(item) + "'" for item in action_dict['update']) + ' )'
                statement = qc.get_select_condition_statement(pr_key + ',' + ','.join(k for k in colons), rem_tbl_name, cond)
                update_data = self._session.query(statement)
                dictionary = dict()
                for item in update_data:
                    statement = qc.updparamsdate_query('integra_buildings', pr_key, item[0], *colons)
                    self._session.query(statement, _data = list(item[1:]),_fetch = False, _main = True)
            
            # sync eaintegration servers with plans    
            def sync_servers():
                s_columns = [('address',), ('certificate',),
                            ('username',), ('password',),
                            ('interval',), ('bias',), ('is_reserved',)]
                _needReserved = "reserved_server_id" in columns
                _needMain = "server_id" in columns
                if not _needMain and not _needReserved:
                    return
                if callable(callback):
                    callback("<li>Синхронизация серверов интеграции</li>")
                cond = ','.join("'" + str(item) + "'" for item in (action_dict['insert'] + action_dict['update']))
                _stmt = qc.get_servers(cond, _needMain, _needReserved)
                _rservers = self._session.query(_stmt)
                _stmt = qc.get_select_statement('sid, address', 'ea_servers')
                _lservers = dict(self._session.query(_stmt, _main = True))
                for item in _rservers:
                    _sid = 0
                    if item[1] not in _lservers.values():
                        _stmt = qc.insertServer(*item[1:])
                        self._session.query(_stmt, _main = True, _fetch = False)
                    plan_uid = item[0]
                    address = item[1]
                    is_Reserved = item[7]
                    _stmt = qc.update_servers(plan_uid, address, is_Reserved)
                    self._session.query(_stmt, _main = True, _fetch = False)

            update_item()
            insert_item()
            sync_servers()

            logging.info('Updating database completed. Generating hierarchy')

            if callable(callback):
                callback("<li>Обновление базы данных завершено. Обновление иерархии объектов</li>")

            self._session.query(qc.updparamsdate_hierarchy_script(), _fetch = False, _main = True)
            #self._session.query("VACUUM integra_buildings;", _fetch = False, _main = True)
            return True
        except:
            logging.error(traceback.format_exc())
            raise Exception

    # get plan uid's
    def get_plan_uids(self):
        plans = getattr(self, 'plans')
        return plans['ids']
