'''
This source file is part of eily.acuario sync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

from psycopg2 import connect
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import os, json, subprocess, utils, logging

# --------------------------------------------------
# exception definitions
# --------------------------------------------------

# Base class
class InitDbExc(Exception):
    pass
# template unavailable exception
class TemplateUnavailable(InitDbExc):
    pass
# unable to run pg_restore
class RestoreBackupExc(InitDbExc):
    pass
# unable to execute script
class ScriptExecExc(InitDbExc):
    pass
# connection error
class ConnectErrorExc(InitDbExc):
    pass
# database allready exists error
class DBAlreadyExistsExc(InitDbExc):
    pass

# Database connection options
default_database_name = 'postgres'

# location of file with primary sql
sql_script_file = 'dbscriptsimplified.sql'

# script routines
timeout_interval = 5
main_connection = None
new_db_connection = None

# config object
config_json = None

def execute_process():
    global new_db_connection, main_connection, config_json
    db_object = config_json['database']
    options = config_json['options']
    os.environ['PGPASSWORD'] = db_object['database_password']
    try:
        if options['use_integra_borders']:
            args = ["pg_restore", "-d", db_object['database_name'], "-h",
                    db_object['database_host'], "-p", db_object['database_port'],
                    "-U", db_object['database_login'], "-wax", "-F", "t",
                    "--table=integra_borders", "integra_borders.backup"]
            proc = subprocess.Popen(args, stdin  = None, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        if options['use_integra_features']:
            args = ["pg_restore", "-d", db_object['database_name'], "-h",
                    db_object['database_host'], "-p", db_object['database_port'],
                    "-U", db_object['database_login'], "-wax", "-F", "t",
                    "--table=integra_features", "integra_features.backup"]
            proc1 = subprocess.Popen(args, stdin  = None, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    except:
        logging.error('Unable to restore tables from backup')
        raise RestoreBackupExc
    logging.info("Restore started")

def start_sync_db():
    global new_db_connection, main_connection
    db_object = config_json['database']
    ftp_object = config_json['ftp']
    stmt = "CREATE DATABASE %s WITH OWNER = postgres TEMPLATE template_postgis" % db_object['database_name']
    cursor = main_connection.cursor()
    try:
        cursor.execute(stmt)
    except:
        logging.error("Unable to create desired database")
        raise TemplateUnavailable
    stmt = "SELECT 1 FROM pg_database WHERE datistemplate = false AND datname = '%s'" % db_object['database_name']
    cursor.execute(stmt)
    if not cursor.fetchone():
        raise TemplateUnavailable
    else:
        try:
            new_db_connection = connect("host=%s port=%s dbname=%s user=%s password=%s connect_timeout=%d"
                                % (db_object['database_host'],
                                    db_object['database_port'],
                                    db_object['database_name'],
                                    db_object['database_login'],
                                    db_object['database_password'],
                                    timeout_interval))
        except:
            logging.error("Unable to connect to created database")
            raise ConnectErrorExc
        new_db_connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        new_cursor = new_db_connection.cursor()
        sql_script = open(sql_script_file).read()
        try:
            new_cursor.execute(sql_script)
            conn_string = str(ftp_object['ftp_host']) + str(':') + str(ftp_object['ftp_port'])
            stmt = "INSERT INTO ea_ftps(address, username, password) \
                    VALUES('%s', '%s', '%s');" % (conn_string,
                                                  ftp_object['ftp_login'],
                                                  ftp_object['ftp_pass'])
            new_cursor.execute(stmt)
        except Exception as e:
            logging.error("An error occured while executing script: %s" % e)
            raise ScriptExecExc
        execute_process()
        new_cursor.close()
        cursor.close()

def init(config_json_object):
    global main_connection, config_json
    config_json = json.loads(config_json_object)
    database_obj = config_json['database']
    try:
        main_connection = connect("host=%s port=%s dbname=%s user=%s \
                            password=%s connect_timeout=%d"
                            % (database_obj['database_host'],
                                database_obj['database_port'],
                                default_database_name,
                                database_obj['database_login'],
                                database_obj['database_password'],
                                timeout_interval))
    except:
        logging.error("Unable to connect to root node database")
        raise ConnectErrorExc
    main_connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = main_connection.cursor()
    statement = "SELECT 1 FROM pg_database WHERE datistemplate = false \
                AND datname = '%s'" % database_obj['database_name']
    cursor.execute(statement)
    if cursor.fetchone():
        logging.error("Desired database already exists. Do nothing.")
        raise DBAlreadyExistsExc
    if main_connection.server_version < 90199:
        statement = "SELECT pg_terminate_backend(pg_stat_activity.procpid) \
            FROM pg_stat_activity WHERE pg_stat_activity.datname = '%s' \
            AND procpid <> pg_backend_pid();" % database_obj['database_name']
    else:
        statement = "SELECT pg_terminate_backend(pg_stat_activity.pid) \
            FROM pg_stat_activity WHERE pg_stat_activity.datname = '%s' \
            AND pid <> pg_backend_pid();" % database_obj['database_name']
    cursor.execute(statement)
    statement = "DROP DATABASE IF EXISTS %s;" % database_obj['database_name']
    cursor.execute(statement)
    start_sync_db()
