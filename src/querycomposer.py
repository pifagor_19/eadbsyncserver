'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

# performs SELECT statement
def get_select_statement(columns_list, table_name):
    return 'SELECT %s FROM %s' % (columns_list, table_name)

# performs SELECT statement with conditions
def get_select_condition_statement(columns_list, table_name, condition_stmt):
    return 'SELECT %s FROM %s WHERE %s' % (columns_list, table_name, condition_stmt)

# get ftp credentials
def get_ftp_info():
    return 'SELECT address as host, username as login, password as pass, \
    media_folder as media, root_folder as root FROM ea_ftps'

# get string with columns of specified table
def get_table_columns_string(table_name):
    return "SELECT array_to_string( array_agg( column_name::text ), ',') \
    AS val FROM information_schema.columns WHERE table_name = '%s';" % table_name

# get table columns
def get_table_columns(table_name):
    return "SELECT column_name FROM information_schema.columns \
    WHERE table_name = '%s' ORDER BY column_name" % table_name

# delete statement
def get_delete_query(table_name, pr_key, vals_list):
    return "DELETE FROM %s WHERE %s IN (%s)" % (table_name, pr_key, vals_list)

# insert statement
def get_insert_query(table_name, keys, _retcol = None):
    sql_string = "INSERT INTO {table}({keys}) VALUES({placeholders})".format(
        table = table_name,
        keys = ','.join(k[0] for k in keys),
        placeholders= ', '.join(["%s" for k in keys]))
    sql_string += (" RETURNING %s" % _retcol) if _retcol else ""
    return sql_string

# update statement
def updparamsdate_query(table_name, pr_key, pr_key_vals, *values_list):
    value_string = str()
    for item in values_list:
        value_string += "{key} = {value},".format(key = item, value = "%s")
    value_string = value_string[:-1]
    sql_string = "UPDATE {table} SET %s WHERE {prim_key} = '{prim_key_values}'".format(
        table = table_name,
        prim_key = pr_key,
        prim_key_values = str(pr_key_vals)) % value_string
    return sql_string

# create buildings hierarchy
def updparamsdate_hierarchy_script():
    # old version - dangerous!!!!
    #return "WITH query1 AS (SELECT name FROM integra_buildings) \
    #SELECT build_hierarchy(query1.name) FROM query1"
    
    # new fully working version
    return "WITH query1 AS (SELECT building_uid FROM integra_buildings) \
    SELECT build_hierarchy(query1.building_uid) FROM query1"

def get_servers(buildings, _main = True, _reserved = True):
    subquery = str()
    if _main:
        subquery = "SELECT building_uid, address, certificate, username, password, interval, bias, is_reserved \
                    FROM integra_buildings INNER JOIN ea_servers ON (server_id = sid)";
    if _reserved:
        subquery = "SELECT building_uid, address, certificate, username, password, interval, bias, is_reserved \
                    FROM integra_buildings INNER JOIN ea_servers ON (reserved_server_id = sid)";
    if (_main and _reserved):
        subquery = "SELECT building_uid, address, certificate, username, password, interval, bias, is_reserved \
                    FROM integra_buildings INNER JOIN ea_servers ON (server_id = sid) \
                    UNION \
                    SELECT building_uid, address, certificate, username, password, interval, bias, is_reserved \
                    FROM integra_buildings INNER JOIN ea_servers ON (reserved_server_id = sid)";
    return "WITH query as \
            ( " + subquery + ") SELECT * FROM query WHERE building_uid IN (%s);" % buildings
        
def update_servers(_buid, servAddress, _isReserved = True):
    substr = str("reserved_server_id = " if _isReserved else "server_id = ") + str("(SELECT sid FROM ea_servers WHERE address = '%s')" % servAddress)
    return "UPDATE integra_buildings SET " + substr + " WHERE building_uid = '" + str(_buid) + "'"

        
def insertServer(address = '127.0.0.1:2807', certificate = 'intercom.cer', username = 'guest', password = '', interval = 60, bias = 1, is_reserved = False):
    return str("SELECT insert_ea_servers('%s', NULL, FALSE); UPDATE ea_servers SET certificate = '%s', username = '%s', password = '%s', interval = %s, bias = %s, is_reserved = %s WHERE address = '%s';" % (address, certificate, username, password, interval, bias, is_reserved, address))

