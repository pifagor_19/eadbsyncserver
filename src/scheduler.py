'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import threading, time, settings, logging, traceback
from datetime import datetime, timedelta

class Scheduler(threading.Thread):
    # create scheduler instance
    def __init__(self, event):
        threading.Thread.__init__(self)
        self.stopped = event
        self.shed_stamp = dict()
        logging.info("Server started at %s" % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    
    # drop scheuler
    def __del__(self):
        logging.info("DESTROYING SCHEDULER")

    # set task manager instance
    def add_executor(self, executor_inst):
        self._tmgr = executor_inst
        self.init_sched()
    
    # init scheduler and add all tasks
    def init_sched(self):
        logging.info("INIT SCHEDULER")
        try:
            tasks = self._tmgr.get_tasks()
            for _uid in tasks.keys():
                self.reg_task(_uid)
        except:
            logging.error(traceback.format_exc())
    
    # add new task to scheduler
    def reg_task(self, task_uid):
        logging.info("REGISTER TASK: %s" % task_uid)
        _tsn = datetime.now()
        _tsk = self._tmgr.get_task(task_uid)
        _tupdpms = _tsk.updparams()
        _sched = dict()
        for _rtn in _tupdpms:
            _stamp = None
            _params = _tupdpms[_rtn]
            if _params.method() == "cron":
                _p = _params.params()
                _stamp = datetime(_tsn.year, _tsn.month, _tsn.day,
                                                _p[0], _p[1], _p[2])
            elif _params.method() == "interval":
                _stamp = _tsn
            else: continue
            _sched[_rtn] = _stamp
        self.shed_stamp[task_uid] = _sched
    
    # delete task from scheuler
    def unreg_task(self, task_uid):
        logging.info("UNREGISTER TASK: %s" % task_uid)
        del self.shed_stamp[task_uid]
    
    # set next time task will be executed
    def update_task_schedule(self, tasks_dict):
        try:
            if not len(tasks_dict): return
            logging.info('Update task schedule')
            for _task in tasks_dict.keys():
                if not tasks_dict[_task]:
                    continue
                if _task not in self.shed_stamp.keys():
                    self.reg_task(_task)
                    continue
                if _task not in self._tmgr.get_tasks():
                    self.unreg_task(_task)
                    continue
                _tparams = self._tmgr.get_task(_task).updparams()
                _schedparams = self.shed_stamp[_task]
                for _routine in _schedparams.keys():
                    _time = None
                    rtn = _tparams[_routine]
                    if rtn.method() == "cron":
                        _time = timedelta( days = 1 )
                    elif rtn.method() == "interval":
                        _p = rtn.params()
                        _time = timedelta( hours = _p[0],
                                          minutes = _p[1],
                                          seconds = _p[2])
                    else: continue
                    self.shed_stamp[_task][_routine] += _time
            logging.info("Update completed")
        except:
            logging.info(traceback.format_exc())
    
    # check if new task added or edited or deleted
    def check_tasks(self):
        tmgr_tasks = self._tmgr.get_tasks().keys()
        edited_tasks = self._tmgr.getEditedTask()
        sch_tasks = self.shed_stamp.keys()
        for item in (set(tmgr_tasks) ^ set(sch_tasks)):
            if item not in tmgr_tasks:
                self.unreg_task(item)
            else:
                self.reg_task(item)
        if edited_tasks:
            for item in edited_tasks:
                logging.info("EDIT TASK")
                self.unreg_task(item)
                self.reg_task(item)
    
    # run scheduler process
    def run(self):
        while not self.stopped.wait(settings.UPDATE_SPEED_INTERVAL):
            try:
                task_dict = dict()
                self.check_tasks()
                curr_time = datetime.now()
                get_delta_keys = lambda task : filter( lambda key: curr_time >= task[key], task )
                for task in self.shed_stamp.keys():
                    routines_list = get_delta_keys(self.shed_stamp[task])
                    if routines_list: task_dict[task] = routines_list
                if task_dict:
                    task_result = dict()
                    if (self._tmgr and not self._tmgr.is_executing()):
                        logging.info("---------------------------------------------------")
                        logging.info("Starting execution of tasks at %s" % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                        task_result = self._tmgr.execute_tasks(**task_dict)
                        self.update_task_schedule(task_result)
                        logging.info("Execution ended at %s" % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                        logging.info("---------------------------------------------------")
            except:
                logging.info(traceback.format_exc())
