import hashlib, sys

def gen_md5(f, block_size = 2048):
    _f = open(f)
    md5 = hashlib.md5()
    while True:
        data = _f.read(block_size)
        if not data: break
        md5.update(data)
	#_f.close()
    return md5.hexdigest()
	
def usage():
	print("		Usage: python genhash.py <filename> (<hash filename>)")
	exit(0)

if len(sys.argv) == 1:
	usage()
elif len(sys.argv) == 2:
	filename = sys.argv[1]
	_hash = gen_md5(filename)
	d = open(str(filename + ".md5"), "w+")
	d.write(_hash)
	d.close()
elif len(sys.argv) == 3:
	filename = sys.argv[1]
	hashfile = sys.argv[2]
	_hashCalculated = gen_md5(filename)
	d = open(hashfile, "r")
	_hashFromFile = d.read()
	if _hashCalculated == _hashFromFile:
		print("Hash are equal")
	else:
		print("Hash are different")
	d.close()
	
