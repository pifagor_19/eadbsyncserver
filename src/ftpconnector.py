'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

from ftplib import FTP
from multiprocessing import Pool
import re, os, time, traceback
import settings, settingsloader, logging, ftpsyncdir1 as fsd

ftp_pool = dict()

'''
        Some comments:
        *       To resume download you need to send commands:
        **          ftp> QUOTE PASV I
        **          ftp> REST %file_offset%
        **          ftp> RETR %file_name%
'''

# ---------------------------------------------------------------------------------------------
# FTP module exceptions
# ---------------------------------------------------------------------------------------------
# Base exception class
class FTPExc(Exception):
    pass

# Connection refused exception
class FTPConnExc(FTPExc):
    pass

# Retrieve file exception
class FTPRetrExc(FTPExc):
    pass

# ---------------------------------------------------------------------------------------------
# FTP module
# ---------------------------------------------------------------------------------------------

class FTPModule():
    # init module
    def __init__(self, kwargs):
        self._ftp = FTP()
        self._home_dir = settingsloader.getinfobytag("settings")['ftp_homedir']
        assert len(kwargs) > 0, "No parameters specified"
        regexp = '(?P<host>[^:/ ]+).?(?P<port>[0-9]*).*'
        m = re.search(regexp,kwargs[0])
        self.tmp_info = str()
        self.host = m.group('host') if kwargs else None
        self.port = m.group('port') if kwargs else None
        self.login = kwargs[1] if kwargs else None
        self.password = kwargs[2] if kwargs else None
        self.media_root = kwargs[3] if kwargs else None
        self.ftp_root = kwargs[4] if kwargs else None
        self.max_attempts = settings.MAX_FTP_CONN_ATTEMPTS
        logging.info("FTP open")
        try:
            self._ftp.connect(self.host, self.port, settings.TIMEOUT_INTERVAL)
            self._ftp.login(self.login, self.password)
        except:
            logging.error(traceback.format_exc())

    # close all ftp connections
    def close(self):
        self._ftp.close()
        logging.info("FTP closed")

    # retrieve file from FTP
    def retrieveFile(self, jobid, file_path, file_name, attempts = 0):
        try:

            try:
                self._ftp.voidcmd("NOOP")
            except:
                self._ftp.close()
                self._ftp.connect(self.host, self.port, settings.TIMEOUT_INTERVAL)
                self._ftp.login(self.login, self.password)

            self._ftp.voidcmd('TYPE I')
            self._ftp.cwd(self.media_root + '/' + file_path)
            dest_path = self._home_dir + self.media_root + '/' + file_path + '/'
            if not os.path.exists(dest_path): os.makedirs(dest_path)
            self.tmp_info = dest_path + file_name
            file_handler = open(self.tmp_info, "wb")
            file_size = self._ftp.size(file_name)

            logging.info('%d Downloading file: %s Size: %.2f Kb' % (jobid,'(' + file_path + '/' + file_name + ')', float(file_size)/1024.0))

            def handle_download(block_data, file):
                percentage = (float(file.tell()) / float(file_size))*100
                #print 'Downloaded %.2f %%' % percentage
                file.write(block_data)

            self._ftp.retrbinary("RETR " + file_name, lambda block : handle_download(block, file_handler))
            return True
        except:
            #print traceback.format_exc()
            logging.error(traceback.format_exc())
            time.sleep(5)
            if attempts >= self.max_attempts:
                logging.error("Max attemts exceeded. Exit")
                os.remove(self.tmp_info)
                raise FTPRetrExc
            self.retrieveFile(jobid, file_path, file_name, attempts + 1)
    
    # manage ftp queue
    def manage_downloads(self, **kwargs):
        itr = 0
        global ftp_pool
        if not self.media_root:
            logging.error("database connection error - ftp is not initialised")
            raise FTPRetrExc
        for item in kwargs.keys():
            folder = item
            _file = kwargs[item]
            self.retrieveFile(itr, folder, _file)
            itr += 1
        self._ftp.close()#quit()
    
    # update typedefs
    def update_resources(self, calbak = None, *srv_file):
        r = True
        for item in srv_file:
            src = self.ftp_root + item
            dst = self._home_dir + self.ftp_root + item
            res = fsd.synchronize(self.host, self.port, self.login, self.password, src, dst, callback = calbak)
            r &= True if res != -1 else False
        return r
    
    # some getter and setter methods
    def set_homedir(self, home_dir):
        if home_dir is not None:
            self._home_dir = home_dir

    def get_homedir(self):
        return self._home_dir

    def get_mediaroot(self):
        return self.media_root
