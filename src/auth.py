# -*- encoding: UTF-8 -*-
#
# Form based authentication for CherryPy. Requires the
# Session tool to be loaded.
#

import  cherrypy, settingsloader, utils, datetime, logging, settings

SESSION_KEY = '_cp_username'


def check_credentials(username, password):
    print "Inside check creds"
    crd = settingsloader.getinfobytag('credentials')
    if (username == crd['server_login']
        and utils.md5_hash(password) == crd['server_password']):
        return None
    else: return u"Введенные имя пользователя и пароль неверны."


def check_auth(*args, **kwargs):
    print "| ", args, "|", kwargs
    print datetime.datetime.now(), " :::>> check auth"
    conditions = cherrypy.request.config.get('auth.require', None)
    if conditions is not None:
        username = cherrypy.session.get(SESSION_KEY)
        if username:
            cherrypy.request.login = username
            for condition in conditions:
                if not condition(): raise cherrypy.HTTPRedirect("/auth/login")
        else: raise cherrypy.HTTPRedirect("/auth/login")
    print datetime.datetime.now(), " :::>> End check auth"
    #return False


def require(*conditions):
    def decorate(f):
        print "REQUIRE",f
        if not hasattr(f, '_cp_config'):
            f._cp_config = dict()
        if 'auth.require' not in f._cp_config:
            f._cp_config['auth.require'] = []
        f._cp_config['auth.require'].extend(conditions)
        return f
    return decorate

cherrypy.tools.auth = cherrypy.Tool('before_handler', check_auth, priority = 60)


class AuthController(object):
    def on_login(self, username):
        print "hey, ", username
        raise cherrypy.HTTPRedirect("/")

    def on_logout(self, username):
        print "logout"
        raise cherrypy.HTTPRedirect("/auth/login")

    def get_loginform(self, username, msg="Enter login information", from_page="/"):
        return open('./web/auth.html')

    @cherrypy.expose
    def login(self, username=None, password=None, from_page="/", _ = None):
        print "on login"
        if username is None or password is None:
            return self.get_loginform("", from_page=from_page)
        error_msg = check_credentials(username, password)
        if error_msg:
            return self.get_loginform(username, error_msg, from_page)
        else:
            cherrypy.session[SESSION_KEY] = cherrypy.request.login = username
            self.on_login(username)

    @cherrypy.expose
    def logout(self, from_page="/"):
        print "on logout"
        sess = cherrypy.session
        username = sess.get(SESSION_KEY, None)
        sess[SESSION_KEY] = None
        if username:
            cherrypy.request.login = None
            self.on_logout(username)

# -------------------------------------------------------------------------------------
# Background task queue
# -------------------------------------------------------------------------------------

import Queue
import threading
from cherrypy.process.plugins import SimplePlugin


class BackgroundTaskQueue(SimplePlugin):

    thread = None

    def __init__(self, bus, qsize=100, qwait=2, safe_stop=True):
        SimplePlugin.__init__(self, bus)
        self.q = Queue.Queue(qsize)
        self.qwait = qwait
        self.safe_stop = safe_stop

    def start(self):
        self.running = True
        if not self.thread:
            self.thread = threading.Thread(target=self.run)
            self.thread.start()

    def stop(self):
        if self.safe_stop:
            self.running = "draining"
        else:
            self.running = False

        if self.thread:
            self.thread.join()
            self.thread = None
        self.running = False

    def run(self):
        while self.running:
            try:
                try:
                    func, args, kwargs = self.q.get(block=True, timeout=self.qwait)
                except Queue.Empty:
                    if self.running == "draining":
                        return
                    continue
                else:
                    func(*args, **kwargs)
                    if hasattr(self.q, 'task_done'):
                        self.q.task_done()
            except:
                self.bus.log("Error in BackgroundTaskQueue %r." % self,
                             level=40, traceback=True)

    def put(self, func, *args, **kwargs):
        """Schedule the given func to be run."""
        self.q.put((func, args, kwargs))
