'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import cherrypy, os, logging, settings, sys, datetime, thread
from dataprovider import *
import settingsloader
#from auth import *

_server = None

class WebService(object):

    #auth = AuthController()

    @cherrypy.expose
    #@require()
    def index(self):
        return open('./web/index.html')

    @cherrypy.expose
    #@require()
    def settings(self):
        return open('./web/settings.html')

    @cherrypy.expose
    #@require()
    def init_db(self):
        return open('./web/init_db.html')

    @cherrypy.expose
    #@require()
    def handle_request(self, format, _, func, **kwargs):
        r = utils.callModuleFunc('dataprovider', func, **kwargs)
        r = utils.convert_to_json({'func': func, 'data': r})
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return format % r

class WebServiceProvider(object):
    def __init__(self, sync_serv = None):
        global _server
        creds = settingsloader.getinfobytag('credentials')
        server_conf = {
            'server.socket_host': settings.WEBSERVICE_HOST,
            'server.socket_port': settings.WEBSERVICE_PORT,
            'tools.encode.on': True,
            'tools.encode.encoding':'utf8',
            'server.ssl_module':'builtin',
            'server.ssl_certificate':'eadbsync.crt',
            'server.ssl_private_key':'eadbsync.key',
            'server.thread_pool': settings.SERVER_THREADS_COUNT,
            'tools.sessions.on': True,
            #'tools.auth.on': True,
            'tools.basic_auth.on': True,
            'tools.basic_auth.realm': 'By default login/pass are: admin/password.',
            'tools.basic_auth.users': {
                creds['server_login']: creds['server_password']
            },
            'tools.basic_auth.encrypt': utils.md5_hash
        }
        _server = sync_serv
        current_dir = os.path.dirname(os.path.abspath(__file__))
        cherrypy.log.screen                       = None
        cherrypy.log.error_log.propagate          = False
        cherrypy.log.access_log.propagate         = False
        cherrypy.config.update(server_conf)
        logging.info("WebService started")
        cherrypy.quickstart(WebService(),config={
            '/js':      {
                'tools.staticdir.on':True,
                'tools.staticdir.dir':os.path.join(current_dir,"web","js")
            },
            '/css':     {
                'tools.staticdir.on':True,
                'tools.staticdir.dir':os.path.join(current_dir,"web","css")
            },
            '/auth/js': {
                'tools.staticdir.on':True,
                'tools.staticdir.dir':os.path.join(current_dir,"web","js")
            },
            '/auth/css':{
                'tools.staticdir.on':True,
                'tools.staticdir.dir':os.path.join(current_dir,"web","css")
            }
        })
