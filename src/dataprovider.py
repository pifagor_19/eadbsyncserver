﻿# -*- coding: utf-8 -*-
'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import databaseconnector as dbcon, settingsloader as stLdr, utils, querycomposer as qc, webservice as web, uuid, init_db, logging, traceback, cherrypy, gettext
from collections import OrderedDict
from webservice import _server
from utils import exc_handler

# -------------------------------------------------------------------------

def tasks_status():
    return web._server.task_manager().status()

# -------------------------------------------------------------------------

def get_server_settings():
    serv_set = stLdr.getinfobytag('local_settings')
    other_settings = stLdr.getinfobytag('settings')
    return dict(serv_set.items() + other_settings.items())

# -------------------------------------------------------------------------

def set_server_settings(**kwargs):
    d = dict()
    s = dict()
    for k,v in kwargs.iteritems():
        if k != "ftp_homedir":
            d[k] = v
        else: s[k] = v
    stLdr.save_local_settings('local_settings', **d)
    stLdr.save_local_settings('settings', **s)
    web._server.task_manager().reconnect()

# -------------------------------------------------------------------------

#@exc_handler(err_msg = "Невозможно установить соединение с сервером", err_ret = {'err_msg': "Невозможно установить соединение с сервером"})
def get_columns_list(**kwargs):
    try:
        t = gettext.translation('eadbsyncsrv', './locale', languages=['ru'])
        db_con = web._server.task_manager().localdb()  #executor.db_connector
        statement = qc.get_table_columns('integra_buildings')
        result = dict()
        for item in db_con.fetch(statement):
            result[item[0]] = t.ugettext(item[0])
        #r = [item[0] for item in db_con.fetch(statement)]
        del result['building_uid']
        result = OrderedDict(sorted(result.items(), key = lambda x: x[1]))
        #sorted(result.items(), key = lambda x: x[1])
        return result
    except:
        logging.error(traceback.format_exc())
        return {'err_msg': "Невозможно установить соединение с сервером"}

# -------------------------------------------------------------------------
#@exc_handler(**{'err_msg': 'Введены неверные параметры соединения!'})
def get_plans(**kwargs):
    try:
        #print 'Inside get_plans'
        for task in web._server.task_manager().get_tasks().values():
            if 'action' in kwargs.keys():
                if (kwargs['action'] == 'insert'):
                    if (kwargs['database_host'] == task.param("database_host")
                        and kwargs['database_name'] == task.param("database_name")):
                        return {'err_msg': "Задание с данными параметрами уже существует!"}
            else: return {'err_msg': "Введены неверные параметры соединения!"}
        db_connector = dbcon.DBModule(**kwargs) #dbcon.DatabaseConnector(**kwargs)
        stmt = qc.get_select_statement('building_uid, name', 'integra_buildings_view ORDER BY name ASC')
        res = db_connector.fetch(stmt) #fetch_query('main', stmt)
        ret_obj = list()
        if res:
            for item in res:
                ret_obj.append({'uid': item[0], 'name': item[1]})
            db_connector = None
            return ret_obj
        else:
            return {'err_msg': 'На данном сервере отсутствуют планы для синхронизации'}
    except:
        logging.error(traceback.format_exc())
        return {'err_msg': 'Введены неверные параметры соединения!'}

# -------------------------------------------------------------------------

def execute_tasks(**kwargs):
    if web._server.task_manager().is_executing() :
        return {'err_msg': 'Синхронизация уже выполняется!'}
    message = {'completed': False, 'msg': ''}
    res = web._server.executor.execute_all()
    #return res

# -------------------------------------------------------------------------
#@exc_handler(**{'err_msg': 'Неверный формат задания!'})
def create_new_task(**kwargs):
    #try:
    #print "Inside new task"
    if 'task' in kwargs.keys():
        raw_task = unicode(kwargs['task'])
        if utils.validate_task_json(raw_task):
            new_task = utils.convert_from_json(raw_task)
            task_name = new_task['task_name']
            db_host = new_task['database_host']
            for task in web._server.task_manager().get_tasks().values():
                if (task_name == task.param("task_name") or db_host == task.param("database_host")):
                    return {'err_msg': "Задание с данными параметрами уже существует!"}
            uid = web._server.executor.add(**new_task)
            return uid
        else: return {'err_msg': 'Задание не прошло валидацию!'}
    else: return {'err_msg': 'Неверный формат задания!'}
    #except Exception as e:
    #        return {'err_msg': 'Неверный формат задания!'}

# -------------------------------------------------------------------------

def edit_task(**kwargs):
    if 'task' in kwargs.keys():
        task_josn = kwargs['task']
        if utils.validate_task_json(task_josn):
            edit_obj = utils.convert_from_json(task_josn)
            if edit_obj['task_uid'] in web._server.executor.tasks_list.keys():
                uid = web._server.executor.edit(**edit_obj)
                return uid
            else: return {'err_msg': "Задания с таким идентификатором не существует!"}
        else: return {'err_msg': "Неверные параметры задания"}

# -------------------------------------------------------------------------

def delete_task(**kwargs):
    if 'task_uid' in kwargs.keys():
        if kwargs['task_uid'] in web._server.executor.tasks_list.keys():
            web._server.executor.remove(kwargs['task_uid'])
            return kwargs['task_uid']
        else: return {'err_msg': 'Задания с таким идентификатором не существует!'}
    else:
        return "Error"

# -------------------------------------------------------------------------

def get_task(**kwargs):
    if 'task_uid' in kwargs.keys():
        if kwargs['task_uid'] in web._server.executor.tasks_list.keys():
            return web._server.task_manager().get_task(kwargs["task_uid"]).description(True)
        else: return {'err_msg': 'Задания с таким идентификатором не существует!'}

# -------------------------------------------------------------------------

def get_all_tasks(**kwargs):
    global _server
    all_tasks = list()
    tasks = web._server.task_manager().get_tasks()
    for uid, task in tasks.iteritems():
        td = {'task_uid': uid, 'task_name': task.param('task_name')}
        all_tasks.append(td)
    all_tasks = sorted(all_tasks, key = lambda k: k['task_name'])
    return all_tasks

# -------------------------------------------------------------------------

def initialise_database(**kwargs):
    try:
        if 'json' in kwargs.keys():
            init_db.init(kwargs['json'])
            return {'status':'True'}
        else:
            return {
                'status':'False',
                'err_msg': 'Не все поля заполнены!'
            }
    except init_db.RestoreBackupExc:
        logging.error("Unable to restore tables from backup")
        return {
            'status':'False',
            'err_msg': 'Невозможно восстановить таблицы. Возможно, postgres не содержится в PATH'
        }
    except init_db.TemplateUnavailable:
        logging.error("Unable to create database from template")
        return {
            'status':'False',
            'err_msg': 'Невозможно создать базу данных - шаблон template_postgis не существует или уже используется'
        }
    except init_db.ScriptExecExc:
        logging.error("Unable to exec script")
        return {
            'status':'False',
            'err_msg': 'Невозможно выполнить инициализацию базы данных'
        }
    except init_db.ConnectErrorExc:
        logging.error("Unable to connect to database")
        return {
            'status':'False',
            'err_msg': 'Ошибка подключения к базе данных'
        }
    except init_db.DBAlreadyExistsExc:
        logging.error("database already exists")
        return {
            'status':'False',
            'err_msg': 'Указанная база данных уже существует. Укажите другое имя.'
        }
    except Exception as e:
        logging.error("unrecognized error. Details: ", traceback.format_exc())
        return {
            'status':'False',
            'err_msg': 'Неизвестная ошибка'
        }

# -------------------------------------------------------------------------

def change_password(password):
    try:
        if not password: return {'err_msg': 'Пароль не может быть пустым'}
        creds = stLdr.getinfobytag('credentials')
        ch_passwd = utils.md5_hash(password)
        creds = {
            "server_password": ch_passwd,
            "server_login": creds["server_login"]
        }
        stLdr.save_local_settings(section_name = 'credentials', **creds)
        cherrypy.config.update({
            'tools.basic_auth.users': {
                creds['server_login']: ch_passwd
            }
        })
        cherrypy.engine.stop()
        cherrypy.engine.start()
        return True
    except:
        logging.error(traceback.format_exc())
        return False

# -------------------------------------------------------------------------

def change_ftp(ftp_path):
    try:
        if not ftp_path: return {'err_msg': 'Путь не может быть пустым'}
        sets = {"ftp_homedir": ftp_path}
        stLdr.save_local_settings(section_name = 'settings', **sets)
        return True
    except:
        logging.error(traceback.format_exc())
        return False
