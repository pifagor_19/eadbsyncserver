'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import syncserver, webservice, settingsloader

if __name__ == '__main__':
    settingsloader.reload_settings()
    sync_server = syncserver.SyncServer()
    websvc = webservice.WebServiceProvider(sync_server)
