-------------------------------------------------------------------
-- Database Creation script
-- Author: Pronin Alexey
-- email: topifagorsend@gmail.com
-------------------------------------------------------------------
-- Создание расширения для генерации uid
-------------------------------------------------------------------

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

-------------------------------------------------------------------
-- Создание всех таблиц
-------------------------------------------------------------------

CREATE TABLE _system_rest
(
    table_oid oid,
    row_oid oid,
    user_id integer,
    is_edited BOOLEAN DEFAULT True,
    CONSTRAINT unique_ctr UNIQUE(table_oid, row_oid, user_id)
);

-------------------------------------------------------------------
-- !!! Possibly, not used in the future !!!
CREATE TABLE ea_servers
(
    sid serial NOT NULL,
    address text NOT NULL,
    serverid text,
    online boolean NOT NULL DEFAULT false,
    datetime timestamp without time zone NOT NULL,
    certificate text NOT NULL DEFAULT 'intercom.cer',
    username text NOT NULL DEFAULT 'guest',
    password text NOT NULL DEFAULT '',
    interval integer NOT NULL DEFAULT 60,
    detailed boolean NOT NULL DEFAULT false,
    keepalive boolean NOT NULL DEFAULT false,
    bias integer NOT NULL DEFAULT 1,
    is_reserved boolean NOT NULL DEFAULT false,
	conn_string text DEFAULT 'host=127.0.0.1 port=5432 dbname=acuario21 user=postgres password=xxxx'::text,
    ea_login text,
	ea_pass text,
    CONSTRAINT ea_servers_pkey PRIMARY KEY (sid),
    CONSTRAINT ea_servers_address_key UNIQUE (address)
);

-------------------------------------------------------------------

CREATE TABLE ea_counters
(
    sid integer NOT NULL,
    version integer NOT NULL,
    deleted boolean NOT NULL,
    type text NOT NULL,
    plus integer,
    minus integer,
    exclamation integer,
    question integer,
    CONSTRAINT ea_counters_pkey PRIMARY KEY (sid, type),
    CONSTRAINT ea_counters_sid_fkey FOREIGN KEY (sid)
        REFERENCES ea_servers (sid) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);

-------------------------------------------------------------------

CREATE TABLE ea_ftps
(
    sid serial NOT NULL,
    address text NOT NULL,
    serverid uuid NOT NULL DEFAULT uuid_generate_v4(),
    online boolean NOT NULL DEFAULT false,
    username text NOT NULL DEFAULT 'guest',
    password text NOT NULL DEFAULT '',
    root_folder text NOT NULL DEFAULT '/eily.acuario',
    media_folder text NOT NULL DEFAULT '/eily.acuario/media/buildings',
    CONSTRAINT ea_ftp_pkey PRIMARY KEY (sid)
);

-------------------------------------------------------------------

CREATE TABLE integra_borders
(
    osm_id bigint NOT NULL,
    admin_level text,
    name text,
    place text,
    preview bytea
);

-------------------------------------------------------------------

-- create way column
SELECT AddGeometryColumn('integra_borders', 'way', 4326, 'GEOMETRY', 2);
-- create cpoint column
SELECT AddGeometryColumn('integra_borders', 'cpoint', 4326, 'POINT', 2);

-------------------------------------------------------------------

CREATE TABLE integra_buildings
(
    model_ver integer,
    transform bytea,
    profile bytea,
    organization integer,
    viewpoint bytea,
    preview bytea,
    addr_postcode text,
    addr_street text,
    addr_housenumber text,
    name text,
    int_name text,
    server_id integer,
    building_uid uuid NOT NULL DEFAULT uuid_generate_v4(),
    model_uid uuid,
    ib_hierarchy bigint[] DEFAULT ARRAY[]::bigint[],
    submodels bytea,
    reserved_server_id integer,
    transform_web text,
    unload_bound real DEFAULT -1,
	preview_type text DEFAULT '',
	ea_server uuid,
    CONSTRAINT integra_buildings_pkey PRIMARY KEY (building_uid),
	CONSTRAINT integra_buildings_unique UNIQUE (building_uid)
) WITH OIDS;

-------------------------------------------------------------------
-- create position column
SELECT AddGeometryColumn( 'integra_buildings', 'position', 4326, 'POINT', 2 );
-- create polygon_array column
SELECT AddGeometryColumn( 'integra_buildings', 'polygon_array', 4326, 'POLYGON', 2 );
-------------------------------------------------------------------

CREATE TABLE integra_buildings_hist
(
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    userid text NOT NULL,
    model_ver integer,
    transform bytea,
    profile bytea,
    organization integer,
    viewpoint bytea,
    preview bytea,
    addr_postcode text,
    addr_street text,
    addr_housenumber text,
    name text,
    int_name text,
    server_id integer,
    building_uid uuid,
    model_uid uuid,
    ib_hierarchy bigint[] DEFAULT ARRAY[]::bigint[],
    submodels bytea,
    reserved_server_id integer,
    transform_web text,
    unload_bound real DEFAULT -1,
	preview_type text DEFAULT ''::text,
	ea_server uuid
);

-------------------------------------------------------------------

-- create position column
SELECT AddGeometryColumn( 'integra_buildings_hist', 'position', 4326, 'POINT', 2 );
-- create polygon_array column
SELECT AddGeometryColumn( 'integra_buildings_hist', 'polygon_array', 4326, 'POLYGON', 2 );

-------------------------------------------------------------------

CREATE TABLE integra_favourite_plans
(
    user_id integer NOT NULL,
    building_uid uuid NOT NULL,
    CONSTRAINT integra_favourite_plans_ref_key FOREIGN KEY (building_uid)
        REFERENCES integra_buildings (building_uid) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT integra_favourite_plans_unique_key UNIQUE (user_id, building_uid)
);

-------------------------------------------------------------------

CREATE TABLE integra_features
(
    ogc_fid bigint NOT NULL,
    height real NOT NULL DEFAULT (-1),
    height_scale real NOT NULL DEFAULT 10,
    levels integer,
    addr_city text,
    addr_housenumber text,
    addr_postcode text,
    addr_street text,
    amenity text,
    int_name text,
    name text,
    visible boolean NOT NULL DEFAULT true,
    CONSTRAINT integra_features_pkey PRIMARY KEY (ogc_fid)
);
-------------------------------------------------------------------

-- add way column
SELECT AddGeometryColumn( 'integra_features', 'way', 4326, 'GEOMETRY', 2 );

-------------------------------------------------------------------

CREATE TABLE integra_plan_settings
(
  building_uid uuid,
  key text NOT NULL,
  value bytea,
  CONSTRAINT ips_unique_settings UNIQUE (building_uid, key)
);

-------------------------------------------------------------------

CREATE TABLE integra_sec_groups_def
(
    group_id serial NOT NULL,
    group_name text,
    description text,
    isvisible boolean NOT NULL DEFAULT false,
    preview bytea,
	preview_type text DEFAULT '',
    CONSTRAINT integra_sec_groups_def_pr_key PRIMARY KEY (group_id),
    CONSTRAINT integra_sec_groups_def_group_name_key UNIQUE (group_name)
);

-------------------------------------------------------------------

CREATE TABLE integra_sec_groups
(
    group_id integer NOT NULL,
    building_uid uuid NOT NULL,
    access_mask integer,
    CONSTRAINT integra_sec_groups_group_id_fkey FOREIGN KEY (group_id)
        REFERENCES integra_sec_groups_def (group_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT integra_sec_groups_ref_key FOREIGN KEY(building_uid)
        REFERENCES integra_buildings(building_uid) ON DELETE CASCADE,
    CONSTRAINT integra_sec_groups_group_id_building_uid_key UNIQUE (group_id, building_uid)
);

-------------------------------------------------------------------

CREATE TABLE integra_sec_users_def
(
    user_id integer NOT NULL,
    user_nick text DEFAULT 'anonymous',
    user_name text DEFAULT '-',
    user_surname text DEFAULT 'Анонимный пользователь',
    user_job text DEFAULT '-',
    user_lastname text DEFAULT '-',
    user_position text DEFAULT '-',
    user_timeout integer,
    security_mask integer,
    user_viewpoint bytea,
    CONSTRAINT integra_sec_users_def_user_id_key UNIQUE (user_id)
);

-------------------------------------------------------------------
-- ACHTUNG!!!
CREATE TABLE integra_sec_users
(
    user_id integer NOT NULL,
    group_id integer NOT NULL,
    CONSTRAINT integra_sec_users_group_id_fkey FOREIGN KEY (group_id)
        REFERENCES integra_sec_groups_def (group_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT integra_sec_users_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES integra_sec_users_def (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT integra_sec_users_user_id_group_id_key UNIQUE (user_id, group_id)
);

-------------------------------------------------------------------

CREATE TABLE integra_settings
(
    role_id integer,
    app_name text,
    settings bytea
);

-------------------------------------------------------------------

CREATE TABLE integra_versions
(
    allowed_clients integer,
    allowed_clients_read integer
);

-------------------------------------------------------------------

ALTER TABLE integra_favourite_plans ADD CONSTRAINT 
    integra_favourite_plans_user_ref_key FOREIGN KEY (user_id)
    REFERENCES integra_sec_users_def (user_id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;
    
-------------------------------------------------------------------
-- Создание всех функций
-- Security functions
-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION update_sys(table_name text)
    RETURNS void AS
$BODY$
    DECLARE
       qry RECORD;
       qry1 RECORD;
       key_query TEXT;
    BEGIN
        key_query := (
        'SELECT tableoid, oid FROM ' 
            || quote_ident(table_name)
            || ';'
        );
        FOR qry IN EXECUTE key_query LOOP 
            FOR qry1 IN SELECT usesysid FROM pg_shadow LOOP
                BEGIN
                    EXECUTE 'INSERT INTO _system_rest VALUES (' || qry.tableoid || ',' || qry.oid || ',' || qry1.usesysid || ', True)';
                    RAISE NOTICE 'RECORD ADDED';
                EXCEPTION
                    WHEN unique_violation THEN
                        RAISE NOTICE 'ALREADY EXISTS!';
                END;
            END LOOP;
        END LOOP;
    EXCEPTION
        WHEN undefined_table THEN 
            RAISE NOTICE 'Table already deleted'; 
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION register_new_user(username text, usersurname text, userlastname text,
userjob text, userposition text, userlogin text, userpass text)
    RETURNS boolean AS
$BODY$
    DECLARE returnResult BOOLEAN;
    DECLARE notificationString TEXT;
    BEGIN
        SELECT is_user_exists(userlogin) INTO returnResult;
        IF returnResult IS TRUE
            THEN RETURN FALSE;
        END IF;
        EXECUTE 'CREATE USER ' || quote_ident(userlogin) || ' WITH INHERIT LOGIN CREATEROLE '
        || ' PASSWORD ' || quote_literal(userpass);
        EXECUTE 'GRANT SELECT ON pg_shadow TO ' || quote_ident(userlogin);
        EXECUTE 'GRANT CONNECT ON DATABASE ' || current_database() || ' TO ' || userlogin;
        INSERT INTO integra_sec_users_def(user_id, user_nick, user_name, user_surname, user_lastname,
        user_job, user_position, user_timeout, security_mask)
            SELECT usesysid, usename, username, usersurname, userlastname, userjob, userposition, 10, 2032
            FROM pg_shadow WHERE usename = userlogin;
        PERFORM update_sys('integra_buildings');
        SELECT is_user_exists(userlogin) INTO returnResult;
        IF returnResult IS TRUE
            THEN
                notificationString := format('New user registered: %s', userlogin);
                EXECUTE 'NOTIFY test, ' || quote_literal(notificationString);
                RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION get_all_groups_info()
    RETURNS TABLE(group_id oid, group_name name) AS
$BODY$
    BEGIN
        RETURN QUERY
            SELECT grosysid, groname FROM pg_group pgr;
    END
$BODY$
LANGUAGE plpgsql;

--------------------------------------------------------------------

CREATE OR REPLACE FUNCTION get_buildings_groups_info(IN username text)
    RETURNS TABLE(group_id integer, group_name text) AS
$BODY$
    BEGIN
        RETURN QUERY
            SELECT isgd.group_id, isgd.group_name FROM integra_sec_groups_def isgd
            WHERE isgd.group_id IN (
                SELECT isu.group_id FROM integra_sec_users isu
                WHERE isu.user_id = (
                    SELECT isud.user_id FROM integra_sec_users_def isud
                    WHERE isud.user_nick = username
                )
            );
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION get_buildings_in_groups_info(IN usergroup text)
    RETURNS TABLE(building_uid uuid, name text) AS
$BODY$
    BEGIN
        RETURN QUERY
            SELECT ib.building_uid, ib.name FROM integra_buildings ib
            WHERE ib.building_uid IN (
                SELECT isg.building_uid FROM integra_sec_groups isg
                WHERE isg.group_id =(
                    SELECT isgd.group_id FROM integra_sec_groups_def isgd
                    WHERE isgd.group_name = usergroup
                )
            ) ORDER BY ib.name;
    END
$BODY$
  LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION get_groups_info(IN username text)
    RETURNS TABLE(group_id oid, group_name name) AS
$BODY$
    BEGIN
        RETURN QUERY
            SELECT pgr.grosysid, pgr.groname FROM integra_sec_users_def pgu
            RIGHT OUTER JOIN pg_group pgr ON pgu.user_id = ANY(pgr.grolist)
            WHERE user_nick = username;
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION get_user_privileges(IN user_name text)
    RETURNS TABLE(table_name name, table_column name, select_column_access boolean,
    insert_column_access boolean, update_column_access boolean, select_access boolean,
    insert_access boolean, update_access boolean, delete_access boolean) AS
$BODY$
    BEGIN
        RETURN QUERY
            SELECT 	c.relname AS "Название таблицы",
            a.attname AS "Столбец",
            has_column_privilege(user_name, c.relname, a.attname, 'SELECT'),
            has_column_privilege(user_name, c.relname, a.attname, 'INSERT'),
            has_column_privilege(user_name, c.relname, a.attname, 'UPDATE'),
            has_table_privilege (user_name, c.relname,	      'SELECT'),
            has_table_privilege (user_name, c.relname,            'INSERT'),
            has_table_privilege (user_name, c.relname,            'UPDATE'),
            has_table_privilege (user_name, c.relname,            'DELETE')
            FROM pg_catalog.pg_attribute a, pg_catalog.pg_class c
            WHERE a.attrelid IN(
                SELECT c.oid FROM pg_catalog.pg_class c
                LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
                WHERE pg_catalog.pg_table_is_visible(c.oid) AND c.relname IN(
                    SELECT c.relname AS table_name FROM pg_catalog.pg_class c LEFT JOIN pg_catalog.pg_namespace n
                    ON (n.oid = c.relnamespace) WHERE (c.relkind  = 'v' OR c.relkind = 'r') AND n.nspname = 'public'
                )
            )
            AND a.attnum > 0 AND NOT a.attisdropped AND c.oid = a.attrelid AND c.relname != 'spatial_ref_sys'
            AND c.relname != 'geometry_columns' ORDER BY c.relname;
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION get_users_info()
    RETURNS TABLE(user_id integer, user_nick text, user_name text, user_surname text, user_job text,
    user_lastname text, user_position text, user_timeout integer, security_mask integer, user_viewpoint bytea) AS
$BODY$
    BEGIN
        RETURN QUERY
            SELECT * FROM integra_sec_users_def
            ORDER BY user_nick;
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION is_user_exists(username text)
    RETURNS boolean AS
$BODY$
    DECLARE isExists BOOLEAN;
    BEGIN
        SELECT ( rolname = username ) INTO isExists FROM pg_roles WHERE rolname = username;
        IF isExists IS NULL
            THEN RETURN FALSE;
            ELSE RETURN isExists;
        END IF;
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION delete_building_group(groupid integer)
    RETURNS void AS
$BODY$
    DECLARE deleteSuccess BOOLEAN;
    BEGIN
        RAISE NOTICE 'START';
        DELETE FROM integra_sec_users WHERE group_id = groupid;
        DELETE FROM integra_sec_groups CASCADE WHERE group_id = groupid;
        DELETE FROM integra_sec_groups_def CASCADE WHERE group_id = groupid;
        RAISE NOTICE 'END';
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION delete_user(username text)
    RETURNS boolean AS
$BODY$
    DECLARE deleteSuccess BOOLEAN;
    BEGIN
        SELECT is_user_exists(username) INTO deleteSuccess;
        IF deleteSuccess IS FALSE
        THEN
                RETURN FALSE;
        END IF;
        EXECUTE 'REVOKE ALL PRIVILEGES ON DATABASE integraplanetearth FROM ' ||  quote_ident(username);
        EXECUTE 'DROP OWNED BY ' || quote_ident(username);
        EXECUTE 'DROP ROLE IF EXISTS ' || quote_ident(username);
        DELETE FROM integra_sec_users WHERE user_id = (
                SELECT user_id FROM integra_sec_users_def
                WHERE user_nick = username
        );
        DELETE FROM _system_rest WHERE user_id = (
                SELECT user_id FROM integra_sec_users_def
                WHERE user_nick = username
        );
        DELETE FROM integra_sec_users_def WHERE user_nick = username;
        SELECT is_user_exists(username) INTO deleteSuccess;
        IF deleteSuccess IS FALSE
        THEN
                RETURN TRUE;
        ELSE
                RETURN FALSE;
        END IF;
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION register_new_user_group(groupname text, privileges text)
    RETURNS boolean AS
$BODY$
    DECLARE returnResult BOOLEAN;
    DECLARE notificationString TEXT;
    BEGIN
        SELECT is_user_exists(groupname) INTO returnResult;
        IF returnResult IS TRUE
                THEN RETURN FALSE;
        END IF;
        EXECUTE 'CREATE GROUP ' || quote_ident(groupname) || ' WITH ' || privileges || ' INHERIT';
        RETURN TRUE;
    END
$BODY$
  LANGUAGE plpgsql;

-------------------------------------------------------------------
-- Eantegration functions
-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION insert_ea_counters(newaddress text, newversion integer, newdeleted boolean,
newtype text, newplus integer, newminus integer, newexclamation integer, newquestion integer)
    RETURNS void AS
$BODY$
    BEGIN
        INSERT INTO ea_counters(sid, version, deleted, type, plus, minus, exclamation, question)
        VALUES(
            (SELECT sid FROM ea_servers WHERE address = newaddress), newversion, newdeleted,
            newtype, newplus, newminus, newexclamation, newquestion
        );
    END;
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION insert_ea_servers(newaddress text, newserverid text, newonline boolean)
    RETURNS void AS
$BODY$
    BEGIN
        INSERT INTO ea_servers(address, serverid, online, datetime) VALUES(newaddress, newserverid,
        newonline, (SELECT NOW() AT TIME ZONE 'UTC'));
    END;
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION reinsert_ea_servers(newaddress text, newserverid text, newonline boolean,
newdatetime timestamp without time zone, newcertificate text, newusername text, newpassword text,
newinterval integer, newdetailed boolean, newkeepalive boolean, newbias integer)
    RETURNS void AS
$BODY$
    BEGIN
        DELETE FROM ea_servers WHERE address = newaddress;
        INSERT INTO ea_servers (address, serverid, online, datetime, certificate, username, password,
        interval, detailed, keepalive, bias)
        VALUES(newaddress, newserverid, newonline, newdatetime, newcertificate, newusername, newpassword,
        newinterval, newdetailed, newkeepalive, newbias);
    END;
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------
-- IPE functions
-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION clean_history()
    RETURNS void AS
$BODY$
    BEGIN
DELETE FROM integra_buildings_hist WHERE stamp IN 
(
	SELECT stamp FROM (
		SELECT stamp, row_number() over (
			partition BY building_uid, model_ver, addr_postcode, addr_street, addr_housenumber, 
			name, int_name, position, server_id, reserved_server_id ORDER BY stamp) 
		AS rnum FROM integra_buildings_hist
	) t WHERE t.rnum > 1);
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION build_hierarchy(plan_uid uuid)
    RETURNS void AS
$BODY$
    BEGIN
        UPDATE integra_buildings SET ib_hierarchy = res_q.result
        FROM (
            SELECT array(
                SELECT i_bord.osm_id FROM integra_borders i_bord
                INNER JOIN integra_buildings i_B ON ST_Within(i_B.position, i_bord.way)
                WHERE i_B.building_uid = plan_uid ORDER BY i_bord.admin_level::integer ASC
            ) AS result
        ) AS res_q
        WHERE building_uid = plan_uid;
    END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION clean_history()
  RETURNS void AS
$BODY$
    BEGIN
DELETE FROM integra_buildings_hist WHERE stamp IN 
(
	SELECT stamp FROM (
		SELECT stamp, row_number() over (
			partition BY building_uid, model_ver, addr_postcode, addr_street, addr_housenumber, 
			name, int_name, position, server_id, reserved_server_id ORDER BY stamp) 
		AS rnum FROM integra_buildings_hist
	) t WHERE t.rnum > 1);
    END
$BODY$
  LANGUAGE plpgsql VOLATILE;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION insert_favourite_building(building_uid uuid)
	RETURNS void AS
$BODY$
	BEGIN
		INSERT INTO integra_favourite_plans SELECT usesysid, building_uid
		FROM pg_shadow WHERE usename = CURRENT_USER;
	END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION insert_new_building()
	RETURNS uuid AS
$BODY$
	DECLARE building_uuid uuid;
	BEGIN
		INSERT INTO integra_buildings_view(building_uid, model_ver, name) VALUES(uuid_generate_v4(), -1, 'Новое здание')
		RETURNING building_uid INTO building_uuid;
		INSERT INTO integra_sec_groups SELECT c.group_id, building_uuid FROM integra_sec_groups_def c WHERE c.group_name = 'defaultBuildingGroup';
		RETURN building_uuid;
	END;
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION delete_favourite_building(building__uid uuid)
	RETURNS void AS
$BODY$
	BEGIN
		DELETE FROM integra_favourite_plans WHERE user_id  = (
			SELECT usesysid FROM pg_shadow WHERE usename = CURRENT_USER
		)
		AND building_uid = building__uid;
	END
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION setdbsettingsvalue(roleid integer, application_name text, settings_json bytea)
  RETURNS void AS
$BODY$
	BEGIN
		UPDATE integra_settings SET settings = settings_json::bytea WHERE role_id = roleId AND app_name = application_name;
		INSERT INTO integra_settings
		SELECT roleId, application_name, decode(settings_json,'escape') WHERE NOT EXISTS (
			SELECT 1 FROM integra_settings WHERE role_id=roleId AND app_name = application_name
		);
	END
$BODY$
  LANGUAGE plpgsql;

-------------------------------------------------------------------
  
CREATE OR REPLACE FUNCTION setplansettings(buid uuid, p_key text, p_value bytea)
  RETURNS void AS
$BODY$
	BEGIN
		UPDATE integra_plan_settings SET value = p_value::bytea WHERE key = p_key AND building_uid = buid;
		INSERT INTO integra_plan_settings  
		SELECT buid, p_key, p_value WHERE NOT EXISTS (
			SELECT 1 FROM integra_plan_settings WHERE building_uid=buid AND key = p_key
		);
	END
$BODY$
  LANGUAGE plpgsql VOLATILE;
  
-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION restore_building(plan_uid uuid, plan_stamp timestamp without time zone)
  RETURNS VOID AS
$BODY$
	DECLARE ibh_row integra_buildings_hist%ROWTYPE;
	BEGIN
		SELECT * INTO ibh_row FROM integra_buildings_hist WHERE building_uid = plan_uid AND stamp = plan_stamp;
		IF ibh_row.operation = 'D' THEN
			INSERT INTO integra_buildings_view(model_ver, transform, profile, organization,
					viewpoint, preview, addr_postcode, addr_street, addr_housenumber, 
					name, int_name, "position", server_id, building_uid, model_uid,
					submodels,  polygon_array, reserved_server_id, transform_web) 
			SELECT model_ver, transform, profile, organization, viewpoint, preview, addr_postcode,
				addr_street, addr_housenumber, name, int_name, "position", server_id, building_uid,
				model_uid, submodels, polygon_array, reserved_server_id, transform_web 
			FROM integra_buildings_hist WHERE building_uid = plan_uid AND stamp = plan_stamp;
			DELETE FROM integra_buildings_hist WHERE building_uid = plan_uid AND stamp = plan_stamp;
			INSERT INTO integra_sec_groups SELECT c.group_id, plan_uid FROM integra_sec_groups_def c
				WHERE c.group_name = 'defaultBuildingGroup';
			PERFORM build_hierarchy(plan_uid);
			RAISE NOTICE 'RESTORING DELETED ITEM';
			RETURN;
		ELSIF ibh_row.operation IN ('I', 'U') THEN
			UPDATE integra_buildings_view SET model_ver = subquery.model_ver,
					transform = subquery.transform,
					profile = subquery.profile,
					organization = subquery.organization,
					viewpoint = subquery.viewpoint,
					preview = subquery.preview,
					addr_postcode = subquery.addr_postcode,
					addr_street = subquery.addr_street,
					addr_housenumber = subquery.addr_housenumber,
					name = subquery.name,
					int_name = subquery.int_name,
					position = subquery.position,
					server_id = subquery.server_id,
					model_uid = subquery.model_uid,
					submodels = subquery.submodels,
					polygon_array = subquery.polygon_array,
					reserved_server_id = subquery.reserved_server_id,
					transform_web = subquery.transform_web 
			FROM (
				SELECT model_ver, transform, profile, organization, viewpoint,
					preview, addr_postcode, addr_street, addr_housenumber, name,
					int_name, position, server_id, building_uid, model_uid, 
					ib_hierarchy, submodels, polygon_array, reserved_server_id, transform_web 
				FROM integra_buildings_hist WHERE building_uid = plan_uid AND stamp = plan_stamp) as subquery
			WHERE integra_buildings_view.building_uid = plan_uid;
			DELETE FROM integra_buildings_hist WHERE building_uid = plan_uid AND stamp = plan_stamp;
			PERFORM build_hierarchy(plan_uid);
			RAISE NOTICE 'RESTORING UPDATED ITEM';
			RETURN;
		ELSE
			RAISE NOTICE 'COULD NOT FIND ITEM';
			RETURN;
		END IF;
	END
$BODY$
  LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION setPlanToGroup(plan_uid uuid, bgroups bigint[])
  RETURNS VOID AS
$BODY$
  DECLARE r bigint;
	  i bigint;
	  id bigint[];
  BEGIN
	SELECT array_agg(isg.group_id) INTO id FROM integra_sec_groups isg 
	INNER JOIN integra_sec_groups_def isgd ON isg.group_id = isgd.group_id
	WHERE isgd.isvisible = True AND isg.group_id::bigint != ALL (bgroups) 
	AND building_uid = plan_uid;
	FOR i IN 1..coalesce(array_length(bgroups, 1), 0) LOOP
		BEGIN
			INSERT INTO integra_sec_groups(group_id, building_uid) VALUES(bgroups[i], plan_uid);
		EXCEPTION
			WHEN unique_violation THEN
				RAISE NOTICE 'EXISITING VALUE - SKIP';
		END;
	END LOOP;
	FOR i IN 1..coalesce(array_length(id, 1), 0) LOOP
		BEGIN
			DELETE FROM integra_sec_groups 
			WHERE building_uid = plan_uid AND group_id = id[i];
		EXCEPTION
			WHEN raise_exception THEN
				RAISE NOTICE 'NON EXISTENT VALUE - SKIP';
		END;
	END LOOP;
  END
$BODY$
  LANGUAGE plpgsql;

-------------------------------------------------------------------
-- Создание новых триггеров
-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION delete_ea_servers()
	RETURNS trigger AS
$BODY$
	BEGIN
		DELETE FROM ea_counters WHERE sid = OLD.sid;
		UPDATE integra_buildings SET server_id = 0 
		    WHERE server_id = OLD.sid;
		UPDATE integra_buildings SET reserved_server_id = 0 
		    WHERE reserved_server_id = OLD.sid;
		RETURN OLD;
	END;
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION process_integra_buildings_hist()
	RETURNS trigger AS
$BODY$
	BEGIN
		IF (TG_OP = 'DELETE') THEN
		    DELETE FROM _system_rest sys
			WHERE sys.table_oid = 'integra_buildings'::regclass::oid 
			AND sys.row_oid = old.oid;
			INSERT INTO integra_buildings_hist SELECT 'D', now(), user, OLD.*;
			RETURN OLD;
		ELSIF (TG_OP = 'UPDATE') THEN
			INSERT INTO integra_buildings_hist SELECT 'U', now(), user, NEW.*;
			RETURN NEW;
		ELSIF (TG_OP = 'INSERT') THEN
			INSERT INTO _system_rest SELECT 'integra_buildings'::regclass::oid, new.oid, isud.user_id, True
			FROM integra_sec_users_def isud ORDER BY isud.user_id;
			INSERT INTO integra_buildings_hist SELECT 'I', now(), user, NEW.*;
			RETURN NEW;
		END IF;
		RETURN NULL;
	END;
$BODY$
LANGUAGE plpgsql;

-------------------------------------------------------------------
-- Создание триггеров
-------------------------------------------------------------------

CREATE TRIGGER integra_buildings_hist
AFTER INSERT OR UPDATE OR DELETE ON integra_buildings
FOR EACH ROW EXECUTE PROCEDURE process_integra_buildings_hist();

CREATE TRIGGER before_delete_ea_servers
BEFORE DELETE ON ea_servers
FOR EACH ROW EXECUTE PROCEDURE delete_ea_servers();

-------------------------------------------------------------------
-- Создание представлений
-------------------------------------------------------------------

CREATE OR REPLACE VIEW integra_buildings_view AS 
    SELECT ib.*, ibr.is_edited FROM integra_buildings ib
    JOIN _system_rest ibr ON ib.tableoid = ibr.table_oid AND ib.oid = ibr.row_oid
    JOIN integra_sec_groups isg ON ib.building_uid = isg.building_uid
    JOIN integra_sec_users isu ON isg.group_id = isu.group_id
    JOIN integra_sec_users_def isud ON isu.user_id = isud.user_id 
        AND isud.user_nick = current_user AND ibr.user_id = isud.user_id
    ORDER BY ib.building_uid;

-------------------------------------------------------------------

CREATE OR REPLACE VIEW integra_fav_plans_view AS
	SELECT ifp.user_id, ifp.building_uid FROM integra_favourite_plans ifp
	INNER JOIN integra_sec_users_def isud ON ifp.user_id = isud.user_id 
	    AND isud.user_nick = current_user;

-------------------------------------------------------------------
-- Создание правил
-------------------------------------------------------------------

CREATE RULE ea_counters_upsert AS ON INSERT TO public.ea_counters
WHERE EXISTS (
	SELECT 1 FROM ea_counters
	WHERE ea_counters.sid = new.sid AND ea_counters.type = new.type
)
DO INSTEAD (
	UPDATE ea_counters SET version = new.version, deleted = new.deleted, plus = new.plus,
	minus = new.minus, exclamation = new.exclamation, question = new.question
	WHERE ea_counters.sid = new.sid AND ea_counters.type = new.type;
);

-------------------------------------------------------------------

CREATE RULE ea_servers_update AS ON UPDATE TO public.ea_servers
WHERE EXISTS (
    SELECT 1 FROM ea_servers
    WHERE ea_servers.address = new.address AND ea_servers.serverid <> new.serverid
)
DO INSTEAD (
    SELECT reinsert_ea_servers(new.address, new.serverid, new.online, new.datetime,
    old.certificate, old.username, old.password, old."interval", old.detailed, old.keepalive, old.bias)
    AS reinsert_ea_servers;
);

-------------------------------------------------------------------

CREATE RULE ea_servers_upsert AS ON INSERT TO public.ea_servers
WHERE EXISTS (
    SELECT 1 FROM ea_servers
    WHERE ea_servers.address = new.address)
DO INSTEAD (
    UPDATE ea_servers SET address = new.address, serverid = new.serverid, online = new.online, datetime = new.datetime
WHERE ea_servers.address = new.address;
);

-------------------------------------------------------------------

CREATE RULE integra_buildings_view_del AS ON DELETE TO public.integra_buildings_view
DO INSTEAD (
	DELETE FROM integra_buildings
	WHERE integra_buildings.building_uid = old.building_uid;
);

-------------------------------------------------------------------

CREATE RULE integra_buildings_view_ins AS ON INSERT TO public.integra_buildings_view
DO INSTEAD (
    INSERT INTO integra_buildings (model_ver, transform, profile, organization, viewpoint,
    preview, addr_postcode, addr_street, addr_housenumber, name, int_name, "position", server_id, building_uid,
    model_uid, ib_hierarchy, submodels, polygon_array, reserved_server_id, transform_web, unload_bound, preview_type, ea_server)
    VALUES (new.model_ver, new.transform, new.profile, new.organization, new.viewpoint,
    new.preview, new.addr_postcode, new.addr_street, new.addr_housenumber, new.name, new.int_name,
    new."position", new.server_id, new.building_uid, new.model_uid, new.ib_hierarchy, new.submodels, new.polygon_array,
    new.reserved_server_id, new.transform_web, new.unload_bound, new.preview_type, new.ea_server)
    RETURNING integra_buildings.model_ver, integra_buildings.transform, integra_buildings.profile,
    integra_buildings.organization, integra_buildings.viewpoint, integra_buildings.preview,
    integra_buildings.addr_postcode, integra_buildings.addr_street, integra_buildings.addr_housenumber,
    integra_buildings.name, integra_buildings.int_name, integra_buildings.server_id, 
    integra_buildings.building_uid, integra_buildings.model_uid, integra_buildings.ib_hierarchy,
    integra_buildings.submodels, integra_buildings.reserved_server_id, integra_buildings.transform_web, 
    integra_buildings.unload_bound, integra_buildings.preview_type, integra_buildings.ea_server, integra_buildings."position", integra_buildings.polygon_array, FALSE;
);

-------------------------------------------------------------------

CREATE RULE integra_buildings_view_upd_mod1 AS ON UPDATE TO public.integra_buildings_view
WHERE old.is_edited = TRUE DO INSTEAD (
    UPDATE integra_buildings SET model_ver = new.model_ver,
    transform = new.transform, profile = new.profile,
    organization = new.organization, viewpoint = new.viewpoint,
    preview = new.preview, addr_postcode = new.addr_postcode,
    addr_street = new.addr_street, addr_housenumber = new.addr_housenumber,
    name = new.name, int_name = new.int_name, "position" = new."position",
    server_id = new.server_id, model_uid = new.model_uid,
    ib_hierarchy = new.ib_hierarchy, submodels = new.submodels,
    polygon_array = new.polygon_array, reserved_server_id = new.reserved_server_id,
    transform_web = new.transform_web, unload_bound = new.unload_bound, preview_type = new.preview_type, ea_server = new.ea_server
    WHERE integra_buildings.building_uid = old.building_uid;
);

-------------------------------------------------------------------

CREATE RULE integra_buildings_view_upd_mod2 AS ON UPDATE TO public.integra_buildings_view
WHERE old.is_edited = FALSE DO INSTEAD NOTHING;

-------------------------------------------------------------------

CREATE RULE integra_buildings_view_upd_main AS ON UPDATE TO public.integra_buildings_view
DO INSTEAD NOTHING;

-------------------------------------------------------------------

CREATE RULE integra_fav_plans_view_del AS ON DELETE TO public.integra_fav_plans_view
DO INSTEAD (
    DELETE FROM integra_favourite_plans
    WHERE integra_favourite_plans.building_uid = old.building_uid AND integra_favourite_plans.user_id = old.user_id;
);

-------------------------------------------------------------------

CREATE RULE integra_fav_plans_view_ins AS ON INSERT TO public.integra_fav_plans_view
DO INSTEAD (
    INSERT INTO integra_favourite_plans (user_id, building_uid)
    VALUES (new.user_id, new.building_uid);
);

-------------------------------------------------------------------
-- Создаем индексы
-------------------------------------------------------------------

CREATE INDEX integra_features_index
    ON integra_features
    USING gist
    ( way );

-------------------------------------------------------------------

CREATE INDEX integra_features_key
    ON integra_features
    USING btree
    ( ogc_fid );

-------------------------------------------------------------------

CREATE INDEX integra_buildings_index
    ON integra_buildings
    USING gist
    ( position );

-------------------------------------------------------------------

CREATE INDEX integra_borders_index
    ON integra_borders
    USING gist
    ( way );

-------------------------------------------------------------------
-- Раздаем привилегии
-------------------------------------------------------------------

GRANT ALL PRIVILEGES ON integra_sec_groups TO PUBLIC;
GRANT SELECT, UPDATE ON integra_sec_users_def TO PUBLIC;
GRANT ALL PRIVILEGES ON integra_favourite_plans TO PUBLIC;
GRANT ALL PRIVILEGES ON integra_fav_plans_view TO PUBLIC;
GRANT SELECT ON integra_buildings_view TO PUBLIC;
GRANT SELECT ON integra_versions TO PUBLIC;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO PUBLIC;

-------------------------------------------------------------------
-- Регистрируем нужных пользователей
-------------------------------------------------------------------

SELECT register_new_user('guest', 'Гость', 'guest', 'guest', 'guest', 'guest', 'guest');
SELECT register_new_user_group('guestgroup', 'NOCREATEROLE');
SELECT register_new_user('monitoring', 'Мониторинг', 'monitoring', 'monitoring', 'monitoring', 'monitoring', 'monitoring');
SELECT register_new_user_group('monitoringgroup', 'NOCREATEROLE');
SELECT register_new_user('operator', 'Оператор', 'operator', 'operator', 'operator', 'operator', 'operator');
SELECT register_new_user_group('operatorgroup', 'NOCREATEROLE');

-------------------------------------------------------------------
-- отдельно назначаем привилегии пользователю мониторинга
-------------------------------------------------------------------

GRANT ALL PRIVILEGES ON ea_counters TO monitoringgroup;
GRANT ALL PRIVILEGES ON ea_servers TO monitoringgroup;
GRANT SELECT ON integra_buildings TO monitoringgroup;
GRANT SELECT ON integra_buildings_view TO monitoringgroup;
GRANT SELECT ON integra_sec_groups TO monitoringgroup;
GRANT SELECT ON integra_sec_groups_def TO monitoringgroup;
GRANT SELECT ON integra_sec_users TO monitoringgroup;
GRANT SELECT ON integra_sec_users_def TO monitoringgroup;
GRANT monitoringgroup TO monitoring;
GRANT operatorgroup TO operator;
GRANT guestgroup TO guest;

UPDATE integra_sec_users_def SET user_surname = 'Администратор' WHERE user_id = 10;
-------------------------------------------------------------------
-- Инициализируем необходимые таблицы
-------------------------------------------------------------------

INSERT INTO integra_sec_users_def(user_id, user_nick) SELECT pg_shadow.usesysid, pg_shadow.usename
FROM pg_shadow WHERE pg_shadow.usesysid NOT IN (SELECT user_id FROM integra_sec_users_def);
INSERT INTO ea_ftps(address, username, password, root_folder, media_folder) 
    VALUES('127.0.0.1:5433', 'ftpadm', 'ftpadm', '/eily.acuario', '/eily.acuario/media/buildings');
INSERT INTO integra_sec_groups_def(group_name, description) VALUES('defaultBuildingGroup', 'default Building Group');
INSERT INTO integra_versions VALUES(2, 2);
--ROLLBACK;