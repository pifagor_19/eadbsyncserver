# -*- coding: utf-8 -*-
'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import querycomposer as qry, ftpconnector as ftp, logging, settingsloader as loader, databaseconnector as db, uuid, copy, dataprovider
from routines import *
#from codecs import getencoder
#import urllib
import traceback

# ------------------------------------------------------------------------------------------------------------
#   Exceptions
# ------------------------------------------------------------------------------------------------------------

# base execption class
class TaskExc(Exception):
    pass

# invalid routine type
class InvalidRTypeExc(TaskExc):
    pass

# task execution exception
class TaskExecExc(TaskExc):
    pass

# task execution assertion
class TaskAlreadyRunningExc(TaskExc):
    pass

# ------------------------------------------------------------------------------------------------------------
#   Task class
# ------------------------------------------------------------------------------------------------------------

class Task:
    # init task and get ref to session
    def __init__(self, session, **kwargs):
        self._session = session
        self._rdict = dict()
        self.load(**kwargs)
        self._isEdited = False

    # create task
    def load(self, **kwargs):
        r_types = {'models': ModelsRoutine, 'syncdir': SyncdirRoutine, 'database': DatabaseRoutine}
        for k,v in kwargs.items():
            setattr(self, k, v)
        if self._rdict: self._rdict.clear()
        routines = getattr(self, 'routines')
        for routine in routines.keys():
            r_pars = routines[routine]
            obj = None
            obj = r_types[routine](self._session, **r_pars) if routine in r_types.keys() else logging.error('Not supported by this version of server')
            if obj: self._rdict[obj._type()] = obj

    # get update params for scheduler
    def updparams(self):
        _r = dict(zip(self._rdict.keys(), [self._rdict[i]._update() for i in self._rdict.keys()]))
        return _r

    # edit task params
    def edit(self, **kwargs):
        self._isEdited = True
        params = ['task_uid', 'task_name', 'database_name',
                  'database_host', 'database_port', 'database_login',
                  'database_password', 'routines', 'ftp_connector']
        for item in params:
            if item in self.__dict__.keys():
                setattr(self, item, None)
        self.load(**kwargs)

    # execute task routines
    def execute(self, routines_to_exec, callback = None):
        try:
            logging.info("Task execution started. Id: %s" % self.param("task_uid"))
            if callable(callback):
                callback('<li>Выполнение задачи</li> <ul type="square">')
            self._session.open_session()
            if not self._session.is_ready():
                logging.error("Unable to execute tasks")
            if not routines_to_exec:
                #raise TaskExecExc
                return False
            if 'models' in routines_to_exec:
                self._rdict['models'].execute(self._plans(), callback)
            if 'syncdir' in routines_to_exec:
                self._rdict['syncdir'].execute(callback)
            if 'database' in routines_to_exec:
                self._rdict['database'].execute(callback)
            if 'models' in routines_to_exec:
                self._rdict['models'].clean(callback)
            logging.info("Task execution completed")
            if callable(callback):
                callback("</ul> <li>Завершение выполнения задачи</li>")
            self._session.close_session()
            return True
        except:
            logging.error( traceback.format_exc() )
            if callable(callback):
                callback("</ul></ul><b>Во время синхронизации произошла ошибка.\nПодробности содержатся в лог-файлах.</b>")
            self._session.close_session()
            return False

    def _plans(self):
        return getattr(self, 'routines')['database']['plans']['ids']

    # get task uid
    def get_uid(self):
        return getattr(self, 'task_uid')

    # set task uid
    def set_uid(self, uid):
        setattr(self, 'task_uid', str(uid))

    # get task desc for web page
    def description(self, addit_info = False):
        t_desc = dict()
        req_fields = ['task_uid', 'task_name', 'database_name', 'database_host',
                      'database_port', 'database_login', 'database_password']
        for item in req_fields: t_desc[item] = getattr(self, str(item))
        t_desc['routines'] = copy.deepcopy(getattr(self, 'routines'))
        return t_desc

    # get specific task parameter
    def param(self, _param):
        return getattr(self, _param)

# ------------------------------------------------------------------------------------------------------------
# Task executor class
# ------------------------------------------------------------------------------------------------------------

class TaskManager(object):
    #init module
    def __init__(self):
        self._db = None
        self.reconnect()
        self.tasks_list = dict()
        self.exec_log = ""
        self._state = False # not executing
        _t = loader.getinfobytag('tasks')
        for task_param in _t:
            session = Session(self.localdb, **task_param)
            issue = Task(session, **task_param)
            self.register_task(issue.get_uid(), issue, session)

    # get task status
    def status(self, info = None):
        return {"msg": self.exec_log, "completed": not self._state}

    # set message
    def set_status(self, msg):
        self.exec_log += "<br>" + msg

    # ~dtor()
    def __del__(self):
        self._db.drop()

    # reconnect to local databse
    def reconnect(self):
        if self._db:
            self._db.drop()
            self._db = None
        self._db = db.DBModule(**loader.getinfobytag('local_settings'))

    # get localdb connector
    def localdb(self):
        return self._db

    # execute all tasks
    def execute_tasks(self, **kwargs):
        if self._state: return
        try:
            callback = self.set_status
            self.exec_log = str()
            if not len(self.get_tasks()):
                if callable(callback):
                    callback("<b>Отсутствуют задачи для синхронизации</b>")
                return
            self._state = True
            if callable(callback):
                callback("<b>Запуск синхронизации</b> <ul type=\"circle\">")
            result = dict()
            for _task in kwargs.keys():
                if _task in self.tasks_list.keys():
                    result[_task] = self.get_task(_task).execute(kwargs[_task], callback)
            if callable(callback):
                callback("</ul><b>Синхронизация завершена.</b>")
            self._state = False
            return result
        except:
            logging.info(traceback.format_exc())
            self._state = False

    # register new task and it session
    def register_task(self, t_uid, task, session):
        self.tasks_list[t_uid] = {"task": task, "session": session}

    # execute all tasks
    def execute_all(self):
        result = True
        todo = dict()
        for uid, task in self.get_tasks().iteritems():
            todo[uid] = task._rdict.keys()
        self.execute_tasks(**todo)
        return True

    # save all tasks
    def save_tasks(self):
        json_task_list = list()
        for uid, task in self.get_tasks().iteritems():
            json_task_list.append(task.description())
        loader.save_tasks('tasks', json_task_list)

    # create new task, if ok - return uid, else: return None
    def add(self, **kwargs):
        #if self.is_executing(): return
        for _task in self.get_tasks().values():
            if ((kwargs['task_name'] == _task.param("task_name"))
                or (kwargs['database_host'] == _task.param('database_host'))):
                return None
        uid = str(uuid.uuid4())
        kwargs['task_uid'] = uid
        _s = Session(self.localdb, **kwargs)
        _t = Task(_s, **kwargs)
        self.tasks_list[uid] = {"task": _t, "session": _s}
        self.save_tasks()
        return uid

    # get execution state
    def is_executing(self):
        return self._state

    # edit task
    def edit(self, **kwargs):
        try:
            #if self.is_executing(): return
            task_uid = kwargs['task_uid']
            edited_task = self.get_task(task_uid)
            edited_task.edit(**kwargs)
            self.save_tasks()
            return task_uid
        except:
            logging.error(traceback.format_exc())

    def getEditedTask(self):
        edited_tasks = list()
        for _uid, _task in self.get_tasks().iteritems():
            if _task._isEdited:
                edited_tasks.append(_uid)
                _task._isEdited = False
        return edited_tasks

    # delete task
    def remove(self, task_uid):
        #if self.is_executing(): return
        if task_uid in self.tasks_list.keys():
            self.tasks_list[str(task_uid)]["session"].close_session()
            del self.tasks_list[str(task_uid)]
        self.save_tasks()

    # get all tasks
    def get_tasks(self):
        return dict(zip(self.tasks_list.keys(), [self.tasks_list[v]["task"] for v in self.tasks_list.keys()]))

    # get task by uid
    def get_task(self, task_uid):
        return self.tasks_list[task_uid]["task"] if task_uid in self.tasks_list.keys() else None

# ------------------------------------------------------------------------------------------------------------
#   Session exceptions
# ------------------------------------------------------------------------------------------------------------

class SessionExc(Exception):
    pass

class FTPExc(SessionExc):
    pass

class DBExc(SessionExc):
    pass

# ------------------------------------------------------------------------------------------------------------
#   Session object - creates connections while executing the task
# ------------------------------------------------------------------------------------------------------------

class Session(object):
    # init session - set db creds
    def __init__(self, callback, **kwargs):
        self._clbk = callback
        self._dbm = db.DBModule(**kwargs)
        self._ftp = None

    # destroy session - delete db connector
    def __del__(self):
        self._dbm = None
        self._ftp = None

    # opens session - create db connection and fetch ftp connection data
    def open_session(self):
        logging.info("OPEN SESSION")
        try:
            self._dbm.open()
            ftp_creds = self._dbm.fetch(qry.get_ftp_info())
            if not ftp_creds:
                raise TaskExecExc
            self._ftp = ftp.FTPModule(*ftp_creds)
        except:
            logging.error(traceback.format_exc())
            raise TaskExecExc

    # close session - destroy db connection and delete ftp connector
    def close_session(self):
        logging.info("CLOSE SESSION")
        if self._ftp: self._ftp.close()
        self._dbm.drop()

    def ftp(self):
        return self._ftp

    def query(self, statement, _data = None, _fetch = True, _main = False):
        try:
            get_data = lambda con: con.fetch(statement, _data) if _fetch else con.execute(statement, _data)
            _r = get_data(self._clbk()) if _main else get_data(self._dbm)
            return _r
        except:
            logging.error(traceback.format_exc())
            raise DBExc

    def download(self, **kwargs):
        try:
            _r = self._ftp.manage_downloads(**kwargs)
            return _r
        except:
            logging.error(traceback.format_exc())
            raise FTPExc

    def upd_res(self, clbk = None, *args):
        try:
            _r = self._ftp.update_resources(clbk, *args)
            return _r
        except:
            logging.error(traceback.format_exc())
            raise FTPExc

    def is_ready(self):
        return (self._dbm and self._ftp)
