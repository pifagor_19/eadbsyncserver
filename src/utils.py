'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

from os import listdir, remove, walk
from os.path import isfile, isdir, join
from shutil import rmtree
import settings, logging, json, sys, hashlib, settingsloader
from jsonschema import Draft3Validator

_home_dir = None

# -----------------------------------------------------------------------------------------
# FTP functions module
# -----------------------------------------------------------------------------------------

def erase_obsolete_models(**kwargs):
    global _home_dir
    logging.info('Erasing obsolete models')
    counter1 = 0
    counter2 = 0
    subfolders_list = [x for x in listdir(_home_dir) if isdir(join(_home_dir, x))]
    for item in subfolders_list:
        if item not in kwargs.keys():
            dir_to_del = join(_home_dir, item)
            rmtree(dir_to_del)
            counter1 += 1
    for key in kwargs.keys():
        folder = key
        file = kwargs[key]
        path = join(_home_dir, folder)
        if isdir(path):
            fileslist = [f for f in listdir(path) if isfile(join(path, f))]
            for item in fileslist:
                if item != file:
                    remove(join(path, item))
                    counter2 += 1
    logging.info('Erasing completed. Deleted folders: %d, files: %d' % (counter1, counter2))

def set_homedir(home):
    global _home_dir
    _home_dir = settingsloader.getinfobytag('settings')['ftp_homedir'] + home

# -----------------------------------------------------------------------------------------
# String formatter
# -----------------------------------------------------------------------------------------

def format_data(curl = "'", delimiter = " = ", **kwargs):
    string = str()
    for key in kwargs.keys():
        string = string + ( str(key) + delimiter + curl + str(kwargs[key]) + curl + ', ')
    string = string[:-2]
    return string

# -----------------------------------------------------------------------------------------
# Import modules
# -----------------------------------------------------------------------------------------

def importModule(m):
    return sys.modules[m] if m in sys.modules else __import__(m)

def callModuleFunc(m, f, **kwargs):
    m = importModule(m)
    f = m.__dict__[f]
    return f(**kwargs)

# -----------------------------------------------------------------------------------------
# JSON proceed module
# -----------------------------------------------------------------------------------------

class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date):
            return str(obj)
        return json.JSONEncoder.default(self, obj)

def convert_to_json(obj):
    c = json.dumps(obj if obj else {}, cls = JSONEncoder)
    return c

def convert_from_json(json_obj):
    c = json.loads(json_obj)
    return c

def validate_task_json(input_json):
    input_json_obj = json.loads(input_json)
    fd = open(settings.VALIDATOR_SCHEMA_NAME).read()
    v_schema = json.loads(fd)
    task_validator = Draft3Validator(v_schema)
    try:
        task_validator.validate(input_json_obj)
        if ( len( input_json_obj['routines'].keys() ) == 0 ):
            raise Exception
        return True
    except Exception as e:
        logging.error("Unable to validate JSON")
        return False

# -----------------------------------------------------------------------------------------
# Exception handling
# -----------------------------------------------------------------------------------------

def exc_handler(err_msg  = None, err_ret = None):
    def outer(func):
        def inner(*args, **kwargs):
            result = None
            try:
                result = func(*args, **kwargs)
                return result
            except Exception as e:
                base_msg = "An exception occured. Details: %s. " % e
                log_msg = err_msg if err_msg else base_msg
                logging.info(log_msg)
                return None if not err_ret else err_ret
        return inner
    return outer

# -----------------------------------------------------------------------------------------
# Cipher module
# -----------------------------------------------------------------------------------------

_shift = 0x71

def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

CIPHER_MODE = enum('CIPHER', 'DECIPHER')

def crypt_text(input_text, cipher_mode = CIPHER_MODE.CIPHER):
    r = ""
    for c in input_text:
        x = ord(c)
        r += chr(x + _shift) if cipher_mode == CIPHER_MODE.CIPHER else chr(x - _shift)
    return r

def md5_hash(input_text):
    m = hashlib.md5()
    m.update(input_text)
    return m.hexdigest()
