# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 02.04.2013
Updated: 02.04.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import os, ftplib, logging, pprint, multiprocessing, thread, shutil, traceback, settings
from FTPFileDir import FTPDirectory
count = 0

def synchronize( url, port, user, passw, fdir, ldir ):
    sysFile = '.ftpsyncdir'
    _ftp = ftplib.FTP(timeout=10)
    logging.debug('%s', _ftp.connect(url, port))
    logging.debug('%s', _ftp.login(user, passw, 10))
    global count
    count = 0
    
    logging.info(_ftp.sendcmd("FEAT"))
    logging.info(_ftp.sendcmd("OPTS UTF8 ON"))
    logging.info(_ftp.sendcmd("PWD"))
    logging.info(_ftp.sendcmd("TYPE A"))
    logging.info(_ftp.sendcmd("PASV"))
    logging.info(_ftp.sendcmd("CWD /"))
    logging.info(_ftp.sendcmd("MLSD"))
    
    def sync_res(ftp_dir):
        global count
        _ftp.cwd(ftp_dir)
        f_items = [os.path.basename(d) for d in _ftp.nlst()]
        #logging.debug(f_items)
        logging.info(_ftp.sendcmd("OPTS UTF8 ON"))
        logging.info(_ftp.sendcmd("TYPE A"))
        logging.info(_ftp.sendcmd("PASV"))
        logging.info(_ftp.sendcmd("MLST"))
        files = list()
        dirs = list()
        for i in f_items:
            try:
                _ftp.cwd(ftp_dir + '/' + i)
                dirs.append(i)
            except:
                files.append(i)
        if not dirs: return
        for i in files:
            #logging.info(i)
            logging.info("%s \t\t %s" % (i, _ftp.sendcmd("MDTM %s" % ftp_dir+"/"+i)))
        [sync_res(ftp_dir + '/' + e) for e in dirs]

    #sync_res(fdir)


def sync(url, port, user, passw, fdir, ldir, clbk = None, codec = 'windows_1251', logfmt = '%(levelname)s - %(message)s', loglvl = logging.INFO, logfile = None):
    try:
        #logging.basicConfig(format = logfmt, level = loglvl, filename = logfile)
        hfile = '.ftpsyncdir'
        ftp = ftplib.FTP(timeout = settings.TIMEOUT_INTERVAL)
        logging.info('%s', ftp.connect(url, port))
        logging.info('%s', ftp.login(user, passw, settings.TIMEOUT_INTERVAL))
        global count
        count = 0
        try:
            os.stat(ldir)
        except:
            os.mkdir(ldir)

        def sync_all(fdir,ldir ):
            global count
            dictr = {}
            dictfilesch = {}

            allitems = [os.path.basename(d) for d in ftp.nlst(fdir)]
            lallitems= os.listdir(ldir)
            for delitem in lallitems:
                if not delitem in allitems and delitem!=hfile:
                    count += 1
                    logging.info('REMOVE %s', ldir+'/'+delitem)
                    if os.path.isfile(ldir+'/'+delitem):
                        try:
                            os.remove(ldir+'/'+delitem)
                        except Exception, e:
                            logging.warn('%s', e)
                    else:
                        shutil.rmtree(ldir+'/'+delitem)

            files = []
            dirs = []
            for item in allitems:
                try:
                    #ftp.sendcmd('MDTM %s' % fdir+'/'+item)
                    ftp.cwd(fdir + '/' + item)
                    logging.info('%s\n%s', ldir, pprint.pformat(item))
                    dirs.append(item)
                except:
                    files.append(item)

            if os.path.exists(ldir+'/'+hfile):
                try:
                    dictr = eval(open(ldir+'/'+hfile,'rb').read())
                except Exception, e:
                    logging.warning('%s', e)

            for file in files:
                filetime = ftp.sendcmd('MDTM %s' % fdir+'/'+file)
                dictfilesch[file] = filetime
                try:
                    timedif = filetime != dictr[file]
                except:
                    timedif = True
                if not os.path.exists(ldir+'/'+file) or timedif:
                    try:
                        logging.info('Transfering %s', fdir + '/' +file)
                        if callable(clbk):
                            clbk("<li>" + file + "</li>")
                        lf = open(unicode((ldir +'/'+ file).decode('utf-8')), 'wb')
                        ftp.retrbinary("RETR " + (fdir + '/' +file), lf.write, 8 * 1024)
                        count += 1
                        lf.close()
                    except Exception, e:
                        logging.info('transfer file failed')
                        dictfilesch[file] = ''
                        logging.error('%s', e)
                open(ldir+'/'+hfile, 'wb').write(pprint.pformat(dictfilesch))

            for dir in dirs:
                if not os.path.exists(ldir+'/'+dir):
                    os.mkdir(ldir+'/'+dir )
                nfdir = fdir + '/' + dir
                nldir = ldir + '/' + dir
                sync_all(nfdir,nldir)
        sync_all(fdir,ldir)
        logging.info('%s', ftp.quit())

        return count
    except Exception, e:
        logging.critical('%s', traceback.format_exc())
        return -1

def _sync(r, *args):
    r.value = sync(*args)

def async(url, port, user, passw, fdir, ldir, callback, codec = 'windows_1251'):
    r = multiprocessing.Value('i', -1)
    print r.value
    p = multiprocessing.Process(target = _sync, args=(r, url, port, user, passw, fdir, ldir, codec))
    p.start()
    def waitexit( p, r, callback):
        p.join()
        callback(r.value)
    thread.start_new_thread( waitexit, ( p, r, callback))



def nothing(ex):
    if ex<0:
        print('ERROR')
    else:
        print(' SYNC CALLBACK OK')

if __name__ == '__main__':
    #synchronize('192.168.40.114', '5433', 'ftpsync', 'ftpsync', '/eaintegration', 'C:\_FTP_')
    #synchronize('192.168.10.25', '5433', 'ftpsync', 'ftpsync', '/eaintegration', 'C:\_FTP_')
    synchronize('192.168.10.31', '5433', 'ftpsync', 'ftpsync', '/eaintegration', 'C:\_FTP_')
