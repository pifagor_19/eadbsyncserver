#!/usr/bin/env python
class Node():
    def __init__(self, name):
        self.name = name
        self.files = []
        self.folders = {}
        
    def __str__(self):
        return '%s: files: %s, folders: %s' % (self.name, self.files, self.folders)

def find_node(top_node, name):
    print top_node.folders, name
    result = top_node
    path = name.split(os.sep)[1:]
    for path_comp in path:
        result = result.folders[path_comp]
    return result

PATH = "C:\\ftp\\eily.acuario\\eaintegration"

import sys, os
tree = Node(PATH)
for root, folders, files in os.walk(PATH):
    parent_node = find_node(tree, root)
    for folder in folders:
        parent_node.folders[folder] = Node(folder)
    for file in files:
        parent_node.files.append(file)

print tree