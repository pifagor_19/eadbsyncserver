#!/usr/bin/env python
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 23.04.2014
Updated: 26.04.2013
Author: Alex Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import os, hashlib, json, logging, settings

PATH = ""
d = {PATH: None}

def gen_md5(f, block_size = 2048):
    _f = open(f)
    md5 = hashlib.md5()
    while True:
        data = _f.read(block_size)
        if not data: break
        md5.update(data)
    return md5.hexdigest()

class Folder(object):
    def __init__(self, name):
        self._name = name
        self.files = {}
        self.folders = {}
        
def gen_tree(folder_path):
    global PATH, d
    PATH = folder_path
    root = Folder(PATH)
    d = {PATH: root}
    for path, folders, files in os.walk(PATH):
        cur = d[path]
        for file in files:
            _name, _ext = os.path.splitext(file)
            if _ext != ".sync" and _name != ".ftpsyncdir":
                cur.files[file] = gen_md5(os.path.join(path, file))
        for folder in folders:
            f = Folder(folder)
            cur.folders[folder] = f
            d[os.path.join(path, folder)] = f

def iter(el, path_, dict_):
    for k,v in el.files.iteritems():
        _k = find_key(el)
        _abs_path = os.path.join(_k, k)
        _rel_path = os.path.relpath(_abs_path, path_)
        dict_[_rel_path] = v
    for item in el.folders:
        iter(el.folders[item], path_, dict_)

def find_key(val):
    for k,v in d.iteritems():
        if v == val: return k

def dump_info(_path):
    src = dict()
    iter(d[_path], _path, src)
    _sysFile = os.path.join(_path, "info.sync")
    if os.path.isfile(_sysFile):
        os.remove(_sysFile)
    f = open(_sysFile, "a")
    f.write(json.dumps(src))
    f.close()

def build_ftp():
    global PATH, d
    dump_info(PATH)
    for path, folders, files in os.walk(PATH):
        for folder in folders:
            dump_info(os.path.join(path, folder))

def serve():
    logging.info("Generation start")
    gen_tree("C:\\ftp\\eily.acuario\\eaintegration")
    build_ftp()
    logging.info("Generation end")