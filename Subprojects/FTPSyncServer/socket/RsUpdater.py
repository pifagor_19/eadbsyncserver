# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario
For the latest info, see http://integra-s.com/
Created: 14.11.2013
Author: Golodov Aleksey mailto:golodov@integra-s.com
Copyright (c) 2013 Integra-S JSC
'''
#=========================================================================================================
PLUGIN_NAME         = 'RsUpdater'
PLUGIN_AUTHOR       = 'Golodov Aleksey'
PLUGIN_TYPE         = 'base'
PLUGIN_VERSION      = '2.1'
PLUGIN_DESCRIPTION  = 'Обновление ресурсов и локализации системы'
PLUGIN_SHOW         = 'true'
PLUGIN_MENU = [
               'updateMedia:Проверить обновления реcурсов'
               ,'updateLocale:Проверить обновления локализации'
               ]
#=========================================================================================================

WEB_RESOURCE_VERSION = '1.0.0'

PLUGINS_CACHE_PATH = 'C:/test'
IPE_CACHE_PATH = 'C:/ftp'

WEB_UPDATE_PATH =  '/eaintegration/web/%s' % WEB_RESOURCE_VERSION
MEDIA_UPDATE_PATH = '/eaintegration/media/'
LOCALE_UPDATE_PATH = '/eaintegration/locale/'

__version__ = PLUGIN_VERSION

import os,sys, datetime, shutil, errno
sys.path.append('../SharedLibs')
import sys, types, os, logging, ftpsyncdir_mod as ftpsyncdir, traceback, argparse
from datetime import datetime


SECTION = {
           'web'    : 'Web-ресурсы',
           'media'  : 'Медиа-ресурсы',
           'locale' : 'Файлы локализации'
           }


class PluginInterface:
    def __init__(self):
        self.tmp_media = PLUGIN_CACHE_PATH+'/eaintegration/media'
        self.ipe_media = os.getcwd() + '/media'
        self.skip_notify = False
        self.updateMedia(skip_success = True)
        self.updateLocale(skip_success = True)
        self.updateWeb(skip_success = True)

    def updateMedia(self, skip_success = False ):
        PluginInterface.Update('media',MEDIA_UPDATE_PATH , os.getcwd() + '/media', FTPHOST, FTPPORT
                               , delete_absents = False
                               , callback = lambda x : PluginInterface.sectionUpdateCompleted( 'media', skip_success , x ))

    def updateLocale(self, skip_success = False):
        PluginInterface.Update('locale', LOCALE_UPDATE_PATH , PLUGINS_CACHE_PATH + '/eaintegration2/locale', FTPHOST, FTPPORT
                               , lambda x : PluginInterface.sectionUpdateCompleted( 'locale', skip_success, x ))

    def updateWeb(self, skip_success = False):
        PluginInterface.Update('web', WEB_UPDATE_PATH, os.getcwd() + '/media/www', FTPHOST, FTPPORT
                               , delete_absents = False
                               , callback = lambda x : PluginInterface.sectionUpdateCompleted( 'web', skip_success , x ))

    @staticmethod
    def Update( section, _from, _to, host = '', port = '21', callback = None, delete_absents = True):
        print 'updating section <%s> from %s to %s' % (section, _from, _to )
        if host:
            section = section.lower()
            ftpsyncdir.async( host, port, 'ftpsync', 'ftpsync', _from, _to , callback, deleteAbsents=delete_absents )
            #ftpsyncdir.synchronize( host, port, 'ftpsync', 'ftpsync', _from, _to , callback, deleteAbsents=delete_absents )
            #if section == 'web':
            #    ftpsyncdir.async( host, port, 'web_sync', 'web_sync', _from, _to ,  callback , deleteAbsents=delete_absents )
            #elif section in [ 'locale', 'media' ]:
            #    ftpsyncdir.async( host, port, 'ftpsync', 'ftpsync', _from, _to , callback, deleteAbsents=delete_absents )
    
    @staticmethod
    def UpdateWeb( _from, _to, host = '', port = '21', callback = None, delete_absents = False ):
        
        if host:
            #ftpsyncdir.synchronize(host, port, 'ftpsync', 'ftpsync', _from, _to ,  callback , deleteAbsents=delete_absents)
            ftpsyncdir.async( host, port, 'ftpsync', 'ftpsync', _from, _to ,  callback , deleteAbsents=delete_absents )
    
    @staticmethod
    def sectionUpdateCompleted( section, skip_success, exitcode ):
        print 'section <%s> update completed with status %s' % ( section, exitcode )
        if exitcode > 0:
            try:
                IPEObjectFactory.getHelper().showNotifiy( 'Обновлены %s системы.\n Для примения изменений перезапустите приложение.' % (SECTION.get(section, '')).lower(), True )
            except:
                pass
        elif exitcode == 0 :
                try:
                    if not skip_success:
                        IPEObjectFactory.getHelper().showNotifiy( '%s системы не требуют обновления.' % SECTION.get(section, ''), True );
                except:
                    pass

    def localeUpdateCompleted( self, exitcode ):
        if exitcode > 0:
            IPEObjectFactory.getHelper().showNotifiy( 'Обновлена локализация системы.\n Для примения изменений перезапустите приложение.', False );

    def mediaUpdateCompleted( self, exitcode ):
        print 'update completed with status %s' % exitcode
        if exitcode > 0:
            if os.path.isdir( self.tmp_media ):
                stamp = open( self.tmp_media+'/last_success_update', 'w' )
                stamp.write( str(datetime.utcnow()) )
                stamp.close()
                IPEObjectFactory.getHelper().showNotifiy( 'Обновлены ресурсы системы.\n Для примения изменений перезапустите приложение.', False );
        elif exitcode == 0 :
            if not self.skip_notify:
                IPEObjectFactory.getHelper().showNotifiy( 'Ресурсы системы не требуют обновления.', True );
            else:
                self.skip_notify = False

def section_upd_complete( caller, code ):
    if not code:
        print '%s is up to date' % caller
    elif code > 0:
        print ' %s successfully updated' % caller

def main():
    # parser = argparse.ArgumentParser( description = 'ftpsync script' )
    # parser.add_argument( '--section', default = 'web', help = 'sets section update from (default: media)' )
    # parser.add_argument( '--source', default = WEB_UPDATE_PATH, help = 'remote source dir (default: "WEB_UPDATE_PATH" )' )
    # parser.add_argument( '--dest', default = PLUGIN_CACHE_PATH, help = 'local destination dir (default: "/" )' )
    # parser.add_argument( '--host', default = 'ipe.integra-s.com', help = 'local destination dir (default: "ipe.integra-s.com" )' )
    # parser.add_argument( '--port', default = '5433', help = 'local destination dir (default: "5433" )' )
    # parser.add_argument( '--delete_unused', default = 'True', help = 'local destination dir (default: "True" )' )
    # cfg = parser.parse_args()
    #PluginInterface.Update('web', WEB_UPDATE_PATH, PLUGIN_CACHE_PATH, 'ipe.integra-s.com', 5433, None, False)
    #PluginInterface.Update('media', WEB_UPDATE_PATH, PLUGIN_CACHE_PATH, 'ipe.integra-s.com', 5433, None, False)
    #PluginInterface.Update('locale', WEB_UPDATE_PATH, PLUGINS_CACHE_PATH, 'ipe.integra-s.com', 5433, None, False)

    PluginInterface.Update('media',
                           MEDIA_UPDATE_PATH ,
                           IPE_CACHE_PATH + '/media',
                           'ipe.integra-s.com',
                           5433,
                           delete_absents = False,
                           callback = None)

    PluginInterface.Update('locale',
                           LOCALE_UPDATE_PATH,
                           PLUGINS_CACHE_PATH + '/eaintegration2/locale',
                           'ipe.integra-s.com',
                           5433,
                           delete_absents = True,
                           callback = None)

    PluginInterface.Update('web',
                           WEB_UPDATE_PATH,
                           IPE_CACHE_PATH + '/media/www',
                           'ipe.integra-s.com',
                           5433,
                           delete_absents = False,
                           callback = None)




    # if cfg.section.lower() == 'web':
    #
    #     delete = True
    #     if cfg.delete_unused.lower() == 'false':
    #          delete = False
    #     print 'from %s to %s --- delete %s' % (cfg.source, cfg.dest, delete)
    #     PluginInterface.Update(  cfg.section, cfg.source, cfg.dest ,cfg.host,cfg.port, lambda x : section_upd_complete( cfg.section, x  ), delete )
                
if __name__ == '__main__':
    main()