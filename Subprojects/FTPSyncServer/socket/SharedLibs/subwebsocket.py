# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 27.03.2013
Updated: 11.04.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import os, logging, websocket, logging, subprocess, threading, thread, time, utils, subprocessing

_exe = None
_process = None
_url = None
_header = None
_lock = threading.Lock()
_socket = None
_callback = None

def _stop():
    global _process, _socket, _callback
    try:
        if _socket:
            _socket.close() 
            logging.info('disconnected')
        if _process:
            _process.kill()
            _process = None
            logging.info('stopped')
    except Exception, e: 
        logging.error('%s', e)
    finally:
        if _socket:
            _callback('setState', False)
        _socket = None
        _process = None

def _connect():
    global _exe, _process, _url, _header, _lock, _socket
    import websocket
    if _url:
        _lock.acquire()
        try:
            if _exe and not _process:
                _process = subprocess.Popen(_exe, cwd = os.path.dirname(_exe[0]))
                logging.info('started %s', _exe)
                time.sleep(2)
            if not _socket:
                _socket = websocket.create_connection(_url, header = _header)
                logging.info('connected to %s', _url)
                _callback('setState', True)
            return True
        except Exception, e: 
            logging.error('%s', e)
            time.sleep(2)
        finally: 
            _lock.release()
        _stop()
        return False

def _receiving():
    global _url, _socket, _callback
    while _url:
        try:
            if(_connect()):
                msg = _socket.recv()
                _callback('receive', msg)
        except Exception, e: 
            if _url:
                logging.error('%s', e)
                _stop()
        time.sleep(0.001)

def configureLogging(loglvl, logfile):
    utils.configureLogging(loglvl, logfile)

def connect(exe, url, header):
    global _exe, _url, _header, _callback
    logging.info('connect to %s(%s)', url, exe)
    _exe = exe
    _url = url
    _header = header
    _callback = connect.callback
    thread.start_new_thread(_receiving, ())

def send(what):
    global _socket
    logging.info('send %d bytes', len(what))
    if(_connect()):
        try:
            _socket.send(what)
            logging.info('sent %d bytes', len(what))
            return True
        except Exception, e:
            logging.error('%s', e)
            _stop()
    return False

def disconnect():
    global _exe, _url, _header
    logging.info('dicsonnect from %s(%s)', _url, _exe)
    _header = _url = _exe = None
    _stop()

class Socket(object):
    def __init__(self, url, exe = None, header = {}, recv = None, state = None, loglvl = logging.INFO, logfile = None, auto = True):
        self.recv = recv
        self.state = state
        self.process = subprocessing.Process(functions = [configureLogging, connect, send, disconnect], callbacks = [self.setState, self.receive], auto = auto)
        self.process(configureLogging, loglvl, logfile)
        self.process(connect, exe, url, header)

    def setState(self, connected):
        if self.state:
            self.state(connected)

    def receive(self, msg):
        if self.recv:
            self.recv(msg)

    def send(self, what):
        self.process(send, what)

    def loop(self):
        self.process.loop(callbacks = [self.setState, self.receive])

    def stop(self):
        self.process(disconnect)
        self.process.stop()

    def join(self):
        self.process.join()
