# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 30.05.2013
Updated: 30.05.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import types, threading, traceback, utils, subprocessing

def callModuleFunc(m, f, *args, **kwargs):
    try:
        callModuleFunc.callback('callback', (True, utils.callModuleFunc(m, f, *args, **kwargs)))
    except:
        callModuleFunc.callback('callback', (False, traceback.format_exc()))

class Process(object):
    def __init__(self, auto = True):
        self.event = threading.Event()
        self.event.set()
        self.process = subprocessing.Process(functions = [callModuleFunc], callbacks = [self.callback], auto = auto)
        self.result = None

    def callback(self, result):
        self.result = result
        self.event.set()

    def beginModuleFunc(self, m, f, *args, **kwargs):
        self.event.clear()
        self.process(callModuleFunc, m, f, *args, **kwargs)

    def endModuleFunc(self, t):
        if not self.event.wait(t):
            raise Exception('operation times out')
        if not self.result[0]:
            raise Exception(self.result[1])
        return self.result[1]

    def callModuleFunc(self, t, m, f, *args, **kwargs):
        self.beginModuleFunc(m, f, *args, **kwargs)
        return self.endModuleFunc(t)

    def isBusy(self):
        return not self.event.isSet()

    def loop(self):
        self.process.loop(callbacks = [self.callback])

    def stop(self):
        self.process.stop()

    def join(self):
        self.process.join()

    def terminate(self):
        self.process.terminate()

class Pool(object):
    def __init__(self, size = 10, auto = True):
        self.pool = [Process(auto) for i in range(size)]

    def callModuleFunc(self, t, m, f, *args, **kwargs):
        for p in self.pool:
            if not p.isBusy():
                return p.callModuleFunc(t, m, f, *args, **kwargs)
        raise Exception('pool is busy')

    def size(self):
        return len(self.pool)

    def loop(self):
        for p in self.pool:
            p.loop()

    def stop(self):
        for p in self.pool:
            p.stop()

    def join(self):
        for p in self.pool:
            p.join()

    def terminate(self):
        for p in self.pool:
            p.terminate()
