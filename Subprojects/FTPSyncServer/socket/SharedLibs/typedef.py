
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 13.03.2013
Updated: 06.07.2013
Updated: 31.01.2014
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import sys, types, glob, xml.dom.minidom, logging, traceback;
from utils import one

def printxml(node):
    node.writexml(sys.stdout, indent = ' ', addindent = ' ', newl = '\n')

def buildmap(node, tag, getvalue, getkey):
    res = {}
    for i in [i for i in node.childNodes if i.nodeName == tag]:
        value = getvalue(i)
        res[getkey(value)] = value
    return res

def buildlist(node, tag, getvalue):
    res = []
    for i in [i for i in node.childNodes if i.nodeName == tag]:
        res.append(getvalue(i))
    return res

class base(object):
    def __init__(self, node):
        self.attrs = reduce(lambda d, i : dict(d.items() + {i[1][0]: i[1][1]}.items()), enumerate(node.attributes.items()), {})

class value(base):
    def __init__(self, node):
        super(value, self).__init__(node)
        self.value = node.firstChild.nodeValue if node.firstChild else None
        self.category = self.attrs.get('category', '?')

class baseobj(base):
    def __init__(self, node):
        super(baseobj, self).__init__(node)
        self.type = self.attrs['type']

class enum(baseobj):
    def __init__(self, node):
        super(enum, self).__init__(node)
        self.values = buildmap(node, 'value', lambda i : value(i), lambda i : i.value)

class param(base):
    def __init__(self, node, enums):
        super(param, self).__init__(node)
        self.value = node.firstChild.nodeValue if node.firstChild else None
        self.name = self.attrs['name'].lower()
        self.type = self.attrs.get('type', 'text')
        self.index = int(self.attrs.get('index', -1))
        self.access = int(self.attrs.get('access', 100))
        self.many = bool(self.attrs.get('many', False))
        self.min = self.attrs.get('min')
        self.max = self.attrs.get('max')
        self.match = self.attrs.get('match')
        self.enum = self.attrs.get('enum')
        self.enum = enums.get(self.enum)

class params(baseobj):
    def __init__(self, node, enums):
        super(params, self).__init__(node)
        self.params = buildmap(node, 'param', lambda i : param(i, enums), lambda i : i.name)

class pin(params):
    def __init__(self, node, enums):
        super(pin, self).__init__(node, enums)
        self.name = self.attrs.get('name', self.type)
        self.many = bool(self.attrs.get('many', False))

class heir(params):
    def __init__(self, node, enums):
        super(heir, self).__init__(node, enums)
        self.abstract = bool(self.attrs.get('abstract', False))
        self.is_ = buildlist(node, 'is', lambda i : i.getAttribute('type'))

class link(heir):
    def __init__(self, node, enums):
        super(link, self).__init__(node, enums)
        self.in_ = self.attrs.get('in')
        self.out = self.attrs.get('out')

class item(heir):
    def __init__(self, node, enums):
        super(item, self).__init__(node, enums)
        self.pins = buildmap(node, 'pin', lambda i : pin(i, enums), lambda i : i.name)

class TypeDef(object):
    def __completeis(self, item, items):
        res = []
        res.append(item.type)
        for i in item.is_:
            res.append(i)
            i = items[i]
            is_ = self.__completeis(i, items)
            for j in [j for j in is_ if j not in res]:
                res.append(j)
        return res

    def _completeis(self, items):
        map = {}
        for k,v in items.items():
            map[k] = self.__completeis(v, items)
        for k,v in items.items():
            v.is_ = map[k]

    def __completeparams(self, item, items, hist = []):
        res = item.params
        for i in item.is_:
            i = items[i]
            if not i in hist:
                params = self.__completeparams(i, items, hist + [i])
                res = reduce(lambda d, i : dict(d.items() + {i: params[i]}.items()), [k for k,v in params.items() if k not in res], res)
        return res

    def _completeparams(self, items):
        for k,v in items.items():
            v.params = self.__completeparams(v, items)

    def __completepins(self, item, hist = []):
        res = item.pins
        for i in item.is_:
            i = self.items[i]
            if not i in hist:
                pins = self.__completepins(i, hist + [i])
                res = reduce(lambda d, i : dict(d.items() + {i: pins[i]}.items()), [k for k,v in pins.items() if k not in res], res)
        return res

    def _completepins(self):
        for k,v in self.items.items():
            v.pins = self.__completepins(v)
            for pk,pv in v.pins.items():
                params = self.pins[pv.type].params
                pv.params = reduce(lambda d, i : dict(d.items() + {i: params[i]}.items()), [k for k,v in params.items() if k not in pv.params], pv.params)

    def _complete(self):
        self._completeis(self.links)
        self._completeparams(self.links)
        self._completeis(self.items)
        self._completeparams(self.items)
        self._completepins()

    def __init__(self, path = './typedef'):
        super(TypeDef, self).__init__()
        try:
            _dom = xml.dom.minidom.parseString('<root></root>');
            for filename in glob.glob(path + '/*.xml'):
                with open(filename, 'r') as file:
                    text = ''.join(line.rstrip() for line in file)
                    dom = xml.dom.minidom.parseString(text)
                    for i in dom.firstChild.childNodes: 
                        if xml.dom.minidom.Node.ELEMENT_NODE == i.nodeType:
                            _dom.firstChild.appendChild(i)
            #printxml(_dom.firstChild)
            self.enums = buildmap(_dom.firstChild, 'enum', lambda i : enum(i), lambda i : i.type)
            self.pins = buildmap(_dom.firstChild, 'pin', lambda i : pin(i, self.enums), lambda i : i.type)
            self.links = buildmap(_dom.firstChild, 'link', lambda i : link(i, self.enums), lambda i : i.type)
            self.items = buildmap(_dom.firstChild, 'item', lambda i : item(i, self.enums), lambda i : i.type)
            self._complete()
        except Exception, e: 
            logging.fatal('%s', traceback.format_exc())

