# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 27.03.2013
Updated: 14.04.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import logging, websocket, logging, logging.config, logging.handlers, subprocessing, socket

_url = None
_header = None
_socket = None
_callback = None

def _stop():
    global _socket
    try:
        if _socket:
            _socket.close() 
    except Exception, e: 
        logging.error('%s', e)
    finally:
        if _socket:
            _callback('onstate', False)
        _socket = None

def _connect():
    global _url, _header, _socket
    if _url:
        try:
            if not _socket:
                _socket = websocket.create_connection(_url, header = _header)
                logging.info('connected to %s', _url)
                _callback('onstate', True)
            return True
        except Exception, e: 
            logging.error('%s', e)
        _stop()
        return False

def configureLogging(config):
    logging.config.dictConfig(config)

def connect(url, header):
    global _header, _url, _callback
    logging.info('connect to %s', url)
    _url = url
    _header = header
    _callback = connect.callback

def send(id, *what):
    global _socket, _callback
    logging.info('send %d messages', len(what))
    if(_connect()):
        try:
            for i in what:
                _socket.send(i)
                logging.info('sent %d bytes', len(i))
            what = _socket.recv()
            logging.info('received %d bytes', len(what))
            _callback('onrecv', id, what)
        except Exception, e:
            logging.error('%s', e)
            _stop()

def disconnect():
    global _url, _header, _socket
    logging.info('dicsonnect from %s', _url)
    _header = _url = None
    if _socket:
        _socket.sock.shutdown(socket.SHUT_RDWR)
    _stop()

class Socket(object):
    def __init__(self, url, header = {}, recv = None, state = None, logcfg = None, auto = True):
        self.recv = recv
        self.state = state
        self.process = subprocessing.Process(functions = [configureLogging, connect, send, disconnect], callbacks = [self.onstate, self.onrecv], auto = auto)
        if logcfg:
            self.process(configureLogging, logcfg)
        self.process(connect, url, header)

    def onstate(self, connected):
        if self.state:
            self.state(connected)

    def onrecv(self, id, msg):
        if self.recv:
            self.recv(id, msg)

    @property
    def busy(self):
        return self.process.busy

    def send(self, id, *what):
        self.process(send, id, *what)

    def loop(self):
        self.process.loop(callbacks = [self.onstate, self.onrecv])

    def stop(self):
        self.process(disconnect)
        self.process.stop()

    def join(self):
        self.process.join()
