# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 13.03.2013
Updated: 13.06.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import psycopg2, psycopg2.extras, psycopg2.pool, logging

pool = None

def start(constr, maxcon = 1):
    global pool
    pool = psycopg2.pool.ThreadedConnectionPool(1, maxcon, constr)

def stop():
    global pool
    pool.closeall()
    pool = None

def update(timeout, sql, *args, **kwargs):
    global pool
    con = pool.getconn()
    try:
        cur = con.cursor()
        try:
            logging.info('UPDATE: %s %s %s', sql, args, kwargs)
            cur.execute('set statement_timeout to %s', (timeout * 1000,))
            cur.execute(sql, kwargs or args)
            con.commit()
            return [{'result': True}]
        except: 
            con.rollback()
            raise
        finally: cur.close()
    finally: pool.putconn(con)

def query(timeout, limit, sql, *args, **kwargs):
    global pool
    res = None
    con = pool.getconn()
    try:
        cur = con.cursor()
        try:
            logging.debug('QUERY: %s %s %s', sql, args, kwargs)
            cur.execute('set statement_timeout to %s', (timeout * 1000,))
            cur.execute(sql, kwargs or args)
            res = [dict((cur.description[i][0], value) for i, value in enumerate(row)) for row in cur.fetchmany(limit)]
        finally: cur.close()
    finally: pool.putconn(con)
    return res

def execute(timeout, sql, *args, **kwargs):
    global pool
    con = pool.getconn()
    try:
        cur = con.cursor()
        old_isolation_level = con.isolation_level
        con.set_isolation_level(0)
        try:
            logging.info('EXECUTE: %s %s %s', sql, args, kwargs)
            cur.execute('set statement_timeout to %s', (timeout * 1000,))
            cur.execute(sql, kwargs or args)
            return [{'result': True}]
        except: 
            raise
        finally: 
            con.set_isolation_level(old_isolation_level)
            cur.close()
    finally: pool.putconn(con)
