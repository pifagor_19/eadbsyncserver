# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 13.03.2013
Updated: 18.07.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import os, time, thread, logging, logging.config, logging.handlers, json, inspect, traceback, copy, mimetypes
import cherrypy, cherrypy.lib.auth_basic, cherrypy.lib.sessions
import pgdb, utils, eaglob, subprocessing

from ws4py.server.cherrypyserver import WebSocketPlugin, WebSocketTool
from ws4py.websocket import WebSocket
from ws4py.messaging import TextMessage
from ws4py.messaging import BinaryMessage

class WebServer(object):
    def __init__(self):
        pass

    @cherrypy.expose
    def ws(self):
        pass

    @cherrypy.expose
    def queryformat(self, format, module, _, func, **kwargs):
        r = eaglob.callModuleFunc(module, func, **kwargs)
        r = utils.tojson(r)
        return format % r

    @cherrypy.expose
    @cherrypy.tools.json_out(handler = lambda *args, **kwargs : utils.tojson(cherrypy.serving.request._json_inner_handler(*args, **kwargs)))
    def queryjson(self, module, func, **kwargs):
        return eaglob.callModuleFunc(module, func, **kwargs)

    def canloadfile(self, path):
        return True;

    @cherrypy.expose
    def default(self, *args, **kwargs):
        path = reduce(lambda r,i : r + '/' + i, args, '.')
        if not self.canloadfile(path):
            raise Exception('access denied loading %s' % path)
        cherrypy.response.headers["Content-Type"] = mimetypes.guess_type(path)[0]
        return open(path)

class WebSocketHandler(WebSocket):
    def opened(self):
        logging.info('websocket connected')

    def received_message(self, m):
        try:
            if m:
                m = json.loads(m.data)
                r = eaglob.callModuleFunc(m['module'], m['func'], **m['kwargs'])
                r = utils.tojson(r)
                self.send(BinaryMessage(r if r else ''))
        except Exception, e:
            logging.warn('%s', traceback.format_exc())
            self.send(TextMessage('EXCEPTION: %s' % e))

    def once(self):
        try:
            return super(WebSocketHandler, self).once()
        except:
            logging.warning('websocket unhandled exception %s', traceback.format_exc())
            return False

    def closed(self, code, reason):
        logging.info('websocket disconnected (code: %d, reason: %s)' % (code, reason))

def startplugin(args, logsettings, callbacks):
    try:
        args = eval(args)
        m = args['module']
        f = args.get('func', 'run')
        kwargs = args.get('kwargs', {})
        logcfg = copy.deepcopy(logsettings)
        kwargs.update({'logcfg': logcfg})
        m = utils.importModule(m)
        f = m.__dict__[f]
        logging.debug('%s\n%s', inspect.getargspec(f), args)
        while True:
            try:
                p = subprocessing.Process(functions = [f], callbacks = callbacks)
                p(f, **kwargs)
                p.join()
                p.stop()
            except:
                logging.warning('%s\n%s', traceback.format_exc(), args)
                time.sleep(5)
    except:
        logging.error('%s\n%s', traceback.format_exc(), args)

def checkCredentials(realm, username, password, opts):
    try:
        user = utils.loginToLDAP(eaglob.cfg.ldapurl, eaglob.cfg.ldapdn.replace('{username}', username), password, opts)
        if user: 
            logging.info('%s', user)
            cherrypy.session.acquire_lock()
            for k,v in utils.selectVars(user).items():
                cherrypy.session[k] = v
            cherrypy.session.release_lock()
        return user
    except:
        logging.error('%s', traceback.format_exc())
    return None

def parse(parser, port, ldapdn, dbname, ftpdir):
    parser.add_argument('--host', default = '0.0.0.0', help = 'http server host name or ip address (default: 0.0.0.0)')
    parser.add_argument('--port', default = port, type = int, help = 'http server port (default: %d)' % port)
    parser.add_argument('--nossl', default = False, action='store_true', help = 'disable ssl protection')
    parser.add_argument('--threads', default = 100, type = int, help = 'thread pool size (default: 100)')
    parser.add_argument('--cert', default = 'cacert.pem', help = 'path to ssl certificate file (default: cacert.pem)')
    parser.add_argument('--key', default = 'cert.pem', help = 'path to ssl private key file (default: cert.pem)')
    parser.add_argument('--loglvl', default = 'info', help = 'logging level (debug, info, warning, error or critical, default: info)')
    parser.add_argument('--ldapurl', default = 'ldaps://127.0.0.1', help = 'ldap server url (default: ldaps://127.0.0.1)')
    parser.add_argument('--ldapdn', default = ldapdn, help = 'ldap server distinguished name string format (default: %s)' % ldapdn)
    parser.add_argument('--ldaptls', default = False, action='store_true', help = 'enable ldap tls usage')
    parser.add_argument('--dbhost', default = '127.0.0.1', help = 'postgre database server host name or ip address (default: 127.0.0.1)')
    parser.add_argument('--dbname', default = dbname, help = 'postgre database name (default: %s)' % dbname)
    parser.add_argument('--dbuser', default = 'admin', help = 'postgre database user name (default: admin)')
    parser.add_argument('--dbpass', default = 'password', help = 'postgre database user password (default: password)')
    parser.add_argument('--ftphost', default = 'acuario.integra-s.com', help = 'eily.acuario ftp server host (default: acuario.integra-s.com)')
    parser.add_argument('--ftpport', default = 5433, type = int, help = 'eily.acuario ftp server port (default: 5433)')
    parser.add_argument('--ftpuser', default = 'ftpadm', help = 'eily.acuario ftp server user name (default: ftpadm)')
    parser.add_argument('--ftppass', default = 'joder', help = 'eily.acuario ftp server user password (default: joder)')
    parser.add_argument('--ftpdir', default = ftpdir, help = 'eily.acuario ftp server directory (default: %s)' % ftpdir)
    parser.add_argument('--plugin', default = '', action = utils.MultiArg, help = "source plugin startup info (example: {'module': 'plugin_nmea_http', 'params': {}})")
    parser.add_argument('--lock', default = None, help = 'sets lock file name (default: not set)')
    eaglob.cfg = parser.parse_args()

def prepare(logsettings, callbacks, ldapopts):
    logging.config.dictConfig(logsettings)
        
    loglvl = utils.parseLogLevel(eaglob.cfg.loglvl)

    logging.info('%s', eaglob.cfg)

    if eaglob.cfg.lock:
        lock = open(eaglob.cfg.lock, 'w')
        lock.write(str(os.getpid()))
        lock.flush()

    from ws4py import configure_logger
    configure_logger(loglvl)

    eaglob.cfg.db = "host='%s' dbname='%s' user='%s' password='%s'" % (eaglob.cfg.dbhost, eaglob.cfg.dbname, eaglob.cfg.dbuser, eaglob.cfg.dbpass)
    pgdb.start(eaglob.cfg.db, len(eaglob.cfg.plugin) + eaglob.cfg.threads)
        
    cherrypy.config.update({'server.socket_host': eaglob.cfg.host, 'server.socket_port': eaglob.cfg.port, 'server.thread_pool': eaglob.cfg.threads, 'tools.sessions.on': True, 'tools.sessions.locking':  'explicit'})
    cherrypy.config.update({'tools.encode.on':True, 'tools.encode.encoding':'utf8'})

    if not eaglob.cfg.nossl: 
        cherrypy.config.update({'server.ssl_module': 'pyopenssl', 'server.ssl_certificate': eaglob.cfg.cert, 'server.ssl_private_key': eaglob.cfg.key})

    cherrypy.config.update({'tools.auth_basic.on': True, 'tools.auth_basic.realm': 'eily.acuario.tracking', 'tools.auth_basic.checkpassword': lambda r,u,p : checkCredentials(r, u, p, ldapopts)})

    WebSocketPlugin(cherrypy.engine).subscribe()
    cherrypy.tools.websocket = WebSocketTool()

    for i in eaglob.cfg.plugin:
        thread.start_new_thread(startplugin, (i, logsettings, callbacks))
   
def start(server):
    cherrypy.quickstart(server, '', config={'/ws': {'tools.websocket.on': True, 'tools.websocket.handler_cls': WebSocketHandler}})

