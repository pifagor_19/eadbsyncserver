# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 27.03.2013
Updated: 06.06.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import logging, types, multiprocessing, thread, Queue, utils, traceback

def _func_dict(fs):
    return reduce(lambda r, f : dict(r.items() + {f.func_name: f}.items()), fs, {}) 

def _func_name(f):
    return f.func_name if type(f) is types.FunctionType else f

class _msg(object):
    def __init__(self, f, args, kwargs):
        self.f = f
        self.args = args
        self.kwargs = kwargs

class _queue(object):
    def __init__(self, q):
        self.q = q

    def __call__(self, f, *args, **kwargs):
        self.q.put(_msg(_func_name(f), args, kwargs))

class Process():
    def __init__(self, functions = [], callbacks = [], auto = True):
        self.qin = multiprocessing.Queue()
        self.qout = multiprocessing.Queue()
        self.process = multiprocessing.Process(target = self._loop, args=(_func_dict(functions), self.qout, self.qin, True))
        if auto:
            thread.start_new_thread(self._loop, (_func_dict(callbacks), self.qin, self.qout, False))
        self.process.start()
       
    def _call(self, m, fs, qout):
        f = fs[m.f]
        f.__dict__.update({'callback': _queue(qout)})
        if len(m.args) > 0 or len(m.kwargs) > 0: f(*m.args, **m.kwargs)
        else: f()

    def _loop(self, fs, qin, qout, suicide):
        if suicide:
            utils.exitAfterParent()
        while True:
            try:
                m = qin.get()
                if m: 
                    self._call(m, fs, qout)
                else: 
                    break
            except:
                logging.error('%s', traceback.format_exc())
        if suicide:
            utils.suicide()

    def loop(self, callbacks = [], block = False, timeout = 5, throw = False):
        try:
            callbacks = _func_dict(callbacks)
            m = self.qin.get(block = block, timeout = timeout)
            if m: 
                self._call(m, callbacks, self.qout)
        except Queue.Empty:
            pass
        except Exception, e:
            logging.error('%s', traceback.format_exc())
            if throw:
                raise e

    def __call__(self, f, *args, **kwargs):
        self.qout.put(_msg(_func_name(f), args, kwargs))

    @property
    def busy(self):
        return not self.qout.empty

    def join(self):
        self.process.join()

    def stop(self):
        self.qout.put(None)
        self.qin.put(None)

    def terminate(self):
        self.process.terminate()


