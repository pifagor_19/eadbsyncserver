# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 13.03.2013
Updated: 15.07.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import sys, os, signal, logging, types, json, thread, time, psutil, hashlib, argparse, pprint, datetime, psutil, inspect, decorator, traceback

def one(col, defval = None):
    try: return iter(col).next()
    except StopIteration: return defval

def md5(d):
    m = hashlib.md5()
    m.update(d)
    return m.digest()

def printjson(obj, func = logging.info):
    func('%s', json.dumps(obj, sort_keys = True, indent = 2))

class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date):
            return str(obj)
        return json.JSONEncoder.default(self, obj)

def tojson(obj):
    return json.dumps(obj, cls = JSONEncoder) if obj else None

def parseLogLevel(level): 
    return {'debug' : logging.DEBUG, 'info' : logging.INFO, 'warning' : logging.WARNING, 'error' : logging.ERROR, 'critical' : logging.CRITICAL}[level.lower()]            

def configureLogging(loglvl = logging.INFO, logfile = None):
    logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(module)s:%(funcName)s:%(lineno)s - %(message)s', level = loglvl, filename = logfile)

def _bool(v):
  return str(v).lower() in ("yes", "true", "t", "1")

def convert(value, func = None, defval = None):
    if func:
        try: return func(value)
        except: return defval
    elif None != defval:
        return convert(
            value,
            {
                types.BooleanType: _bool,
                types.IntType: int,
                types.LongType: long,
                types.FloatType: float,
                types.StringType: str,
                types.UnicodeType: unicode,
                types.TupleType: tuple,
                types.ListType: list,
                types.DictType: dict
            }[type(defval)]
            ,
            defval)
    return value

def loginToLDAP(url, userdn, password, opts, tls = False):
    try: 
        import ldap
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        con = ldap.initialize(url)
        con.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
        if tls: con.start_tls_s()
        con.bind_s(userdn, password)
        attrs = con.search_s(userdn, ldap.SCOPE_SUBTREE, '(cn=*)', [k for k,v in opts.items()])
        def val(k, v, a):
            r = a[0][1].get(k)
            return v[1](r[0]) if r else v[0]
        return type('User', (), dict([(k, val(k, v, attrs)) for k,v in opts.items()]))
    except ldap.INVALID_CREDENTIALS: logging.warning('invalid credentials')
    except ldap.LDAPError, e: logging.error(e.message['desc'] if type(e.message) == dict and e.message.has_key('desc') else e.message)
    except: logging.error('%s', traceback.format_exc())
    return None

def suicide():
    psutil.Process(os.getpid()).kill()

def exit(status = 0):
    os._exit(status)

def exitOnDefaultSignals():
    signal.signal(signal.SIGINT, lambda s, f : exit())
    signal.signal(signal.SIGTERM, lambda s, f : exit())

def exitAfterParent(sec = 0.1):
    def _wait(sec):
        try:
            p = psutil.Process(os.getpid())

            while p:
                pp = p.parent
                if not pp or pp.pid <= 1:
                    logging.warn('parent process exits')
                    break
                time.sleep(sec)

            childs = []

            try:
                childs = [ pc for pc in p.get_children(recursive = True) ]
            except:
                childs = [ pc for pc in p.get_children() ]
                
            childs = childs + [ p ]

            for i in range( 0, 50 ):
                if all( map( lambda pc: not pc.is_running(), childs ) ):
                    logging.info( 'all processes closed' )
                    break;
                time.sleep( 0.1 )
            
            for pc in childs:
                if pc.is_running():
                    pc.kill()
                    logging.info( '%s - killed', pc.name )
        except:
            logging.error('%s', traceback.format_exc())
        finally:
            p.kill()
    thread.start_new_thread(_wait, (sec,))

class MultiArg(argparse.Action):
    def __call__(self, parser, namespace, value, option_strings = None):
        dest = getattr(namespace, self.dest, None) 
        if(dest == self.default):
            dest = []
            setattr(namespace, self.dest, dest)
            parser.set_defaults(**{self.dest: None}) 
        dest += [value]

def buildTreeNode(items, contains):
    cur = [i for i in items if not [j for j in items if i != j and contains(j, i)]]
    node = dict([(i, {}) for i in cur])
    items = set(items) - set(cur)
    if items:
        sub = buildTreeNode(items, contains)
        for i in node.keys():
            node[i] = dict([(k,v) for k,v in sub.items() if contains(i, k)])
    return node

def extendTreeNode(node, items, contains, getkey):
    res = set()
    for k,v in node.items():
        if v not in items:
            res |= extendTreeNode(v, items, contains, getkey)
            for i in set(items) - res:
                if contains(k, i):
                    v[getkey(i)] = i
                    res |= set([i])
    return res

def selectVars(obj, expand = (), selector = lambda k : not k.startswith('_')):
    return dict((k,(selectVars(v) if k in expand and v else v)) for k,v in vars(obj).items() if selector(k))

def prettyStr(obj, expand = ()):
    return pprint.pformat(selectVars(obj, expand))

def importModule(m):
    return sys.modules[m] if m in sys.modules else __import__(m)

def callModuleFunc(m, f, *args, **kwargs):
    m = importModule(m)
    f = m.__dict__[f]
    return f(*args, **kwargs)

def getkwargs(f):
    spec = inspect.getargspec(f)
    la = len(spec.args)
    ld = len(spec.defaults) if spec.defaults else 0
    dl = la - ld
    return dict((a, spec.defaults[i - dl]) for i,a in zip(range(dl, la), spec.args))

@decorator.decorator
def convertkwargs(f, *args):
    spec = inspect.getargspec(f)
    if spec.defaults:
        la = len(args)
        ld = len(spec.defaults)
        dl = la - ld
        args = [(convert(a, defval = spec.defaults[i - dl]) if i >= dl else a) for i,a in zip(range(0, la), args)]
    return f(*args)

class maybe(object):
    def __init__(self, value = None):
        self.value = value
    def __call__(self, param = None):
        return maybe(param(self.value) if self.value else None) if type(param) is types.FunctionType else (self.value if self.value else param)

def first(col, pred, defval = None):
    for i in col:
        if pred(i):
            return i
    return defval
