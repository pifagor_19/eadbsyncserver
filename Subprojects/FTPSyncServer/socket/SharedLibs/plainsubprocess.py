# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 07.06.2013
Updated: 07.06.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import os, logging, logging, logging.config, logging.handlers, subprocess, time, thread, subprocessing, traceback, utils, psutil

_exe = None
_process = None

def configureLogging(config):
    logging.config.dictConfig(config)

def _thread():
    global _exe, _process
    while _exe:
        try:
            logging.info('starting %s', _exe)
            _process = subprocess.Popen(_exe, cwd = os.path.dirname(_exe[0]))
            logging.info('started %s', utils.prettyStr(_process))
            _process.wait()
        except: 
            logging.error('%s', traceback.format_exc())
            time.sleep(2)

def start(exe):
    global _exe
    if not _exe:
        _exe = exe
        logging.info('start %s', _exe)
        thread.start_new_thread(_thread, ())

def stop():
    global _exe, _process
    try:
        if _exe:
            logging.info('stop %s', _exe)
            _exe = None
            if _process:
                logging.info('stopping %s', utils.prettyStr(_process))
                #_process.Kill() - ни работаит, йа в панеке
                psutil.Process(_process.pid).kill()
    except:
        pass

class Process(object):
    def __init__(self, exe, logcfg = None):
        self.process = subprocessing.Process(functions = [configureLogging, start, stop], auto = False)
        if logcfg:
            self.process(configureLogging, logcfg)
        self.process(start, exe)

    def stop(self):
        self.process(stop)
        self.process.stop()

    def join(self):
        self.process.join()
