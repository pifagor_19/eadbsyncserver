# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 11.06.2013
Updated: 14.06.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import traceback, ftplib, urllib2, types, multiprocessing

total = 0
downloaded = 0

def write(file, buf, progress):
    global total, downloaded
    downloaded += len(buf)
    file.write(buf)
    progress.value = downloaded * 100 / total

def download(url, path, progress):
    global total
    try:
        u = urllib2.urlparse.urlparse(url)
        ftp = ftplib.FTP()
        ftp.connect(u.hostname, u.port)
        ftp.login(u.username, u.password)
        ftp.sendcmd('TYPE i')
        total = ftp.size(u.path)
        file = open(path, 'wb')
        ftp.retrbinary("RETR " + u.path, lambda buf: write(file, buf, progress), 8 * 1024) 
        file.close()
        ftp.quit()
    except:
        progress.value = -1
        print traceback.format_exc()

class Downloader(object):
    def __init__(self, url, path):
        self.progress = multiprocessing.Value('i', 0)
        multiprocessing.Process(target = download, args=(url, path, self.progress)).start()



