# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 15.04.2013
Updated: 12.06.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import os, logging, logging, logging.config, logging.handlers, subprocess, threading, time, subprocessing
from suds import *
from suds.client import Client

_exe = None
_process = None
_url = None
_svcurl = None
_lock = threading.Lock()
_client = None
_callback = None

def _stop():
    global _process, _client, _callback
    try:
        if _process:
            _process.kill()
            _process = None
            logging.info('stopped')
    except Exception, e: 
        logging.error('%s', e)
    finally:
        if _client:
            logging.info('disconnected')
            _callback('setState', False)
        _client = None
        _process = None

def _connect():
    global _exe, _process, _url, _svcurl, _lock, _client
    if _url:
        _lock.acquire()
        try:
            if _exe and not _process:
                logging.info('starting %s', _exe)
                startupinfo = subprocess.STARTUPINFO()
                startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
                _process = subprocess.Popen(_exe, cwd = os.path.dirname(_exe[0]), startupinfo = startupinfo)
                logging.info('started %s', _exe)
            if not _client:
                attempt = 0
                while attempt <= 5:
                    try:
                        exception = None
                        attempt += 1
                        _client = Client(_url)
                        _client.set_options(location = _svcurl)
                        logging.info('connected to %s', _url)
                        _callback('setState', True)
                        break
                    except Exception, e:
                        exception = e
                        time.sleep(1)
                if exception:
                    raise exception
            return True
        except Exception, e: 
            logging.error('%s', e)
            time.sleep(2)
        finally: 
            _lock.release()
        _stop()
        return False

def configureLogging(config):
    logging.config.dictConfig(config)

def connect(exe, url, svcurl):
    global _exe, _url, _svcurl, _callback
    logging.info('connect to %s %s (%s)', url, svcurl, exe)
    _exe = exe
    _url = url
    _svcurl = svcurl
    _callback = connect.callback

def _execute(method, params, ticket = None):
    global _client, _callback
    logging.info('%s(%s)', method, params)
    
    if(_connect()):
        try:
            logging.debug( 'method called' )
            result = _client.service.CallMethod(method, params, 5000)
            logging.debug( 'method received' )
            _callback('receive', method, params, result, ticket )
            return True
        except Exception, e:
            logging.error('%s', e)
            _stop()
    return False

def execute(method, params, ticket = None ):
    return _execute(method, params, ticket ) or _execute(method, params, ticket )

def disconnect():
    global _exe, _url, _svcurl
    logging.info('dicsonnect from %s %s (%s)', _url, _svcurl, _exe)
    _url = _svcurl = _exe = None
    _stop()

class Process(object):
    def __init__(self, url, svcurl, exe = None, recv = None, state = None, extfuncs = [], logcfg = None, auto = True):
        self.recv = recv
        self.state = state
        self.process = subprocessing.Process(functions = [configureLogging, connect, execute, disconnect] + extfuncs, callbacks = [self.setState, self.receive], auto = auto)
        self.process(configureLogging, logcfg)
        self.process(connect, exe, url, svcurl)

    def setState(self, connected):
        if self.state:
            self.state(connected)

    def receive(self, method, params, result, ticket ):
        if self.recv:
            self.recv(method, params, result, ticket )

    def execute(self, method, params = '', ticket = None):
        self.process(execute, method, params, ticket )

    def loop(self):
        self.process.loop(callbacks = [self.setState, self.receive])

    def stop(self):
        self.process(disconnect)
        self.process.stop()

    def join(self):
        self.process.join()
