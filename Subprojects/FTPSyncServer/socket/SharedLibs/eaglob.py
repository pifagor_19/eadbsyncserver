# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 10.04.2013
Updated: 15.07.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import pgdb, cherrypy, types, decorator, utils, functools

cfg = None
tdef = None
tag = None

def accesslevel(key, lvl):
    @decorator.decorator
    def wrapfunc(f, *args):
        if hasattr(cherrypy.serving, 'session') and cherrypy.session[key] < lvl:
            raise Exception('access denied')
        return f(*args)
    return wrapfunc

def _execute(req, func):
    if type(req) is types.TupleType:
        return func(req[0], *req[1:])
    elif type(req) is types.DictType:
        return func(req['sql'], **req)
    elif type(req) in types.StringTypes:
        return func(req)
    raise Exception('invalid sql operation %s', req)

def checkargs(*args):
    for a in args:
        if a and type(a) in types.StringTypes:
            s = unicode(a)
            if u"'" in s or u'"' in s:
                raise Exception('sql arguments can not contain quotes')

def query(timeout = 60, limit = 1000):
    @decorator.decorator
    def wrapfunc(f, *args):
        return _execute(f(*args), functools.partial(pgdb.query, timeout, limit))
    return wrapfunc

def update(timeout = 60):
    @decorator.decorator
    def wrapfunc(f, *args):
        checkargs(*args)
        return _execute(f(*args), functools.partial(pgdb.update, timeout))
    return wrapfunc

def execute(timeout = 60):
    @decorator.decorator
    def wrapfunc(f, *args):
        checkargs(*args)
        return _execute(f(*args), functools.partial(pgdb.execute, timeout))
    return wrapfunc

def callModuleFunc(module, func, **kwargs):
    if not module.startswith('sql_'):
        raise Exception('module not allowed')
    return utils.callModuleFunc(module, func, **kwargs)
