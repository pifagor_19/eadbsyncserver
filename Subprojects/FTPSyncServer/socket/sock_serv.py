import SocketServer, TreeModel, logging, traceback, settings

class MyTCPHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        self.data = self.request.recv(1024).strip()
        if self.data == 'build':
            try:
                logging.info("Start building hierarchy")
                TreeModel.serve()
            except:
                logging.error(traceback.format_exc())
                self.request.sendall("FAIL")
        self.request.sendall("OK")

if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", 9999
    logging.info("Server started")
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
    server.serve_forever()
    logging.info("Server ended")
    server.server_close()