#!/usr/bin/env python
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 23.04.2014
Updated: 26.04.2013
Author: Alex Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

import os, hashlib, json, logging, settings, zipfile, shutil

PATH = ""
d = {PATH: None}

def gen_md5(f, block_size = 2048):
    _f = open(f)
    md5 = hashlib.md5()
    while True:
        data = _f.read(block_size)
        if not data: break
        md5.update(data)
    return md5.hexdigest()

class Folder(object):
    def __init__(self, name):
        self._name = name
        self.files = {}
        self.folders = {}
        
def gen_tree(folder_path):
    global PATH, d
    PATH = folder_path
    root = Folder(PATH)
    d = {PATH: root}
    for path, folders, files in os.walk(PATH):
        cur = d[path]
        for file in files:
            _name, _ext = os.path.splitext(file)
            if _name not in ("info", ".ftpsyncdir"):
                cur.files[file] = gen_md5(os.path.join(path, file))
        for folder in folders:
            f = Folder(folder)
            cur.folders[folder] = f
            d[os.path.join(path, folder)] = f

def iter(el, path_, dict_):
    for k,v in el.files.iteritems():
        _k = find_key(el)
        _abs_path = os.path.join(_k, k)
        _rel_path = os.path.relpath(_abs_path, path_)
        _rel_path = _rel_path.replace("\\", "/")
        dict_[_rel_path] = v
    for item in el.folders:
        iter(el.folders[item], path_, dict_)

def find_key(val):
    for k,v in d.iteritems():
        if v == val: return k

def dump_info(_path):
    src = dict()
    iter(d[_path], _path, src)
    for item in ("info", "info.sync", str()):
        if os.path.isfile(item):
            os.remove(item)
    f = open("info", "a")
    f.write(json.dumps(src))
    f.close()
    with zipfile.ZipFile("info.sync", "w", zipfile.ZIP_DEFLATED) as _zip:
        _zip.write("info")
    os.remove(os.path.join(os.path.dirname(os.path.realpath(__file__)), "info"))
    shutil.move("info.sync", _path + "/" + "info.sync")

def build_ftp():
    global PATH, d
    dump_info(PATH)
    for path, folders, files in os.walk(PATH):
        for folder in folders:
            dump_info(os.path.join(path, folder))

def serve():
    logging.info("Generation start")
    dirs = ["/ftp/eily.acuario/eaintegration",
            "/ftp/eily.acuario/typedef",
            "/ftp/eily.acuario/tracking"]
    for _f in dirs:
        gen_tree(_f)
        build_ftp()
    logging.info("Generation end")

if __name__ == "__main__":
    serve()