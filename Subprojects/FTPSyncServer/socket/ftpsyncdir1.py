#!/usr/bin/env python
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 23.04.2014
Updated: 26.04.2013
Author: Alex Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

__author__ = 'PRONIN'

import logging, traceback, multiprocessing, json
import zipfile, settings, os, shutil, time, thread
from ftplib import FTP

MAX_RETRIES = 7
TIMEOUT_INTERVAL = 20
SLEEP_TIMEOUT = 5
ZIP_ARCH = "info.sync"
SRV_FILES_INFO = "info"
CLT_FILES_INFO = "info.clt"

# -------------------------------------------------------------------
_distinct = lambda src,acc : filter( lambda x: x not in acc,src )
_same = lambda src,acc : filter( lambda x: x in acc,src )
# -------------------------------------------------------------------

def get_fsize(start_path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size

def exc_handler(func):
    def inner(*args, **kwargs):
        result = None
        try:
            result = func(*args, **kwargs)
        except:
            logging.error(traceback.format_exc())
        return result if result != None else -1
    return inner

class FTPManager(object):
    def __init__(self, host, port, login, passw, ftp_dir, loc_dir):
        self._ftp = FTP()
        self._host = host
        self._port = port
        self._login = login
        self._passw = passw
        self._src = ftp_dir
        self._dst = loc_dir
        self.local_files = dict()
        self._connect()


    @exc_handler
    def _connect(self):
        if self._ftp:
            self._ftp.close()
        logging.info(self._ftp.connect(self._host, self._port, TIMEOUT_INTERVAL))
        logging.info(self._ftp.login(self._login, self._passw))
        logging.info(self._ftp.voidcmd("TYPE i"))

    @exc_handler
    def manage_downloads(self):
        _ret_code = 0
        try:
            try: os.stat(self._dst)
            except: os.makedirs(self._dst)
            self._ftp.cwd(self._src)
            self._ftp.retrbinary("RETR " + ZIP_ARCH, open(self._dst + "/" + ZIP_ARCH, "wb").write)
            zf = zipfile.ZipFile(self._dst + "/" + ZIP_ARCH)
            for name in zf.namelist():
                zf.extract(name, self._dst)
            zf.close()
            os.remove(self._dst + "/" + ZIP_ARCH)
            self.ftp_files = json.load(open(self._dst + "/" + SRV_FILES_INFO))
            if os.path.exists(self._dst + "/" + CLT_FILES_INFO):
                try:
                    self.local_files = json.load(open(self._dst + "/" + CLT_FILES_INFO, "rb"))
                except:
                    logging.error(traceback.format_exc())
            if not len(self.local_files):
                logging.info("CREATE ARCH")
                for _fpath, _hash in self.ftp_files.iteritems():
                    logging.info(_fpath)
                    _ret_code += 1
                    self._get_file(_fpath)
            else:
                logging.info("UPDATE ARCH")
                self._check_sync()
                for _f in _distinct(self.local_files, self.ftp_files):
                    try:
                        os.remove(self._dst + "/" + _f)
                        _ret_code += 1
                    except: pass
                for _f in _distinct(self.ftp_files, self.local_files):
                    self._get_file(_f)
                    _ret_code += 1
                for _f in _same(self.ftp_files, self.local_files):
                    if self.ftp_files[_f] != self.local_files[_f]:
                        self._get_file(_f)
                        _ret_code += 1
            shutil.move(self._dst + "/" + SRV_FILES_INFO, self._dst + "/" + CLT_FILES_INFO)
            self._cleanup()
            return _ret_code
        except:
            logging.error(traceback.format_exc())
            return -1

    def _cleanup(self):
        _del_folders = list()
        for _path, _files, _folders in os.walk(self._dst):
            _mpath = _path.replace("\\", "/")
            if not get_fsize(_mpath):
                _del_folders.append(_mpath)
        for i in _del_folders:
            try:
                shutil.rmtree(i)
            except:pass

    def _check_sync(self):
        import copy
        _td = dict()
        for _fpath, _hash in self.local_files.iteritems():
            if os.path.isfile(self._dst + "/" + _fpath):
                _td[_fpath] = _hash
        self.local_files = copy.deepcopy(_td)

    def _get_file(self, file, retr = 0):
        try:
            try: self._ftp.voidcmd("NOOP")
            except: self._connect()
            full_path = self._dst + "/" + file
            folder = os.path.dirname(full_path)
            try:
                os.makedirs(folder)
            except: pass
            fstream = open(self._dst + "/" + file, "wb")
            self._ftp.retrbinary("RETR " + str(self._src + "/" + file), fstream.write, 8 * 1024)
            logging.info(file)
            return 1
        except:
            logging.error("Unable to download file. Retry...")
            if retr >= MAX_RETRIES:
                logging.error("All attempts exceeded. Quit.")
                return -1
            time.sleep(SLEEP_TIMEOUT)
            self._get_file(file, retr + 1)

def synchronize(host, port, login, passw, ftp_dir, loc_dir, codec = 'windows_1251', deleteAbsents = True):
    try:
        _f = FTPManager(host, port, login, passw, ftp_dir, loc_dir)
        _ret_code = _f.manage_downloads()
        logging.info("Ret code is: %s" % _ret_code)
        return _ret_code
    except:
        logging.error(traceback.format_exc())
        return -1

def _sync(r, *args):
    r.value = synchronize(*args)

def async(url, port, user, passw, fdir, ldir, callback, codec = 'windows_1251', deleteAbsents = True):
    r = multiprocessing.Value('i', -1)
    p = multiprocessing.Process(target = _sync, args=(r, url, port, user, passw, fdir, ldir, codec))
    p.start()
    def waitexit( p, r, callback):
        p.join()
        if callable(callback):
            callback(r.value)
    thread.start_new_thread( waitexit, ( p, r, callback))

def nothing(ex):
    if ex<0: print('ERROR')
    else: print(' SYNC CALLBACK OK')

def clbk(value):
    print "Hello from clbk with val: ", value

if __name__ == '__main__':
    logging.info("Start sync")
    WEB_VER = "1.0.1"
    #WEB_VER = "upd_test"
    #synchronize("192.168.10.25", 5433, "ftpsync", "ftpsync", "/eaintegration/media", "D:/ftp/media")
    synchronize("192.168.10.25", 5433, "ftpsync", "ftpsync", "/eaintegration/web" + "/" + WEB_VER, "D:/ftp/www")
    # async("192.168.10.25", 5433, "ftpsync", "ftpsync", "/tracking", "D:/ftp/eily.acuario/tracking", callback=clbk)
    # async("192.168.10.25", 5433, "ftpsync", "ftpsync", "/eaintegration", "D:/ftp/eily.acuario/eaintegration", callback=clbk)
    logging.info("End sync")