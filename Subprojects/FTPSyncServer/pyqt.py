__author__ = 'PRONIN'
import sys
import os
import os.path

from PyQt4.QtCore import *
from PyQt4.QtGui import *

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        template_menu = self.menuBar().addMenu('&Templates')
        menus = {'templates': template_menu}

        for dirpath, dirnames, filenames in os.walk('templates'):
            current = menus[dirpath]
            for dn in dirnames:
                menus[os.path.join(dirpath, dn)] = current.addMenu(dn)
            for fn in filenames:
                current.addAction(fn)
        a = 0

if __name__=='__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
