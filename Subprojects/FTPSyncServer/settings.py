'''
This source file is part of eadbsync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''
import logging, ctypes, os

from logutils.colorize import ColorizingStreamHandler

class RainbowLoggingHandler(ColorizingStreamHandler):
    level_map = {
        logging.DEBUG: ('black', 'cyan', True),
        logging.INFO: ('black', 'green', True),
        logging.WARNING: ('black', 'yellow', True),
        logging.ERROR: ('white', 'red', False),
        logging.CRITICAL: ('red', 'white', True),
    }
    date_format = "%H:%m:%S"
    who_padding = 22
    show_name = True

    def get_color(self, fg=None, bg=None, bold=False):
        params = []
        if bg in self.color_map:
            params.append(str(self.color_map[bg] + 40))
        if fg in self.color_map:
            params.append(str(self.color_map[fg] + 30))
        if bold:
            params.append('1')
        color_code = ''.join((self.csi, ';'.join(params), 'm'))
        return color_code

    def colorize(self, record):
        if record.levelno in self.level_map:
            fg, bg, bold = self.level_map[record.levelno]
        else:
            bg = None
            fg = "white"
            bold = False
        template = [
            "[",
            self.get_color("black", None, True),
            "%(asctime)s",
            self.reset,
            "] ",
            self.get_color("white", None, True) if self.show_name else "",
            "%(levelname)s " if self.show_name else "",
            "%(padded_who)s",
            self.reset,
            " ",
            self.get_color(bg, fg, bold),
            "%(message)s",
            self.reset,
        ]
        format = "".join(template)
        who = [self.get_color("yellow", None, True),
               getattr(record, "module", ""),
               "::",
               self.get_color("green"),
               getattr(record, "funcName", ""),
               "()",
               self.get_color("black", None, True),
               self.get_color("cyan"),
               ":{",
               str(getattr(record, "lineno", 0)),
               "}"]
        who = "".join(who)
        unformatted_who = getattr(record, "module", "") + getattr(record, "funcName", "") + "()" + \
            ":" + str(getattr(record, "lineno", 0))
        if len(unformatted_who) < self.who_padding:
            spaces = " " * (self.who_padding - len(unformatted_who))
        else:
            spaces = ""
        record.padded_who = who + spaces
        formatter = logging.Formatter(format, self.date_format)
        self.colorize_traceback(formatter, record)
        output = formatter.format(record)
        record.ext_text = None
        return output

    def colorize_traceback(self, formatter, record):
        if record.exc_info:
            record.exc_text = "".join([
                self.get_color("red"),
                formatter.formatException(record.exc_info),
                self.reset,
            ])

    def format(self, record):
        if self.is_tty:
            message = self.colorize(record)
        else:
            message = logging.StreamHandler.format(self, record)
        return message

logging.basicConfig(
        format          =       '%(asctime)s {%(levelname)s} %(module)s::%(funcName)s (%(lineno)d) - "%(message)s"',
        filename        =       './/logs//database_sync.log',
        level           =       logging.DEBUG
)

try:
    import sys
    console = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s {%(levelname)s} %(module)s::%(funcName)s (%(lineno)d) - "%(message)s"')
    console.setFormatter(formatter)
    #logging.getLogger('').addHandler(console)
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    root.addHandler(RainbowLoggingHandler(sys.stdout))
except:
    logging.error("Unable to register console handler - perhaps, server running in service mode?")

# ------------------------------------------------------------------------------------------------------------------------ #

SERVER_VERSION          =   '0.6.2'                         # server version
CONFIG_FILE_NAME        =   'tasks_params.json'             # config file name
VALIDATOR_SCHEMA_NAME   =   'task_validator_schema.json'    # validation schema name
SERVER_THREADS_COUNT    =   40                              # cherrypy threads to process user requests
FORCE_UPDATE_FTP        =   False                           # option to forcely update entire ftp
UPDATE_SPEED_INTERVAL   =   5 #40                           # schedule timer update interval
TIMEOUT_INTERVAL        =   10                              # timeout for ftp and db connections
MAX_FTP_CONN_ATTEMPTS   =   7                               # max attempts that ftp module tries to start/continue downloading file
WEBSERVICE_HOST         =   '0.0.0.0'                       # web service network interface
WEBSERVICE_PORT         =   8973                            # web service port

# ------------------------------------------------------------------------------------------------------------------------ #

sample_config_format = {
    "credentials": {
        "server_password": "5f4dcc3b5aa765d61d8327deb882cf99",
        "server_login": "admin"
    },
    "tasks": [],
    "local_settings": {
        "database_password": "",
        "database_name": "",
        "database_host": "",
        "database_port": "",
        "database_login": ""
    },
    "server_version": str(SERVER_VERSION),
    "settings": {
        "ftp_homedir": "/ftp"
    }
}
