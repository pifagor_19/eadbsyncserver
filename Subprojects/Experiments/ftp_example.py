import time, ftplib, os
from ftplib import FTP

host = '192.168.10.233'
port = 5433
user = 'ftpadm'
passw = 'ftpadm'
timeout = 60

from_path = '/eily.acuario/eaintegration/locale/ru/LC_MESSAGES/'
#from_path = '/media/buildings/05328627-3935-4483-8ee6-7ceebea33c4f/'
to_path = 'C:\eadbsyncsrv\experiments'
#file_name = 'b23a9d8a-f064-48a3-8ed5-8d4a3791911a'
file_name = 'eaintegration.po'

#_fh = open(to_path + '\\' + file_name, 'wb')
#c = FTP()
#print c.connect(host, port, timeout)
#print c.login(user, passw)
#print c.voidcmd('TYPE i')
#print c.dir()
#print c.cwd(from_path)
#file_size = c.size(file_name)
#def handle_download(block_data, file):
#	percentage = (float(file.tell()) / float(file_size))*100
#	print 'Downloaded %.2f %%' % percentage
#	file.write(block_data)
#print c.retrbinary('RETR ' + file_name, lambda block : handle_download(block, _fh))
#print c.close()

class FTPManager():
	def __init__(self, h, p, l, ps, m):
		self._ftp = FTP()
		self._h = h
		self._p = p
		self._l = l
		self._ps = ps
		self._max = m
	
	def conn(self):
		print "An attempt to connect"
		print self._ftp.connect(self._h, self._p, 10)
		self._ftp.login(self._l, self._ps)
	
	def handle_download(self, block_data, file):
		percentage = (float(file.tell()) / float(self._size))*100
		print 'Downloaded %.2f %%' % percentage
		file.write(block_data)
	
	def FtpRmTree(self, path):
		"""Recursively delete a directory tree on a remote server."""
		wd = self._ftp.pwd()
		 
		try:
			names = self._ftp.nlst(path)
		except ftplib.all_errors as e:
			# some FTP servers complain when you try and list non-existent paths
			print('FtpRmTree: Could not remove {0}: {1}'.format(path, e))
			return
		 
		for name in names:
			if os.path.split(name)[1] in ('.', '..'): continue
			 
			print('FtpRmTree: Checking {0}'.format(name))
		 
			try:
				self._ftp.cwd(name) # if we can cwd to it, it's a folder
				self._ftp.cwd(wd) # don't try a nuke a folder we're in
				FtpRmTree(name)
			except ftplib.all_errors:
				self._ftp.delete(name)
		 
		try:
			self._ftp.rmd(path)
		except ftplib.all_errors as e:
			print('FtpRmTree: Could not remove {0}: {1}'.format(path, e))
	
	def get(self, src, dst, file, c = 0):
		print 'Download file. Attempt #: ', c
		if c == self._max: 
			print "Max attempts occured. Exit"
			return
		try:
			print "An attempt to connect"
			self._ftp.connect(self._h, self._p, 60)
			self._ftp.login(self._l, self._ps)
			self._ftp.set_pasv(1)
			self._ftp.voidcmd('TYPE i')
			self.FtpRmTree(src)
			#print self._ftp.nlst()
			#print self._ftp.sendcmd("mdelete")
			#self._ftp.retrlines('LIST')
			#self._ftp.voidcmd('TYPE i')
			#self._ftp.sendcmd()
			#self._ftp.voidcmd('TYPE i')
			#self._ftp.cwd(src)
			#self._fh = open(to_path + '\\' + file_name, 'wb')
			#self._size = self._ftp.size(file_name)			
			#self._ftp.retrbinary('RETR ' + file, lambda block : self.handle_download(block, self._fh))
		except Exception as e:
			print "Exception: ", e
			time.sleep(5)
			self._ftp.close()
			self.get(src, dst, file, c + 1)
		
if __name__ == '__main__':
	f = FTPManager(host, port, user, passw, 20)
	f.conn()
	f.get(from_path, to_path, file_name)