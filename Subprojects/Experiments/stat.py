import psycopg2, time, os, datetime, logging

logging.basicConfig(
        format          =       '%(asctime)s "%(message)s"',
        filename        =       './/logs//database_sync.log',
        level           =       logging.DEBUG
)

clear = lambda: os.system('cls')

dbname = 'eatracking2'
host = '192.168.10.25'
port = 5432

query_string = '''SELECT table_name, pg_size_pretty(table_size) AS table_size,
    pg_size_pretty(indexes_size) AS indexes_size,
    pg_size_pretty(total_size) AS total_size
    FROM (SELECT table_name, pg_table_size(table_name) AS table_size,
    pg_indexes_size(table_name) AS indexes_size,
    pg_total_relation_size(table_name) AS total_size
    FROM (SELECT ('"' || table_schema || '"."' || table_name || '"') AS table_name
    FROM information_schema.tables WHERE table_schema = 'public'
    ) AS all_tables ORDER BY total_size DESC) AS pretty_sizes ORDER BY table_name ASC;'''
query_string2 = "select pg_size_pretty(pg_database_size('%s'));" % dbname
query_string3 = "select count(1) from archive_params"
c_str = "host=%s port=%d dbname=%s user=postgres password=joder2joder" % (host, port, dbname)

connector = psycopg2.connect(c_str)
cursor = connector.cursor()
while True:
    clear()
    today = datetime.datetime.now()
    logging.info('Now is: {0:^80}'.format(str(today)))
    print('Now is:                 {0:^80}'.format(str(today)))
    cursor.execute(str(query_string))
    retVals = cursor.fetchall()
    logging.info("--------------------------------------------------------------------------------")
    logging.info('{0:^40} {1:^10} {2:^10} {3:^10}'.format("Table name", "Table size", "Index size", "Total size"))
    logging.info("--------------------------------------------------------------------------------")
    print("--------------------------------------------------------------------------------")
    print('{0:^40} {1:^10} {2:^10} {3:^10}'.format("Table name", "Table size", "Index size", "Total size"))
    print("--------------------------------------------------------------------------------")
    for record in retVals:
        logging.info('|{0:<40} |{1:<10} |{2:<10} |{3:<10}'.format(record[0], record[1], record[2], record[3]))
        print('| {0:<35} | {1:<10} | {2:<10} | {3:<10}'.format(record[0], record[1], record[2], record[3]))
    logging.info("--------------------------------------------------------------------------------")
    print("--------------------------------------------------------------------------------")
    cursor.execute(str(query_string2))
    retVals = cursor.fetchall()
    logging.info("Overall database size: %s" % retVals[0])
    print("Overall database size: %s" % retVals[0])
    cursor.execute(str(query_string3))
    retVals = cursor.fetchall()
    logging.info("Overall rows count in archive_params: %s" % retVals[0])
    print("Overall rows count in archive_params: %s" % retVals[0])
    time.sleep(1)

    retVals = cursor.fetchall()