import dns.query
import dns.zone

z = dns.zone.from_xfr(dns.query.xfr('192.168.10.25', '192.168.10.2'))
names = z.nodes.keys()
names.sort()
for n in names:
    print z[n].to_text(n)