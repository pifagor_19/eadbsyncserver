# -*- coding: utf-8 -*-
'''
This source file is part of eily.acuario

For the latest info, see http://integra-s.com/

Created: 02.04.2013
Updated: 02.04.2013
Author: Undwad(Selchenkov) mailto:undwad@integra-s.com mailto:undwad@mail.ru

Copyright (c) 2013 Integra-S JSC
'''

import os, ftplib, logging, pprint, multiprocessing, thread, shutil, traceback

count = 0
def sync(url, port, user, passw, fdir, ldir, codec = 'windows_1251', logfmt = '%(levelname)s - %(message)s', loglvl = logging.INFO, logfile = None):
    try:
        logging.basicConfig(format = logfmt, level = loglvl, filename = logfile)
        hfile = '.ftpsyncdir'
        ftp = ftplib.FTP()
        logging.info('%s', ftp.connect(url, port))
        logging.info('%s', ftp.login(user, passw))
        
        try:
           os.stat(ldir)
        except:
           os.mkdir(ldir)
        
        def sync_all(fdir,ldir ):
            global count
            dictr = {}
            dictfilesch = {}
            
            print fdir
            allitems = [os.path.basename(d) for d in ftp.nlst(fdir)]
            lallitems= os.listdir(ldir)
            for delitem in lallitems:
                if not delitem in allitems and delitem!=hfile:
                    count += 1
                    logging.info('REMOVE %s', ldir+'/'+delitem)
                    if os.path.isfile(ldir+'/'+delitem):
                        try:
                            os.remove(ldir+'/'+delitem)
                        except Exception, e:
                            logging.warn('%s', e)
                    else:
                        shutil.rmtree(ldir+'/'+delitem)
                        
            print('\nCurrent directory FTP: %s' %fdir)
            print('List file on the FTP:\n %s' %allitems)
            files = []
            dirs = []
            for item in allitems:
                try:
                    #logging.info("ITEM IS: %s" % (fdir+'/'+item))
                    #logging.info(ftp.sendcmd('MDTM %s' % fdir+'/'+item))
                    #ftp.sendcmd('MDTM %s' % fdir+'/'+item)
                    ftp.cwd(fdir+'/'+item)
                    logging.debug('%s\n%s', ldir, pprint.pformat(item))
                    #files.append(item)
                    dirs.append(item)
                except:
                    #dirs.append(item)
                    files.append(item)
            
                    
            if os.path.exists(ldir+'/'+hfile):
                try:
                    dictr = eval(open(ldir+'/'+hfile,'rb').read())
                except Exception, e:
                    logging.warning('%s', e)

            for file in files:
                filetime = ftp.sendcmd('MDTM %s' % fdir+'/'+file)
                dictfilesch[file] = filetime
                try:
                    timedif = filetime != dictr[file]
                except:
                    timedif = True
                if not os.path.exists(ldir+'/'+file) or timedif:
                    try:
                        logging.info('Transfering %s', fdir + '/' +file)
                        lf = open(unicode((ldir +'/'+ file).decode('utf-8')), 'wb')
                        logging.info('%s\n', ftp.retrbinary("RETR " + (fdir + '/' +file), lf.write, 8 * 1024))
                        count += 1
                        lf.close()
                    except Exception, e:
                        print('transfer file failed')
                        dictfilesch[file] = ''
                        logging.error('%s', e)
                open(ldir+'/'+hfile, 'wb').write(pprint.pformat(dictfilesch))
                        
            for dir in dirs:
                if not os.path.exists(ldir+'/'+dir):
                    os.mkdir(ldir+'/'+dir )
                nfdir = fdir + '/' + dir
                nldir = ldir + '/' + dir
                sync_all(nfdir,nldir)
        sync_all(fdir,ldir)
        logging.info('%s', ftp.quit())
        
#ftpsyncdir.async( '192.168.10.83', '5433', 'ftpuser', '111', './media/sensors', 'D:/media',  nothing )
        return count
    except Exception, e:
        logging.critical('%s', traceback.format_exc())
        return -1

def _sync(r, *args):
    r.value = sync(*args)

def async(url, port, user, passw, fdir, ldir, callback, codec = 'windows_1251'):
    r = multiprocessing.Value('i', -1)
    print r.value
    p = multiprocessing.Process(target = _sync, args=(r, url, port, user, passw, fdir, ldir, codec))
    p.start()
    def waitexit( p, r, callback):
        p.join()
        callback(r.value)
    thread.start_new_thread( waitexit, ( p, r, callback))

    

def nothing(ex):
    if ex<0:
        print('ERROR')
    else:
        print(' SYNC CALLBACK OK')
		
sync( '192.168.10.31', '5433', 'ftpadm', 'ftpadm', '/eily.acuario/tracking', 'C:\eadbsyncsrv\experiments\FTP')
#sync( '192.168.10.25', '5433', 'ftpadm', 'z%F_]wa@8mGy<(W', '/eily.acuario/tracking', 'C:\eadbsyncsrv\experiments\FTP')