'''
This source file is part of eily.acuario sync server

For the latest info, see http://integra-s.com/

Author: Alexey Pronin mailto:topifagorsend@gmail.com

Copyright (c) 2014 Integra-S JSC
'''

from psycopg2 import connect
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import os, json, subprocess

# Database connection options
default_database_name = 'postgres'

# location of file with primary sql
sql_script_file = 'dbscriptsimplified.sql'
config_path = 'config.json'

# script routines
timeout_interval = 5
main_connection = None
new_db_connection = None

# config object
config_json = None

def save_settings(fileName, config):
    jsonText = json.dumps(config, indent = 4)
    file = open(fileName, "w")
    file.write(jsonText)
    file.close()

# load settings
def load_settings(fileName):
    global config_json
    file = open(fileName, "r")
    jsonText = file.read()
    file.close()
    config_json = json.loads(jsonText)

# create json config
def create_config(fileName):
    database_settings = {'database_host' : '', 'database_port' : '', 'database_name' : '','database_login' : '', 'database_pass' : ''}
    ftp_settings = {'ftp_host' : '', 'ftp_port': '', 'ftp_login': '', 'ftp_pass': ''}
    options = {'use_integra_borders': 'True', 'use_integra_features': 'False'}
    config = {'database': database_settings, 'ftp': ftp_settings, 'options': options}
    save_settings(fileName, config)

def execute_process():
    global new_db_connection, main_connection
    db_object = config_json['database']
    options = config_json['options']
    os.environ['PGPASSWORD'] = db_object['database_pass']
    if options['use_integra_borders'] == 'True':
        print 'start integra_borders'
        args = ["pg_restore", "-d", db_object['database_name'], "-h", db_object['database_host'], "-p", db_object['database_port'], "-U", db_object['database_login'], "-wax", "-F", "t", "--table=integra_borders", "integra_borders.backup"]
        proc = subprocess.Popen(args, stdin  = None, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        stdout, stderr = proc.communicate()
        print stdout, stderr
    if options['use_integra_features'] == 'True':
        print 'start integra_features'
        args = ["pg_restore", "-d", db_object['database_name'], "-h", db_object['database_host'], "-p", db_object['database_port'], "-U", db_object['database_login'], "-wax", "-F", "t", "--table=integra_features", "integra_features.backup"]
        proc1 = subprocess.Popen(args, stdin  = None, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        stdout1, stderr1 = proc1.communicate()
        print stdout1, stderr1

def start_sync_db():
    global new_db_connection, main_connection
    db_object = config_json['database']
    ftp_object = config_json['ftp']
    stmt = "CREATE DATABASE %s WITH OWNER = postgres TEMPLATE template_postgis" % db_object['database_name']
    cursor = main_connection.cursor()
    cursor.execute(stmt)
    stmt = "SELECT 1 FROM pg_database WHERE datistemplate = false AND datname = '%s'" % db_object['database_name']
    cursor.execute(stmt)
    if not cursor.fetchone():
        print 'Error!'
        exit(1)
    else:
        new_db_connection = connect("host=%s port=%s dbname=%s user=%s password=%s connect_timeout=%d" % (db_object['database_host'], db_object['database_port'], db_object['database_name'], db_object['database_login'], db_object['database_pass'], timeout_interval))
        new_db_connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        new_cursor = new_db_connection.cursor()
        sql_script = open(sql_script_file).read()
        new_cursor.execute(sql_script)
        conn_string = str(ftp_object['ftp_host']) + str(':') + str(ftp_object['ftp_port'])
        stmt = "INSERT INTO ea_ftps(address, username, password) VALUES('%s', '%s', '%s');" % (conn_string, ftp_object['ftp_login'], ftp_object['ftp_pass'])
        new_cursor.execute(stmt)
        execute_process()
        new_cursor.close()
    cursor.close()

# init main conn
def init():
    global main_connection
    database_obj = config_json['database']
    main_connection = connect("host=%s port=%s dbname=%s user=%s password=%s connect_timeout=%d" %
                    (database_obj['database_host'], database_obj['database_port'], default_database_name, database_obj['database_login'], database_obj['database_pass'], timeout_interval))
    main_connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    statement = "SELECT datname FROM pg_database WHERE datistemplate = false AND datname = '%s';" % database_obj['database_name']
    cursor = main_connection.cursor()
    cursor.execute(statement)
    values = cursor.fetchall()
    for item in values:
        if database_obj['database_name'] == item[0]:
            r = raw_input('Database already exists. Do you want to recreate it? ')
            if (r.lower() == 'y'):
                statement = "SELECT pg_terminate_backend(pg_stat_activity.procpid) \
                                        FROM pg_stat_activity \
                                        WHERE pg_stat_activity.datname = '%s' \
                                        AND procpid <> pg_backend_pid();" % database_obj['database_name']
                cursor.execute(statement)
                statement = "DROP DATABASE %s;" % database_obj['database_name']
                cursor.execute(statement)
            else:
                print 'Nothing to do. Exiting'
                exit(0)
    res = raw_input('Are you really want to create new database?(Yes/No): ')
    if (res.lower() in ['y', 'yes']):
        start_sync_db()
    elif (res.lower in ['n', 'no']):
        exit(0)
    else:
        print 'Unrecognized'

if __name__ == '__main__':
    print "Database init script"
    if not os.path.exists(config_path):
        print 'Config file not found'
        create_config(config_path)
        exit(0)
    else:
        load_settings(config_path)
        init()
