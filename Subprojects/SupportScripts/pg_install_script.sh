#! /bin/bash

# postgresql version = 9.1 & postgis version = 1.5

sudo apt-get install postgis postgresql-9.1 postgresql-server-dev-9.1 postgresql-contrib-9.1 postgis postgresql-9.1-postgis
sudo -u postgres createdb template_postgis
sudo -u postgres psql -d template_postgis -c "UPDATE pg_database SET datistemplate=true WHERE datname='template_postgis'"
sudo -u postgres psql -d template_postgis -f /usr/share/postgresql/9.1/contrib/postgis-1.5/postgis.sql
sudo -u postgres psql -d template_postgis -f /usr/share/postgresql/9.1/contrib/postgis-1.5/spatial_ref_sys.sql
sudo -u postgres psql -U postgres -d template_postgis -c "GRANT ALL ON geometry_columns TO PUBLIC;"
sudo -u postgres psql -U postgres -d template_postgis -c "GRANT ALL ON spatial_ref_sys TO PUBLIC;"
sudo -u postgres psql -U postgres -d template_postgis -c "GRANT ALL ON geography_columns TO PUBLIC;"

# postgresql version = 9.3 & postgis version = 2.1

sudo apt-get install postgis postgresql-9.3 postgresql-server-dev-9.3 postgresql-contrib-3.1 postgis
sudo -u postgres createdb postgis_21_sample
sudo -u postgres psql -d postgis_21_sample -c "UPDATE pg_database SET datistemplate=true WHERE datname='postgis_21_sample'"
sudo -u postgres psql -d postgis_21_sample -f /usr/share/postgresql/9.3/extension/postgis--2.1.3.sql
sudo -u postgres psql -U postgres -d postgis_21_sample -c "GRANT ALL ON geometry_columns TO PUBLIC;"
sudo -u postgres psql -U postgres -d postgis_21_sample -c "GRANT ALL ON spatial_ref_sys TO PUBLIC;"
sudo -u postgres psql -U postgres -d postgis_21_sample -c "GRANT ALL ON geography_columns TO PUBLIC;"
